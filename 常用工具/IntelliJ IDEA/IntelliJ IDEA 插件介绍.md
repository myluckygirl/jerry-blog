# IntelliJ IDEA 插件简介

## 1. 常见的 IDEA 插件分类

- **常用工具支持**  
Java 日常开发需要接触到很多常用的工具，为了便于使用，很多工具也有 IDEA 插件供开发使用，其中大部分已经在 IDEA 中默认集成了。  
例如 : maven、git、svn、tomcat、jetty、jrebel、Gradle 等。

- **功能增强**  
还有些插件提供了一些 IDE 中不具有的功能，比如 : 静态代码扫描、代码自动生成等。

- **框架集成**  
集成框架主要是为了提供框架定制的代码和配置的生成，以及快速的访问框架提供的功能。  
例如 : 集成 Spring 框架，Mybatis 框架等。

- **UI 定制化及优化**  
UI 定制化相关的插件主要提供一下个性化需求定制，例如修改编辑区的背景图片插件、修改代码颜色等。

- **其他编程语言支持**  
IDEA 主要支持 Java，为了使用其他语言，可以使用一些支持其他语言的插件，通过这些插件可以实现语法分析，配色主题，代码格式化和提示等功能。  
例如 : Go 语言的支持的插件。

- **个人或者公司特殊需求**  
公司内部插件

## 2. 常用插件列表

简单介绍一下日常开发中使用到的插件。

![常用插件列表](../../img/tools/IntelliJ IDEA/myplugins.png)

## 3. IDEA 插件安装

IDEA 的插件安装非常简单，对于很多插件来说，只要你知道插件的名字就可以在 IDEA 里面直接安装。

**安装方式** ：
- `Preferences —> Plugins —> Browse repositories -> 查找所需插件 —> Install`  
- `Preferences —> Plugins —> Install plug from disk —> 选择下载好的插件安装`
安装之后重启 IDEA 即可生效。

> IntelliJ IDEA 打开设置的快捷键是 ： " command + , "

![插件安装步骤](../../img/tools/IntelliJ IDEA/install.png)

**IDEA 插件仓库**  
IntelliJ IDEA 激发了许多 Java 开发人员编写插件， IntelliJ IDEA Plugins 中目前包含 3000+ 个插件并且还在不断增长。

IDEA插件仓库地址 <https://plugins.jetbrains.com/?idea>

## 4. 基本功能

作者目前使用的 IntelliJ IDEA 版本是专业版2018.2.4（Ultimate Edition）

很多插件中提供的功能在这个版本中都已经集成进来了，这里简单说几个可能比较常用的，这几种功能就无需加装插件了。

### 4.1 **背景图片**  

**设置方法** : `control + command + A (或者 Help -> Find Action) 调用弹窗后输入Set Background Image`

![背景图片](../../img/tools/IntelliJ IDEA/BackgroundImg.jpg)


设定要设置为Image的图片,透明度调到15左右，保存即可。
![设置](../../img/tools/IntelliJ IDEA/ImgSet.png)

### 4.2 内置 Terminal

目前 IntelliJ IDEA 已经有一个内置的 Terminal 工具，可以方便的使用 shell 命令。

**快捷键使用** ： option + F12

![Terminal](../../img/tools/IntelliJ IDEA/Terminal.png)

### 4.3 内置support

目前很多新版本的 IntelliJ IDEA 中，已经内置了很多 support 插件，比如我们常用的 Markdown support 、UML support 以及 android support 等。

如果没有内置的话，可以根据需要自行下载相关插件并安装。

## 5. 必备插件

接下来，开始介绍一些比较常用的插件，这些插件可以大大提升开发效率。

### 5.1 Maven Helper

目前，Java 开发很多都在使用 maven 进行项目管理和自动构建。

日常开发中，可能经常会遇到 jar 包冲突等问题，就需要通过查看 maven 依赖树来查看依赖情况。这种方式不是很高效，这里推荐一个插件，安装之后，直接打开 pom 文件，即可查看依赖数，还能自动分析是否存在 jar 包冲突。  

一旦安装了 Maven Helper 插件，只要打开 pom 文件，就可以打开该 pom 文件的 **Dependency Analyzer** 视图（在文件打开之后，文件下面会多出这样一个 tab）。  

进入 Dependency Analyzer 视图之后有三个查看选项，分别是 : 
- Conflicts(冲突)
- All Dependencies as List(列表形式查看所有依赖)
- All Dependencies as Tree(树结构查看所有依赖)

并且这个页面还支持搜索。

![Maven Helper](../../img/tools/IntelliJ IDEA/MavenHelper1.png)

### 5.2 FindBugs-IDEA

FindBugs 很多人都并不陌生，Eclipse 中有插件可以帮助查找代码中隐藏的 bug，IDEA 中也有这款插件。

使用方法很简单，就是可以对多种级别的内容进行 finbugs.

![FindBugs](../../img/tools/IntelliJ IDEA/FindBugs1.jpg)

分析完之后会有一个视图进行提示，详细的说明是哪种问题。按照提示解决完问题之后再执行 findbugs 查看情况即可。

### 5.3 阿里巴巴代码规约检测

2017 年 10 月 14 日杭州云栖大会，Java 代码规约扫描插件全球首发仪式正式启动，规范正式以插件形式公开走向业界，引领 Java 语言的规范之路。

Java 代码规约扫描插件以今年年初发布的《阿里巴巴 Java 开发规约》为标准，作为 Eclipse、IDEA 的插件形式存在，检测 JAVA 代码中存在不规范得位置然后给予提示。规约插件是采用 kotlin 语言开发的，感兴趣的同学可以去开看插件源码。

阿里巴巴规约插件包含三个子菜单 ：
- 编码规约扫描
- 关闭实时检测功能
- 切换语言

并且，该插件支持在编写代码的同时进行提示。这款插件，真的可以很大程度上提升代码质量，一定要安装。

插件安装完后，在 Tools 菜单下，启动扫描的快捷键是 ： `option + command + control + J`

![Alibaba](../../img/tools/IntelliJ IDEA/Alibaba.jpg)

### 5.4 GsonFormat

Java 开发中，经常有把 json 格式的内容转成 Object 的需求，比如项目开始时，合作方给你提供了一个 json 格式 request/response，这时候你就需要将其定义成一个 Java 类，GsonFormat 这款插件可以实现该功能。

### 5.5 Lombok plugin

在 Java 中，我们经常会定义很多 JavaBean，这些 Bean 需要有 getter、setter、toString、equals 和 hashCode 等方法。

通常情况下，我们可以使用 IDEA 的快捷键生成这些代码，但是自动生成的代码后，如果 bean 中的属性一旦有修改，需要重新生成，给代码维护增加了一定的负担。

有一款很好的插件，可以帮助开发者节省这部分工作。那就是 Lombok。

只要在 IDEA 中安装了该插件，只需要在 JavaBean 中添加一行注解代码，插件就会自动帮我们生成 getter、setter、toString、equals 和 hashCode 等方法。

当然，这些方法不止在 IDE 中的代码调用中需要用到，在真正线上部署的时候也需要有，所以，还需要使用 maven 引入一个 lombok 的包。

```
<dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>1.16.10</version></dependency>
```
 
```java
public class HollisLab {

    public static void main(String[] args) {
        Wechat wechat = new Wechat();
        wechat.setId("1111");
        wechat.setDesc("这是一个描述");
        System.out.println(wechat);
    }
}

@Data
class Wechat {
    private String id;
    private String desc;
}
```
输出结果：
`Wechat(id=1, desc=这是一个描述)`

我们在 Wechat 类上面添加了 `@Data` 注解，插件就自动帮我们添加了 getter/setter 和 toString 方法。

### 5.6 String Manipulation

字符串日常开发中经常用到的，但是不同的字符串类型在不同的地方可能有一些不同的规则，比如类名要用驼峰形式、常量需要全部大写等，有时候还需要进行编码解码等。这里推荐一款强大的字符串转换工具 —— String Manipulation。

它强大到什么程度，看下他的功能列表你就知道了：

- 文本转换操作

> - 切换样式（camelCase, hyphen-lowercase, HYPHEN-UPPERCASE, snake_case, SCREAMING_SNAKE_CASE, dot.case, words lowercase, Words Capitalized, PascalCase）  
> - 转换为 SCREAMING_SNAKE_CASE (或转换为 camelCase)
> - 转换为 snake_case (或转换为 camelCase)  
> - 转换为 dot.case (或转换为 camelCase)  
> - 转换为 hyphen-case (或转换为 camelCase)   
> - 转换为 hyphen-case (或转换为 snake_case)   
> - 转换为 camelCase (或转换为 Words)
> - 转换为 camelCase (或转换为 lowercase words)
> - 转换为 PascalCase (或转换为 camelCase)
> - 选定文本大写
> - 样式反转
> - Un/Escape
> - Un/Escape 选中的 java 文本
> - Un/Escape 选中的 javascript 文本
> - Un/Escape 选中的 HTML 文本
> - Un/Escape 选中的 XML 文本
> - Un/Escape 选中的 SQL 文本
> - Un/Escape 选中的 PHP 文本
> - 将 diacritics(accents) 转换为 ASCII
> - 将非 ASCII 转换为转义的 Unicode
> - 将转义的 Unicode 转换为字符串
> - Encode/Decode
> - Encode 选中的文本为 MD5 Hex16
> - De/Encode 选中的文本为 URL
> - De/Encode 选中的文本为 Base64
> - 递增/递减
> - 递增/递减所有找到的数字
> - 复制行并且递增/递减所有找到的数字
> - 创建序列：保持第一个数字，递增替换所有其他数字
> - 递增重复的数字
> - 按自然顺序排序
> - 按行倒序
> - 按行随机排序
> - 区分大小写 A-z 排序
> - 区分大小写 z-A 排序
> - 不区分大小写 A-Z 排序
> - 不区分大小写 Z-A 排序
> - 按行长度排序
> - 通过子选择行排序：每行仅处理一个选择/插入符号
> - 对齐
> - 通过选定的分隔将选定的文本格式化为列/表格
> - 将文本对齐为左/中/右
> - 过滤/删除/移除
> - grep 选定的文本，所有行不匹配输入文字将被删除。 （不能在列模式下工作）
> - 移除选定的文本
> - 移除选定文本中的所有空格
> - 删除选定文本中的所有空格
> - 删除重复的行
> - 只保留重复的行
> - 删除空行
> - 删除所有换行符
> - 其他
> - 交换字符/选择/线/标记
> - 切换文件路径分隔符：Windows < – > UNIX
> - 安装好插件后，选中需要处理的内容后，按快捷键 Alt+m，即可弹出工具功能列表。

很好很强大的一款字符串处理工具。

```html
<article class="" style="margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; overflow-wrap: break-word !important;">
```

### 5.7 .ignore

目前很多开发都在使用 git 做版本控制工具，但是有些时候有些代码我们是不想提到到我们的代码仓库中的，比如 IDE 自动生成的一些配置文件，或者是我们打包生成的一些 jar 文件等，这时候就需要编写一个 .ignore 文件，来排除那些不想被版本管理的文件。

这里推荐一个好用的插件 .ignore，他可以帮我们方便的生成各种 ignore 文件。

安装插件后，选中项目，右键新建的时候，会多出一个 .ignore 文件的选项，可以通过这个选项创建 ignore 文件。

在弹出的对话框中，可以自动帮我们生成一份 .ignore 文件，这里我们让其帮忙自动排除所有和 IDEA 有关的文件。

### 5.8 Mybatis plugin

目前 ORM 框架中，Mybatis 非常受欢迎。但是，同时给很多开发带来困扰的就是 Mybatis 需要很多 xml 的配置文件，有的时候很难去进行修改。

这里推荐一款神器，可以让你像编辑 java 代码一样编辑 mybatis 的文件。

Intellij Idea Mybatis 插件主要功能：
- 提供 Mapper 接口与配置文件中对应 SQL 的导航
- 编辑 XML 文件时自动补全
- 根据 Mapper 接口, 使用快捷键生成 xml 文件及 SQL 标签
- ResultMap 中的 property 支持自动补全，支持级联(属性 A. 属性 B. 属性 C)
- 快捷键生成 @Param注解
- XML 中编辑 SQL 时, 括号自动补全
- XML 中编辑 SQL 时, 支持参数自动补全(基于 @Param 注解识别参数)
- 自动检查 Mapper XML 文件中 ID 冲突
- 自动检查 Mapper XML 文件中错误的属性值
- 支持 Find Usage
- 支持重构从命名
- 支持别名
- 自动生成 ResultMap 属性

但是这款插件是收费的，但是不影响他确实是一个很实用，可以很大程度上提升开发效率的插件。读者可以考虑使用 Free Mybatis plugin（这款插件我没用过，具体是否好用有待考证）。

### 5.9 Key promoter X

对于很多刚刚开始使用 IDEA 的开发者来说，最苦恼的就是不知道快捷键操作是什么。

使用 IDEA，如果所有操作都使用鼠标，那么说明你还不是一个合格的程序员。

这里推荐一款可以进行快捷键提示的插件 Key promoter X。

Key Promoter X 是一个提示插件，当你在 IDEA 里面使用鼠标的时候，如果这个鼠标操作是能够用快捷键替代的，那么 Key Promoter X 会弹出一个提示框，告知你这个鼠标操作可以用什么快捷键替代。

当我使用鼠标查看一个方法都被哪些类使用的时候，就会提示, 记住这个快捷键以后，就可以使用快捷键代替鼠标啦。

### 5.10 AceJump

前面介绍了一款可以通过使用快捷键来代替鼠标操作的插件，这里再介绍一款可以彻底摆脱鼠标的插件，即 AceJump

AceJump 允许您快速将光标导航到编辑器中可见的任何位置，只需点击“ctrl +;”，然后输入一个你想要跳转到的字符，之后键入匹配的字符就跳转到你想要挑战的地方了。

## 6. 总结

以上插件可以提升程序员开发效率、提升代码质量、提升编码心情的软件。
