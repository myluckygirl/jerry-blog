
# VS Code 配置

### 前置知识
首先说明一下，vscode 为我们提供了两种设置方式：

- User Settings（用户设置）：全局设置，对任意一个运行的 VS Code 都会起作用。
- Workspace Settings（项目设置）：只对当前项目的设置。
注：Workspace Settings 会覆盖 User Settings。

打开用户设置和项目设置的方法如下：

- On Windows/Linux - File > Preferences > Settings
- On macOS - Code > Preferences > Settings
- 或直接通过命令行面板（Ctrl+Shift+P）输入 open settings 进行调出。

vscode 中的 设置选项 都配置在一个 settings.json 文件中。
其中，用户设置（User Settings） 的路径为：

- Windows %APPDATA%\Code\User\settings.json
- macOS $HOME/Library/Application Support/Code/User/settings.json
- Linux $HOME/.config/Code/User/settings.json
而 项目设置（Workspace Settings） 的路径为：根目录下的.vscode中。

