

这样配置，让你的 VS Code 好用到飞起！ : https://www.jianshu.com/p/fd945e8e099d


### 编辑器与窗口管理

Ctrl+Shift+P: 打开命令面板。

Ctrl+Shift+N: 新建窗口。

Ctrl+Shift+W: 关闭窗口。

切分窗口：Ctrl+1/Ctrl+3/Ctrl+3

Ctrl+H：最小化窗口

Ctrl+B：显示/隐藏侧边栏

Ctrl+"+/-"：放大/缩小界面

### 文件操作

Ctrl+N：新建文件

Ctrl+W：关闭文件

Ctrl+Tab：文件切换

### 代码注释 （再操作一次取消注释）

行注释： Ctrl + /

块注释在： Shift + Alt + A

### 格式调整

Ctrl+C/Ctrl+V：复制或剪切当前行/当前选中内容 (F3 查找下一个)

Ctrl+F 查找

Ctrl+H 替换

Ctrl+S 保存

Ctrl + Shift + Enter 上方插入一行

Command + Enter  下方插入一行

Alt + Shift + F 格式化代码

Alt+Up/Down：向上/下移动一行

Shift+Alt+Up//Down：向上/下复制一行

Ctrl+Delete：删除当前行

Shift+Alt+Left/Right：从光标开始向左/右选择内容

### 代码编辑

Ctrl+D：选中下一个相同内容

Ctrl+Shift+L：选中所有相同内容

Ctrl+F：查找内容

Ctrl+Shit+F：在整个文件夹中查找内容

### 常用设置

我们可以在settings.json中手动进行一些设置，让我们的编辑器更好用。
