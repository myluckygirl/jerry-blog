# 一. Markdown 是什么

**Markdown是一种轻量级的「标记语言」**

> Markdown 可以使用普通文本编辑器，通过简单的标记语法，使普通文本内容具有一定的格式。还可以转化成格式丰富的 HTML 页面，Markdown 文件的后缀名是".md"

Markdown官网：<http://daringfireball.net/projects/markdown/><br/>
参考资料：<http://markdown.tw/>

# 二. 为什么选择 Markdown

- **语法简洁**：没有任何编程基础的人 10 分钟语言即可上手
- **注重内容**：专注于内容编写，不在因为格式排版而苦恼
- **易阅读性**：即使没有经过转换，大部分内容仍可阅读
- **易编辑性**：任何文本编辑器都能编辑 Markdown 文件
- **跨平台性**：由于是纯文本内容，不存在格式兼容的问题
- **导出方便**：支持导出为 HTML, PDF 等常见格式

# 三. 如何使用 Markdown

## 1. Headers 标题

Markdown 支持 6 种级别的标题，对应 html 标签 h1 ~ h6

```
# h1
## h2
### h3
#### h4
##### h5
###### h6
```
# h1
## h2
### h3
#### h4
##### h5
###### h6

另外，h1 和 h2 还能用以下方式显示：
```
h1 : 一级标题
===

h2 : 二级标题
---
```

## 2. Block Code 段落及区块引用

- **Blockquotes 引用**
```
> 这段文字将被高亮显示，多敲<br/><br/><br/>可以换多个空行，输入多个
>> 可以实现嵌套
>>> 可以实现嵌套
```
> 这段文字将被高亮显示，多敲<br/><br/><br/>可以换多个空行，输入多个
>> 可以实现嵌套
>>> 可以实现嵌套

- **Hard Line Breaks 换行**
```
在一行的结尾处加上2个或2个以上的空格，也可以使用</br>标签   第一行<br/>第二行
```
在一行的结尾处加上2个或2个以上的空格，也可以使用</br>标签   第一行<br/>第二行

- **Escape character 转义符(反斜杠：\\)**

Markdown 支持以下这些符号前面加上反斜杠来帮助插入普通的符号：

```
\反斜杠  
`反引号  
*星号  
_下划线  
{}花括号  
[]方括号  
()括弧  
#井字号  
+加号  
-减号  
.英文句 
!感叹号
```

## 3. 表格
```
|:在左靠左 |:在两端居中:|在右靠右:|
|:----|:----:|----:|
| A | A | A |
| B | B | B |
| C | C | C |
```

|:在左靠左 |:在两端居中|:在右靠右|
|:----|:----:|----:|
| A | A | A |
| B | B | B |
| C | C | C |

## 4. Emphasis 文本强调(字体样式)

|序号 |源代码|效果|
|:----:|:----|:----|
| 1 | \~~删除线~~ | ~~删除线~~ |
| 2 | \*斜体字* 或 \_斜体字_ | *斜体字* 或 _斜体字_ |
| 3 | \*\*粗体** 或 \_\_粗体__ | **粗体** 或 __粗体__ |
| 4 | \*\*\*粗斜体*** 或 \_\_\_粗斜体___ | ***粗斜体*** 或 ___粗斜体___ |
| 5 | 上标：X\<sup>2\</sup> | 上标：X<sup>2</sup> |
| 6 | 下标：H\<sub>2\</sub>O | 下标：H<sub>2</sub>O |
| 7 | 字体颜色$\color{red}{红色字}$ 或 $\color{#FF0000}{红色字}$ 或 $\color{rgb(255,255,0)}{黄色字}$ | 字体颜色$\color{red}{红色字}$ 或 $\color{#FF0000}{红色字}$ 或 $\color{rgb(255,255,0)}{黄色字}$ |

> 注：* 和 _ 和文字之间不能有空格，如果则会被当成普通符号。如果文字前后需要插入 * 或 _ ，可以使用反斜线（转义符：\），如：\*我是星星里面的文字\*

## 5. Links 图片和链接

```
[Inline-style 内嵌方式](https://www.baidu.com)
```
[Inline-style 内嵌方式](https://www.baidu.com)

```
Reference-style 引用方式：<https://www.baidu.com>
```
Reference-style 引用方式：<https://www.baidu.com>

```
[Relative reference to a repository file 引用存储文件](../README.md)
```
[Relative reference to a repository file 引用存储文件](../../README.md)

```
邮箱地址自动链接 123456789@qq.com
```
邮箱地址自动链接 123456789@qq.com
```
![Images 图片链接，可以添加备注](https://upload-images.jianshu.io/upload_images/3668672-847b228d15e1e9d1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "markdown图片")
```
![Images 图片链接，可以添加备注](https://upload-images.jianshu.io/upload_images/3668672-847b228d15e1e9d1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "markdown图片")

> 注：文件名称最好不要用中文。

### 6. 代码高亮
```
行内高亮：`npm install marked`
```
行内高亮：`npm install marked`

实现代码块适配合适的高亮方法，可以用```包裹一段代码，并指定一种语言。

```
    ```javascript
        function test() {
        console.log("打印输出");
        }
    ```
```


```javascript
    function test() {
      console.log("打印输出");
    }
```

```
    ```html
        <div>Syntax Highlighting</div>
    ```
```
```html
        <div>Syntax Highlighting</div>
```
```
    ```css
        body{font-size:12px}
    ```
```
```css
    body{font-size:12px}
```


支持的语言： $\color{red}{bash, basic,cmake, css, delphi, django, dockerfile, go, http,,ini, java, javascript, json, leaf, less, matlab, perl, php, python, ruby, rust, scss, sql, stylus, swift, typescript, vbscript, xml, yaml等}$

### 7. Unordered 无序列表

*/-/+/>/# 通用，后面跟空格即可，另外输入 **1 个 TAB 或 2 ~ 4 个空格**可实现层级关系。
```
+ 一级列表
    + 二级列表
        + 三级列表
```
+ 一级列表
    + 二级列表
        + 三级列表

### 8. Ordered 有序列表

**注意数字后面有空格**，序号递增排列，即使输错也会自动纠正

```
1. 第一
2. 第二
3. 第三
```
1. 第一
2. 第二
3. 第三

> 避免文本转化为有序列表，可以在 "." 前加上反斜杠（转义符：\）：如：

- 修改前
    ```
    2020. 年度工作计划
    ```
    2020. 年度工作计划

- 修改后
    ```
    2020\. 年度工作计划
    ```
    2020\. 年度工作计划


### 9. 分割线

可以在一行中用 3 个以上的 * 或 - 或 _ 来建立一个分割线，行内不能有其他东西，为了兼容其他平台，可以在符号中间插入空格。

> \--- <br/>
> \*** <br/>
> \___

---
***
___

### 10. 转义字符

```
\**粗体**
\>
\<br>
上标：X\<sup>2\</sup> 
```


**粗体**
>
<br>
上标：X<sup>2</sup> 

### 11. 脚注

```
生成一个脚注[^footnote].
[^footnote]:这里是**脚注**的*内容*
```
生成一个脚注[^footnote].
[^footnote]:这里是**脚注**的*内容*

### 12. 特殊符号[^footnote].
   [^footnote]:文本通用，非MD独有

> 常用排版 : ▌▍◆★☆☁➤➜❤➊➋➌  
> TodoList : ✅☑✓✔√☓☒✘ㄨ✕✖✗❌❎   
> emoji : 🌹🍀🌙🍂🍃🌷💎🔥⭐🍄🏆  

更多的特殊符号就不一一列举了，想要了解更多打法的请查阅下面链接，也可以使用搜狗输入法：

[HTML 中的特殊符号](https://www.w3school.com.cn/tags/html_ref_symbols.html)  
[Emoji](https://emojipedia.org)

13. Markdown编辑器：

在线版，推荐和印象笔记结合的马克飞象

- [马克飞象](https://maxiang.io/)
- [作业部落](https://www.zybuluo.com/mdeditor)

浏览器插件：

- [马克飞象](https://chrome.google.com/webstore/detail/marxico/kidnkfckhbdkfgbicccmdggmpgogehop?utm_source=chrome-ntp-icon)
- [Markdown-here](https://markdown-here.com/)

客户端：

Mac：[MacDown](http://macdown.uranusjr.com/)   
Windows：[MarkdownPad](http://markdownpad.com/)、[MarkPad](http://code52.org/DownmarkerWPF)  
