## 1、Markdown 表格中 " | " 符号，无法正常转义。
举个栗子 ：

```
| 表达式 | 描述 |
|:---:|:---:|
| (mh|g|abc) | 查找任何指定的选项 |
```
**解决方式** ： 使用 [ASCII 字符集](http://www.runoob.com/tags/html-ascii.html)。具体看参照 ：[HTML 中特殊字符和与之对应的 ASCII代码](https://www.w3school.com.cn/tags/html_ref_ascii.asp)。" | " 符号 HTML 对应的 ASCII 对应的字符：`&#124;`

[W3C:HTML转义字符对照表](http://www.w3chtml.com/html/character.html)  
[OSCHINA:HTML转义字符对照表](https://tool.oschina.net/commons?type=2)
```
| 表达式 | 描述 |
|:---:|:---:|
| (mh&#124;g&#124;abc) | 查找任何指定的选项 |
```

修改后的效果：

| 表达式 | 描述 |
|:---:|:---:|
| (mh&#124;g&#124;abc) | 查找任何指定的选项 |