# 年轻时的 Linus Torvalds
芬兰，被誉为千湖之国。这是一个由众多湖泊和岛屿组成的国家，准确的说，全国共有 188,000 个湖泊和 179,000 个岛屿。

1969 年，Linus Torvalds 生于芬兰一个知识分子家庭。父亲 Nils Torvalds 毕业于赫尔辛基大学，是一名活跃的电台记者。母亲 Anna Torvalds 同样毕业于赫尔辛基大学，也是一名记者。有趣的是，他的祖父奥 Ole Torvalds 也是一名记者。除此之外，Torvalds 的外祖父 Leo Tornqvist 是芬兰第一批统计学教授。优秀的家庭背景为 Torvalds 奠定了接受良好教育的基础。Torvalds 在 11 岁时，应其外祖父要求用 BASIC 语言编写一些统计学方面的小程序。大众普遍认为，这是他编程经历之始。

在这样的背景下，似乎注定了这位少年拥有不平凡的成长经历。
![Linus Torvalds](../../img/tools/Linus Torvalds.png)

1988年，Torvalds 进入赫尔辛基大学计算机科学系就读。在兴趣的趋势下，Torvalds 创造并发布了自制的操作系统，并将其命名为 Linux。1996 年硕士毕业并移居美国，后拥有美国国籍。2003 年，为专心维护 Linux 从全职公司辞职。

芬兰人性格内敛，这与 Torvalds 的行事方式不谋而合。但他对开源的信念是近乎执着的，为此不少树敌。

> 赫尔辛基大学，成立于1640年，是芬兰最大且历史最悠久的最高学府。Torvalds 一家三口都毕业于此。

# Git 的诞生史

## Linux 代码手工管理难以继续

很多人都知道，Linus 在 1991 年创建了开源的 Linux，从此，Linux 系统不断发展，已经成为最大的服务器系统软件了。

Linus 虽然创建了 Linux，但 Linux 的壮大是靠全世界热心的志愿者参与的，这么多人在世界各地为 Linux 编写代码，那 Linux 的代码是如何管理的呢？

事实是，在 2002 年以前，世界各地的志愿者把源代码文件通过 diff 的方式发给 Linus，然后由 Linus 本人通过手工方式合并代码！

你也许会想，为什么 Linus 不把 Linux 代码放到版本控制系统里呢？不是有 CVS、SVN 这些免费的版本控制系统吗？因为 Linus 坚定地反对 CVS 和 SVN，这些集中式的版本控制系统不但速度慢，而且必须联网才能使用。有一些商用的版本控制系统，虽然比 CVS、SVN 好用，但那是付费的，和 Linux 的开源精神不符。

不过，到了 2002 年，Linux 系统已经发展了十年了，代码库之大让 Linus 很难继续通过手工方式管理了，社区的弟兄们也对这种方式表达了强烈不满。

## BitKeeper 使用到禁用

当时最好用的版本管理软件是 BitKeeper(BitKeeper 不开源，收费)。BitKeeper CEO Larry McVoy 也是 Linus 的好友，他对 Linus 说，Linux Kernal 开发团队可以免费使用 BitKeeper，并且说服 Linus 在 Linux Kernel 项目中使用 BitKeeper。但后来，BitKeeper 功能越来越不适用，而且 BitKeeper 在免费使用的许可证中加入很多限制条件，这样惹恼了内核开发者。

开发 Samba 的 Andrew 试图破解 BitKeeper 的协议（这么干的其实也不只他一个），被 BitMover 公司发现了（监控工作做得不错！），于是 BitMover 公司怒了，要收回 Linux 社区的免费使用权。

Linus 可以向 BitMover 公司道个歉，保证以后严格管教弟兄们，嗯，这是不可能的。双发最终没有谈妥，BitKeeper 停止向开源社区提供免费使用的 BitKeeper。

## Git 诞生

> The development of Git began on 3 April 2005.[20] Torvalds announced the project on 6 April;[21] it became self-hosting as of 7 April.[20] The first merge of multiple branches took place on 18 April.[22] Torvalds achieved his performance goals; on 29 April, the nascent Git was benchmarked recording patches to the Linux kernel tree at the rate of 6.7 patches per second.[23] On 16 June Git managed the kernel 2.6.12 release   --- 摘自[Git Wiki](https://en.wikipedia.org/wiki/Git)

Linus 大神 4 月 6 日宣布 Git 项目，到 4 月 18 日，就达到设定的 Performance goals; 6 月 16 日，Git 就开始正式管理 Linux Kernel 开发。

Linus 花了差不多 10 天的时间，自己用 C 写了一个分布式版本控制系统！牛是怎么定义的呢？大家可以体会一下。

# Git 极具前瞻性的三个诉求

在确定开发 Git 前，Torvalds 对市面上多个版本管理方案进行过评估，但现有的方案都不令人满意，最终决定开发自己的版本管理系统。

Torvalds 认为，健壮的版本管理系统应当有以下三个特性：

- 可靠性（reliable）
- 高效（high-performance）
- 分布式（distributed）

这三个特性，被视为 git 的核心灵魂所在，深远的影响了 Git 及其他 SCM 的后续发展。

## 可靠性（reliable）

数据的存入取出必须是安全的、一致的。所有行为都要校验，仓库任何部分不允许篡改。在今天看来，这并不是什么高明之举，但在当时，绝大多数的 SCM 都做不到这一点。那时候，人们依赖保护中央服务器来保证数据不被篡改，而不是依靠版本管理自身的设计来保证。

Torvalds 认为，应当通过算法来保证，仓库任何一个字节被篡改，都能够被发现。

## 高效（high-performance）

这是 Git 极具优势的特性。Torvalds 认为，版本管理的所有操作都必须在毫秒级内完成：这是对 SVN 最大的批评。查日志、拉分支、合并、提交这些高频率操作，必须能够在本地能够完成，而不是等待服务器响应。这一特性直接决定了 Git 能够被广泛传播。

作为对比，SVN 每一步操作都在等待数据包成为了人们口中的诟病。

## 分布式（distributed）

以往人们在备份资料上花了很大精力：传统的中央服务器，资料受损是灾难性事故。现在，所有人电脑中的 Git 仓库都是一致的，每个人的仓库都是完整的副本。完整的副本意味着可以在本地做所有事；允许相互同步促使它被设计成为自带数据校验。

事实证明，这些想法是极具前瞻性的。Git 在解决原有诉求的前提下，还解决了长久以来的一些痛点。

# 开源，共筑，影响力

革新总是艰难的。当 Linux 开发团队决定使用 Git 作为版本管理的时候，社区反对声音并不小，理由是 Git 太难用了。实际上，确实是这样的，Torvalds 也承认了这点。

技术服众还不够。Torvalds 的领导力是惊人的，他发动社区力量让大家投入到 Git 的开发当中。随着开发的深入，Git 命令变得更简单，更易用。另一方面，开源有助于集思广益，并且避免腐败。

后来的事，大家都知道了。

Git 迅速成为最流行的分布式版本控制系统，尤其是 2008 年，GitHub 网站上线了，它为开源项目免费提供 Git 存储，无数开源项目开始迁移至 GitHub，包括 jQuery，PHP，Ruby 等等。

历史就是这么偶然，如果不是当年 BitMover 公司威胁 Linux 社区，可能现在我们就没有免费而超级好用的 Git 了。

另外，2016 年 5 月，BitKeeper 宣布以 Apache 2.0 许可证开源。但人们说，太晚了。

