## 一、上传项目到 GitLab 仓库

a. 创建新项目
```
git clone git@git.xxxxxx.com:xxxxxx/project-name.git
cd project-name
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

b. 已存在文件夹
> 1. 进入项目文件夹   
`git init`
> 2. 项目代码添加到本地 git  
`git add .`
> 3. 提交到stage区域  
`git commit -m '提交内容说明'`
> 4. 本地仓库连接远程 GitLab 地址    
`git remote add origin https://gitlab.com/xxxxxx/xxxxxx.git`
> 5. 上传本地代码    
`git push -u origin master`
> 6. 查看提交状态    
`git status`

c. 已存在的 Git 仓库
```
cd existing_repo
git remote add origin git@git.xxxxxx.com:xxxxxx/project-name.git
git push -u origin --all
git push -u origin --tags
```

## Git 的使用：从远程仓库获取最新代码合并到本地分支

这里展示以下两类三种方式

### 1. git pull：获取最新代码到本地，并自动合并到当前分支
git命令展示 

```git
//查询当前远程的版本
$ git remote -v
//直接拉取并合并最新代码
$ git pull origin master [示例1：拉取远端origin/master分支并合并到当前分支]
$ git pull origin dev [示例2：拉取远端origin/dev分支并合并到当前分支]
```
> 备注：不推荐这种方式，因为是直接合并，无法提前处理冲突。

### 2. git fetch + merge: 获取最新代码到本地，然后手动合并分支

#### 2.1. 额外建立本地分支
git命令展示

```git
//查看当前远程的版本
$ git remote -v 
//获取最新代码到本地临时分支(本地当前分支为[branch]，获取的远端的分支为[origin/branch])
$ git fetch origin master:master1  [示例1：在本地建立master1分支，并下载远端的origin/master分支到master1分支中]
$ git fetch origin dev:dev1[示例1：在本地建立dev1分支，并下载远端的origin/dev分支到dev1分支中]
//查看版本差异
$ git diff master1 [示例1：查看本地master1分支与当前分支的版本差异]
$ git diff dev1    [示例2：查看本地dev1分支与当前分支的版本差异]
//合并最新分支到本地分支
$ git merge master1    [示例1：合并本地分支master1到当前分支]
$ git merge dev1   [示例2：合并本地分支dev1到当前分支]
//删除本地临时分支
$ git branch -D master1    [示例1：删除本地分支master1]
$ git branch -D dev1 [示例1：删除本地分支dev1]
```
> 备注：不推荐这种方式，还需要额外对临时分支进行处理。

#### 2.2. 不额外建立本地分支
git命令展示

```git
//查询当前远程的版本
$ git remote -v
//获取最新代码到本地(本地当前分支为[branch]，获取的远端的分支为[origin/branch])
$ git fetch origin master  [示例1：获取远端的origin/master分支]
$ git fetch origin dev [示例2：获取远端的origin/dev分支]
//查看版本差异
$ git log -p master..origin/master [示例1：查看本地master与远端origin/master的版本差异]
$ git log -p dev..origin/dev   [示例2：查看本地dev与远端origin/dev的版本差异]
//合并最新代码到本地分支
$ git merge origin/master  [示例1：合并远端分支origin/master到当前分支]
$ git merge origin/dev [示例2：合并远端分支origin/dev到当前分支]
```
> 备注：推荐这种方式。


## 二、提示：fatal: not a git repository (or any of the parent directories): .git

输入 `git init` 就可以解决

### 三、git clone时RPC failed; curl 18 transfer closed with outstanding read data remaining

git clone时报RPC failed; curl 18 transfer closed with outstanding read data remaining 错误

原因1：缓存区溢出

解决方法：命令行输入

```git config http.postBuffer 524288000```

执行上面命令如果依旧clone失败，考虑可能原因2：网络下载速度缓慢

解决方法：命令行输入
```
git config --global http.lowSpeedLimit 0
git config --global http.lowSpeedTime 999999
```

如果依旧clone失败，则首先浅层clone，然后更新远程库到本地

```
git clone --depth=1 http://gitlab.xxx.cn/yyy/zzz.git
git fetch --unshallow
```