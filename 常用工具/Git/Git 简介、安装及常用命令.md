# 一. 简介

## Git 是什么

 [Git](https://git-scm.com) 是一个免费的开源分布式版本控制系统。
 
### 什么是版本控制
版本控制是一种记录一个或若干个文件内容变化，以便将来查阅特定版本修订情况的系统。

### 什么是分布式版本控制系统
介绍分布式版本控制系统前，有必要先了解一下传统的集中式版本控制系统。  

**集中化的版本控制系统**，诸如 CVS，Subversion 等，都有一个单一的集中管理的服务器，保存所有文件的修订版本，而协同工作的人们都通过客户端连到这台服务器，取出最新的文件或者提交更新。  

这么做最显而易见的缺点是中央服务器的单点故障。如果宕机一小时，那么在这一小时内，谁都无法提交更新，也就无法协同工作。要是中央服务器的磁盘发生故障，碰巧没做备份，或者备份不够及时，就会有丢失数据的风险。最坏的情况是彻底丢失整个项目的所有历史更改记录。  

![集中化的版本控制系统](../../img/tools/Git/Central%20CVS.png)

**分布式版本控制系统**的客户端并不只提取最新版本的文件快照，而是把代码仓库完整地镜像下来。这么一来，任何一处协同工作用的服务器发生故障，事后都可以用任何一个镜像出来的本地仓库恢复。因为每一次的提取操作，实际上都是一次对代码仓库的完整备份。
![分布式的版本控制系统](../../img/tools/Git/Git.png)

# 三. 安装配置

## 1. 下载 & 安装

[官方下载地址](https://git-scm.com/downloads)
- [Mac OS X](https://git-scm.com/download/mac)
- [Windows](https://git-scm.com/download/win)
- [Linux/Unix](https://git-scm.com/download/linux)

也可以通过 Git 本身获取最新的开发版本：`git clone https://github.com/git/git`

## 2. 配置

Git 自带一个git config的工具来帮助设置控制 Git 外观和行为的配置变量。 这些变量存储在三个不同的位置：

- **/etc/gitconfig 文件** ：包含系统上每一个用户及他们仓库的通用配置。 如果使用带有 --system 选项的 git config 时，它会从此文件读写配置变量。
- **\~/.gitconfig或\~/.config/git/config 文件** ： 只针对当前用户。 可以传递 --global 选项让 Git 读写此文件。
- 当前使用仓库的 Git 目录中的 config 文件（就是 .git/config）：针对该仓库。

每一个级别覆盖上一级别的配置，所以 .git/config 的配置变量会覆盖 /etc/gitconfig 中的配置变量。

在 Windows 系统中，Git 会查找 $HOME 目录下（一般情况下是 C:\Users\$USER）的 .gitconfig 文件。 Git 同样也会寻找 /etc/gitconfig 文件，但只限于 MSys 的根目录下，即安装 Git 时所选的目标位置。

## 3. 用户信息

当安装完 Git 应该做的第一件事就是设置你的用户名称与邮件地址。 这样做很重要，因为每一个 Git 的提交都会使用这些信息，并且它会写入到你的每一次提交中，不可更改：<br/>
`$ git config --global user.name "Jerry.li"`  
`$ git config --global user.email jerry.li@example.com`

再次强调，如果使用了 --global 选项，那么该命令只需要运行一次，因为之后无论你在该系统上做任何事情， Git 都会使用那些信息。 当你想针对特定项目使用不同的用户名称与邮件地址时，可以在那个项目目录下运行没有 --global 选项的命令来配置。

很多 GUI 工具都会在第一次运行时帮助你配置这些信息。

## 4. .gitignore

.gitignore 文件可能从字面含义也不难猜出：这个文件里配置的文件或目录，会自动被 Git 所忽略，不纳入版本控制。

在日常开发中，我们的项目经常会产生一些临时文件，如编译 Java 产生的 *.class 文件，又或是 IDE 自动生成的隐藏目录（Intellij 的 .idea 目录、Eclipse 的 .settings 目录等）等等。这些文件或目录实在没必要纳入版本管理。在这种场景下，你就需要用到 .gitignore 配置来过滤这些文件或目录。

配置的规则很简单，也没什么可说的，看几个例子，自然就明白了。

这里推荐一下 Github 的开源项目：<https://github.com/github/gitignore>

在这里，你可以找到很多常用的模板，如：Java、Nodejs、C++ 的 .gitignore 模板等等。

## 5. GitLab 配置 SSH （Mac）

步骤1. 首先运行 terminal 检查是否已经有 SSH Key  
`$ cd ~/.ssh`  
`$ ls`

检查是否已经存在 id_rsa.pub 或 id_dsa.pub 文件，如果文件已经存在，那么你可以跳过步骤2，直接进入步骤3  

步骤2. 创建一个SSH key  
`$ ssh-keygen -t rsa -C "your_email@example.com"`  

接着又会提示你输入两次密码（该密码是你 push 文件的时候要输入的密码，而不是 GitLab 管理者的密码），当然，你也可以不输入密码，直接按回车。那么 push 的时候就不需要输入密码，直接提交到 GitLab 上了，如：
```
Enter passphrase (empty for no passphrase): 
# Enter same passphrase again:

our identification has been saved in /your_path/.ssh/id_rsa.
# Your public key has been saved in /your_path/.ssh/id_rsa.pub.
# The key fingerprint is:
# 01:0f:f4:3b:ca:85:d6:17:a1:7d:f0:68:9d:f0:a2:db your_email@example.com
```
当你看到上面这段代码时，那就说明，你的 SSH key 已经创建成功，你只需要添加到 GitLab 的 SSH key 上就可以了。

步骤3. 添加公钥到你的远程仓库（GitLab）  
a. 查看你生成的公钥，输入：  
`$ cat ~/.ssh/id_rsa.pub`   
b. 把 terminal 上显示的内容 copy 出来  
c. 登陆你的 GitLab 帐户。点击你的头像，然后 Settings -> 点击 SSH Keys , Add an SSH Key  
d. 然后你复制上面的公钥内容，粘贴进 “Key” 文本域内。 Title 域，自己随便起个名字。  
e. 点击 Add key。  

![GitLab SSH 配置](../../img/tools/Git/SSH.png)

# 三. Git 主要概念解析

Git实现在本地和远端进行版本管理。

## 1. 工作空间

- 工作目录(workspace)
- 暂存区(index)
- 本地仓库(local repository)
- 远程仓库(remote repository)

## 2. Head & branch & master & origin

Git 系统的实质更像是一棵大树，树干（就是 Head）是最后一次提交的成果。在树干上，你可以开无数的分支（就是 branch）胡弄，弄乱了也不怕，大不了剪掉再开一个，树干不受任何影响。折腾 OK 的分支，最后可以 merge 到默认 branch 也就是 master 上。

用技术性语言描述，分支用来将特性开发绝缘开来。在创建仓库的时候，master 是“默认的”分支。在其他分支上进行开发，完成后再将它们合并到主分支上。

那 origin 又是什么？origin 是远程默认的仓库。clone 完成之后，Git 会自动将远程仓库命名为 origin。

那 Head 和 master 又是什么关系？Head 其实只是个指针，指向当前最近 commit 的 branch。而 master 是本地默认的 branch，所以 Head 经常都是指向 master。另外 Head 是官方定义的，而 master 和 origin 都是大家常用的命名，并不一定要叫 master 和 origin。

## 3. 工作流：add & commit & push

![工作流](../../img/tools/Git/Git workflow.png)

你的本地仓库由 Git 维护的三棵“树”组成。
- 第一个是你的工作目录，它持有实际文件；
- 第二个是 暂存区（Index），它像个缓存区域，临时保存你的改动；
- 最后是 HEAD，它指向你最后一次提交的结果。

a. 正常工作流

![工作流1：正常](../../img/tools/Git/Git%20workflow%201.png)

上面的四条命令在工作目录、暂存目录(也叫做索引)和仓库之间复制文件。

- `git add <filename> ` ：把当前文件放入暂存区域。(提交全部：git add *)
- `git commit -m "代码提交信息"` ：实际提交改动到 HEAD，但是还没到你的远程仓库。
- `git push origin master` : 将改动提交到远程仓库，可以把 master 换成你想要推送的任何分支。

如果你还没有克隆现有仓库，并欲将你的仓库连接到某个远程服务器，你可以使用如下命令添加：  
`git remote add origin <server>`

逆向操作：
- `git reset -- files` : 用来撤销最后一次 git add files，你也可以用 git reset 撤销所有暂存区域文件。
- `git checkout -- files` : 把文件从暂存区域复制到工作目录，用来丢弃本地修改。

你可以用 git reset -p, git checkout -p, or git add -p进入交互模式。

也可以跳过暂存区域直接从仓库取出文件或者直接提交代码。

b. 工作流捷径

![工作流2：捷径](../../img/tools/Git/Git%20workflow%202.png)

- `git commit -a` : 相当于运行 `git add` 把所有当前目录下的文件加入暂存区域再运行。`git commit`.
- `git commit files` : 进行一次包含最后一次提交加上工作目录中文件快照的提交。并且文件被添加到暂存区域。
- `git checkout HEAD -- files` : 回滚到复制最后一次提交。

## 4. 分支
![分支](../../img/tools/Git/Git branch.png)

分支是用来将特性开发绝缘开来的。在你创建仓库的时候，master 是“默认的”分支。在其他分支上进行开发，完成后再将它们合并到主分支上。

**创建一个叫做“feature_x”的分支，并切换过去** :   
`git checkout -b feature_x`

**切换回主分支** :   
`git checkout master`

**再把新建的分支删掉** :   
`git checkout -d feature_x`

除非你将分支推送到远端仓库，不然该分支就是 不为他人所见的 :   
`git push origin <branch>`

### 分支更新与合并

**更新你的本地仓库至最新改动** : 以在你的工作目录中 获取（fetch） 并 合并（merge） 远端的改动  
`git pull`

**合并其他分支到你的当前分支（例如 master）** :   
`git merge <branch>`

> 在这两种情况下，Git 都会尝试去自动合并改动。遗憾的是，这可能并非每次都成功，并可能出现冲突（conflicts）。 这时候就需要你修改这些文件来手动合并这些冲突（conflicts）。改完之后，你需要执行如下命令以将它们标记为合并成功 :   

`git add <filename>`

在合并改动之前，你可以使用如下命令预览差异 :   
`git diff <source_branch> <target_branch>`

### 替换本地改动

（1）替换本地文件

假如你操作失误（当然，这最好永远不要发生），你可以使用如下命令替换掉本地改动 :   
`git checkout -- <filename>`

此命令会使用 HEAD 中的最新内容替换掉你的工作目录中的文件。已添加到暂存区的改动以及新文件都不会受到影响。

（2）丢弃本地所有改动与提交

假如你想丢弃你在本地的所有改动与提交，可以到服务器上获取最新的版本历史，并将你本地主分支指向它 :   
`git fetch origin`   
`git reset --hard origin/master`

## 5. 标签

为软件发布创建标签是推荐的。这个概念早已存在，在 SVN 中也有。你可以执行如下命令创建一个叫做 1.0.0 的标签 (1b2e1d63ff 是你想要标记的提交 ID 的前 10 位字符):   
`git tag 1.0.0 1b2e1d63ff`

可以使用下列命令获取提交 ID :   
`git log`

你也可以使用少一点的提交 ID 前几位，只要它的指向具有唯一性。

## 6. 日志

如果你想了解本地仓库的历史记录，最简单的命令就是使用 :  
`git log`

你可以添加一些参数来修改他的输出，从而得到自己想要的结果。 只看某一个人的提交记录 :   
`git log --author=jerry`

一个压缩后的每一条提交记录只占一行的输出 :   
`git log --pretty=oneline`

或者你想通过 ASCII 艺术的树形结构来展示所有的分支, 每个分支都标示了他的名字和标签 :   
`git log --graph --oneline --decorate --all`

看看哪些文件改变了 :   
`git log --name-status`

这些只是你可以使用的参数中很小的一部分。更多的信息，参考 :   
`git log --help`

# 四. 常用命令

> 最常用：git command --help

## 1. 创建 & 克隆

> 需要进入目标目录进行操作

**创建新仓库**：创建新文件夹，进入，然后执行   
`git init`

**创建一个本地仓库的克隆版本**：   
`git clone /path/to/repository`

**克隆远端服务器上的仓库**：   
① 通过 SSH ：  
`git clone ssh://user@domain.com/repo.git`

② 通过 HTTPS ：  
`git clone https://domain.com/user/repo.git`

## 2. status/查询
查看信息 : 显示工作路径下已修改的文件  
`git status`

- **staged** : 已在 index，等待被 commit
- **unstaged** : 文件做了改动，但还不能被 commit
- **untracked** : Git 还没有开始跟踪，需要先 add
- **deleted** : 文件已被删除，等待 remove
- **Staging Area** : commit 前把文件们收集到一起，以便打包 commit

## 3. add/添加

添加修改暂存区（让Git开始跟踪更改，也就是从 untracked 变为 tracked）：  
① 把指定文件添加到暂存区   
`git add <filename>` 

② 把当前所有修改添加到暂存区  
`git add .`

③ 把所有修改添加到暂存区（-A 表示包含删除的文件）   
`git add -A` 

④ 从暂存区移除指定文件    
`git reset <filename>`

## 4. commit/提交

提交修改到本地仓库(提交改动到 head，但还没到远程服务器)。   
① 提交本地所有修改   
`git commit -a` 

② 提交之前已标记的变化   
`git commit`

③ 附加消息提交   
`git commit -m "代码提交信息""`

## 5. push/推送
① 将本地版本推送到远程仓库中(远程仓库默认叫origin)   
`git push remote <remote> <branch>`  
 
如 ：`git push -u origin master`   
> -u : 告诉 Git 记住参数，下次可以直接使用 push。

② 提交文件到远程仓库（可以把 master 换成你想要推送的任何分支）  
`git push origin master` 

③ 如果还没有克隆现有仓库，并想将仓库连接到某个远程服务器   
`git remote add origin <server>`

④ 删除远程端分支  
`git push <remote> :<branch> (since Git v1.5.0)`  
`git push <remote> --delete <branch> (since Git v1.7.0)`

⑤ 发布标签  
`git push --tags`

## 6. pull/拉取
① 更新本地仓库至最新改动   
`git pull origin master`

② 更新：下载远程端版本，但不合并到HEAD中  
`git fetch <remote>`

③ 以rebase方式将远端分支与本地合并  
`git pull --rebase <remote> <branch>`

## 7. stash/存储
有时，我们需要在同一个项目的不同分支上工作。当需要切换分支时，偏偏本地的工作还没有完成，此时，提交修改显得不严谨，但是不提交代码又无法切换分支。这时，你可以使用 git stash 将本地的修改内容作为草稿储藏起来。

官方称之为储藏，但我个人更喜欢称之为存草稿。

① 将修改作为当前分支的草稿保存  
`git stash` 

② 查看草稿列表  
`git stash list`  
`stash@{0}: WIP on master: 6fae349 :memo: Writing docs.`

③ 删除草稿  
`git stash drop stash@{0}`

④ 读取草稿  
`git stash apply stash@{0}`

## 8. reset/撤销修改
撤销本地修改。

> 从缓存区撤销所有文件：`git reset`

① 移除缓存区的所有文件（撤销上次git add）  
`git reset HEAD`

② 从缓存区中撤销最后一次 add 的文件  
`git reset --<flies>`

③ 将HEAD重置到上一次提交的版本，并将之后的修改标记为未添加到缓存区的修改  
`git reset <commit>`

④ 将HEAD重置到上一次提交的版本，并保留未提交的本地修改  
`git reset --keep <commit>`

---

> 恢复之前版本：`git reset --hard`

① 放弃工作目录下的所有修改  
`git reset --hard HEAD`

② 将HEAD重置到指定的版本，并抛弃该版本之后的所有修改  
`git reset --hard <commit-hash>`

③ 用远端分支强制覆盖本地分支   
`git reset --hard <remote/branch>`

④ 放弃某个文件的所有本地修改  
`git checkout HEAD <file>`

⑤ 删除添加.gitignore文件前错误提交的文件  
`git rm -r --cached .`   
`git add .`   
`git commit -m "remove xyz file""`

---

① 撤销远程修改（创建一个新的提交，并回滚到指定版本）  
`git revert <commit-hash>`

② 彻底删除指定版本
> 执行下面命令后，commit-hash 提交后的记录都会被彻底删除，使用需谨慎  
`git reset --hard <commit-hash>`  
` git push -f`

回滚到最近一次：`git checkout -- <target>`

## 9. checkout/切换

checkout命令用于从历史提交（或者暂存区域）中拷贝文件到工作目录，也可用于切换分支

切换分支： git checkout <branch>

新建并切换到分支：git checkout -b new_branch 等同于：git branch new_branch + git checkout new_branch

把文件从暂存区域复制到工作目录，用来丢弃本地修改：git checkout --<files>

回滚到复制最后一次提交:git checkout HEAD -- <files>

## 10. diff/比对
显示与上次提交版本文件的不同 ：  
`git diff`

## 11. 显示提交历史
① 从最新提交开始，显示所有的提交记录（显示hash， 作者信息，提交的标题和时间）  
`git log`

② 显示某个用户的所有提交  
`git log --author="username"`

③  显示某个文件的所有修改  
`git log -p <file>`

## 12. 显示搜索内容
① 从当前目录的所有文件中查找文本内容  
`git grep "Hello"`

② 在某一版本中搜索文本  
`git grep "Hello" v2.5`

## 13. 分支
- 分支增删改查  

① 列出所有的分支  
`git branch`

② 列出所有的远端分支
`git branch -r`

③ 基于当前分支创建新分支  
`git branch <new-branch>`

④ 基于远程分支创建新的可追溯的分支
`git branch --track <new-branch> <remote-branch>`

⑤ 删除本地分支  
` git branch -d <branch>`  

⑥ 强制删除本地分支，将会丢失未合并的修改  
`git branch -D <branch>`

---
- 切换分支

① 切换分支  
`git checkout <branch>`

② 创建并切换到新分支  
`git checkout -b <branch>`

## 14. 标签
① 给当前版本打标签  
`git tag <tag-name>`

② 当前版本打标签并附加消息  
`git tag -a <tag-name>`

## 15. 合并与重置
> merge 与 rebase 虽然是 git 常用功能，但是强烈建议不要使用 git 命令来完成这项工作。  
> 因为如果出现代码冲突，在没有代码比对工具的情况下，实在太艰难了。  
> 你可以考虑使用各种 Git GUI 工具。

① 合并 : 将分支合并到当前HEAD中   
`git merge <branch>`

② 重置 ：将当前HEAD版本重置到分支中，请勿重置已发布的提交  
`git rebase <branch>`

## 16. merge
合并其他分支到当前分支 ：  
`git merge`

## 17. cremove & clean
① 从硬盘和index移除文件 ：  
`git rm`

② 删除分支 ：  
`git branch -d <branch name>`

# Ref
[1]. [Git简明指南](http://rogerdudler.github.io/git-guide/index.zh.html)  
[2]. [What are the git concepts of HEAD, master, origin?](http://stackoverflow.com/questions/8196544/what-are-the-git-concepts-of-head-master-origin)  
[3]. [Try Git](https://try.github.io/)  
[4]. [图解Git](http://marklodato.github.io/visual-git-guide/index-zh-cn.html)  
[5]. [Pro Git中文版](https://github.com/progit/progit/tree/master/zh)  
[6]. [Gitmagic中文版](https://github.com/blynn/gitmagic/tree/master/zh_cn)  
[7]. [多用Git少交税](https://www.jianshu.com/p/8a985c622e61)
