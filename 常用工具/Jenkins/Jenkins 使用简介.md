
Jenkins + 单元测试 持续集成
testNG 、 Maven

Jenkins + 接口 自动化测试 持续集成


Jenkins + Web UI 自动化测试 持续集成
注意有界面、无界面的运行方式

Jenkins +  APP UI 自动化测试 持续集成
真机、模拟器

Jenkins + Android APP 自动化编辑、打包、发布、安装、自动回归测试
gradle、adb 命令

Jenkins +  邮件通知、条件设定、结果提取


Jenkins Wed Hook 机制
运行Jenkins 任务触发的方式
- 主动运行
- 定时构建的方式
- 定时轮询代码库方式
- 通过 GitHub 、GitLab Hook 触发

Jenkins API 调用
