# Vue

## 一、创建第一个 Vue 实例

Vue 官方网址：https://cn.vuejs.org

学习教程：进入 Vue 官网，点击 "学习 - 教程" ：https://cn.vuejs.org/v2/guide/

**创建一个 VUe 实例**  
1、新建一个项目，拷贝 vue.js；   
2、新建 index.html，在 head 标签下的 script 标签引入 vue.js； 
  
> 注意：在 head 标签下引入 vue.js 可以防止抖屏的情况。

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Vue入门</title>
	<script src="./vue.js" type="text/javascript"></script>
</head>
<body>
    <div>Hello World</div>
</body>
</html>
```
3、保存，右键 "Open in Browser"，在浏览器打开；  
4、新建一个 Vue 是实例，建立一个挂载点 el，和标签绑定；

new Vue({}) : 创建一个 Vue 的实例
el: "#root" : Vue 实例 el 绑定的挂载为，id 为 root 的标签 <div>；

```html
<body>
	<div id="root">{{msg}}}</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
			    msg: "hello world"
			}
		})
	</script>	
</body>
```
在原生或者 jQuery 里面的写法，手动处理 dom
```html
<body>
	<div id="root">{{msg}}</div>
	<script type="text/javascript">
		var dom = document.getElementById("root");
		dom.innerHTML = "hello world";
	</script>	
</body>
```

##  二、挂载点，模板与实例

关系：

- <div> 标签是 Vue 实例的挂载点，Vue 只会处理挂载点下面的内容  
- 挂载点: 就是 Vue 实例的属性 el 绑定的 id 对应的标签 <div>
- 模板：在挂载点下面的内容都叫做模板内容。模板内容可以写在挂载点标签里面，也可以写在 Vue 实例里面的 template 属性
- 实例：在 Vue 实例里面，定义好挂载点 el，写上对应的模板 template，Vue 自动的结合模板 (template) 和数据 (data), 生成最终要展示的内容，然后把内容放到挂载点之中

> <div> 标签是挂载点，标签下的内容 &lt;h1&gt;hello {{msg}}}&lt;/h1&gt; 就叫做模板。  

```html
<div id="root">
    <h1>hello {{msg}}}</h1>
</div>
```
**模板内容的编写方式**   

1\.  模板内容写在挂载点标签里面

```html
<body>
	<div id="root">
		<h1>hello {{msg}}</h1>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				msg: "hello world"
			}
		})
	</script>	
</body>
```
2\. 将模板内容写在 Vue 实例的 template 属性。
```html
<body>
	<div id="root"></div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			template: "<h1>hello {{msg}}</h1>",
			data: {
				msg: "hello world"
			}
		})
	</script>	
</body>
```

## 三、Vue 实例中的数据，事件和方法

### 数据绑定的三种方法:
1、**插值表达式** : 在模板内容定义，在挂载点标签中引入；
2、使用 v-text ：内容会经过一次转义
3、使用 v-html：内容不会转义

插值表达式：
```html
<body>
	<div id="root">
		<h1>{{number}}</h1>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				number: 123
			}
		})
	</script>	
</body>
```
数据绑定：

```html
<body>
	<div id="root">
		<div v-html="content"></div>
		// <div v-text="content"></div>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				content: "<h1>hello</h1>"
			}
		})
	</script>	
</body>
```

### 事件绑定

1、在标签中绑定事件：v-on:click="handleClick"  【v-on:click 可以简写为 @click】 
2、在 Vue 实例中定义 handleClick 事件方法
3、通过 this.content="world" 直接修改实例里面的数据

```html
methods: {
 
            handleClick: function() {
                alert("123");
            }
        }
```

```html
<body>
	<div id="root">
		<div v-on:click="handleClick">{{content}}</div>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				content: "hello"
			},
			methods: {
				handleClick: function() {
					alert("123");
					this.content="world";
				}
			}
		})
	</script>	
</body>
```
## 四、Vue 中的属性绑定和双向数据绑定

## 属性绑定 

> 语法：":属性"  
> v-bind:title 可以简写为: ":title"

Vue 语法中，模板指令 "v-bind:title" 后面跟着的就不是字符串了，而是一个 js 表达式。 可以写为 ： v-bind:title="'Jerry Li ' + title"

v-bind:title="title"  第二个 title 指的是 Vue 模板属性中的 title 所对应的值。

> 

```html
<body>
	<div id="root">
		<div v-bind:title="title">Hello world</div>
	</div>
	<script type="text/javascript"> 
		new Vue({
			el: "#root",
			data: {
				title: "This is hello world"
			}
		})
	</script>	
</body>
```
### 双向数据绑定

> 语法："v-model" 

v-model="content" 双向数据绑定，会改变 Vue 实例里面 "content" 的值，而 <div>标签里面的 "{{content}}" 引用 Vue 实例里面的值，所以关联发生变化。

```html
<body>
	<div id="root">
		<input v-model="content" />
		<div>{{content}}</div>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				content: "This is content"
			}
		})
	</script>	
</body>
```
## 五、Vue 中的计算属性和侦听器

### 计算属性
> 语法："computed" ，属性计算，一个属性的值是由其他数据项计算出来的新的结果。    
> 只有计算的属性发生变化才会重新计算，如果没有重新计算则使用上次的计算结果，可以提高计算的效率。

例如一个输入姓名的的输入框，最后要显示 "{{firstName}}{{lastName}}"，但是这种表达方式不太合理

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>计算属性与侦听器</title>
	<script src="./vue.js" type="text/javascript"></script>
</head>
<body>
	<div id="root">
		姓：<input v-model="firstName">
		名：<input v-model="lastName">
		<div>{{firstName}}{{lastName}}</div>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				firstName: '',
				lastName: ''
			}
		})
	</script>	
</body>
</html>
```

使用计算属性 computed，如果 firstName 或 lastName 其中一个发生变化才会重新触发计算，如果都没变化就会取默认缓存的速度，可以提高计算效率
和 react 中的 reselect 库的属性类似。

```html
<body>
	<div id="root">
		姓：<input v-model="firstName">
		名：<input v-model="lastName">
		<div>{{fullName}}</div>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				firstName: '',
				lastName: '',
				count: 0
			},
			computed: {
				fullName: function() {
					return this.firstName + ' ' + this.lastName
				}
			}
		})
	</script>	
</body>
```
### 侦听器

侦听器指的是监听某一个数据或计算属性的变化，一旦数据发生变化，就可以在侦听器里面做一些业务逻辑

> 语法："watch" 

侦听 firstName、lastName 属性的变化，也可以直接侦听 fullName 计算属性的变化

```html
<body>
	<div id="root">
		姓：<input v-model="firstName">
		名：<input v-model="lastName">
		<div>{{fullName}}</div>
		<div>{{count}}</div>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				firstName: '',
				lastName: '',
				count: 0
			},
			computed: {
				fullName: function() {
					return this.firstName + ' ' + this.lastName
				}
			},
			watch: {
				firstName: function() {
					this.count ++
				},
				lastName: function() {
					this.count ++
				}
			}
		})
	</script>	
</body>
```

```html
<body>
	<div id="root">
		姓：<input v-model="firstName">
		名：<input v-model="lastName">
		<div>{{fullName}}</div>
		<div>{{count}}</div>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				firstName: '',
				lastName: '',
				count: 0
			},
			computed: {
				fullName: function() {
					return this.firstName + ' ' + this.lastName
				}
			},
			watch: {
				fullName: function() {
					this.count ++
				}
			}
		})
	</script>	
</body>
```
## 六、v-if, v-show 与 v-for 指令

- v-if ：每次执行的时候标签会在 dom 目录把标签删除，性能较低，但是如果只使用一次的话，可以考虑使用这个
- v-show ：不会在 dom 目标把标签删除，会加一个 "display:none" 的属性，效率比较高

```html
<body>
	<div id="root">
		<div v-if="show">Hello world</div>
		<button @click="handleClick">toggle</button>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data : {
				show: true
			},
			methods: {
				handleClick: function() {
					this.show = !this.show;
				}
			}
		})
	</script>	
</body>
```

```html
<body>
	<div id="root">
		<div v-show="show">Hello world</div>
		<button @click="handleClick">toggle</button>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data : {
				show: true
			},
			methods: {
				handleClick: function() {
					this.show = !this.show;
				}
			}
		})
	</script>	
</body>
```
- v-for : 表单渲染。
1、在 Vue 实例里面定义一个数组 list: [1, 2, 3]；
2、在标签里面循环读取里面的元素，循环渲染 <li v-for="(item, index) of list" :key="index">{{item}}</li>

```html
<body>
	<div id="root">
		<div v-if="show">Hello world</div>
		<button @click="handleClick">toggle</button>
		<ul>
			<li v-for="(item, index) of list" :key="index">{{item}}</li>
		</ul>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data : {
				show: true,
				list: [1, 2, 3]
			},
			methods: {
				handleClick: function() {
					this.show = !this.show;
				}
			}
		})
	</script>	
</body>
```

