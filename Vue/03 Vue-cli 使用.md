# Vue-cli 使用

## 一、vue-cli 简介与使用

官网安装：https://cn.vuejs.org/v2/guide/installation.html

Vue 提供了一个官方命令行工具，可用于快速搭建大型单页应用，改工具为现代化的前端开发工作流程提供了开箱即用的构建配置。只需要几分钟即可创建并启动一个带热重载、保存静态检查以及可用于生产环境的构建配置的项目：

> 注意： 安装这个之前要先按照 node 和 npm，可以先执行命令 node -v ，npm -v, 看看是否已经安装
> 安装 node 会自动安装 npm

1. 全局安装 vue-cli  
sudo npm install --global vue-cli  
2. 创建一个基于 webpack 模板的新项目  
sudo vue init webpack my-project  
3. 安装依赖  
cd my-project  
npm run dev  

在本地运行，自动生成链接：http://localhost:8080/

项目的目录结构：
- build 文件夹：放 webpack 的配置文件，可以先不动
- config 文件夹：针对开发环境和线上环境的配置文件
- node_modules： 项目的依赖
- src 文件夹：源代放置
- static 文件夹：静态资源
- .babelrc、.editorconfig、eslintignore、eslintrc.js

src 文件夹：源代放置
- main.js：整个项目的入口文件，创建一个 Vue 实例，Vue 的跟实例挂载点在 index.html 的 app 标签上

## 二、使用 vue-cli 开发 TodoList

启动 Vue 项目，在项目目录下，执行命令 npm run start 或 npm run dev

原因：在项目 webpack.json，已经有定义 script，执行时相当于运行里面的命令

```html
  "scripts": {
    "dev": "webpack-dev-server --inline --progress --config build/webpack.dev.conf.js",
    "start": "npm run dev",
    "lint": "eslint --ext .js,.vue src",
    "build": "node build/build.js"
  }
```

> 模板内容 template 下只能有一个根标签
## 三、全局样式与局部样式


## 四、课程总结