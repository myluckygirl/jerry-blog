
# Vue-cli3 全集

## Vue-cli 家庭成员及存在的意义

### 常见概念

Command-Line Interface: 又称命令行界面或字符用户界面

- cli2
- cli3

- 前台：以 css html 为基础的页面开发，重在页面的布局、美化
- 后台：包括但不限于以 Java 语言开发，也称为服务器的开发

前端和后端是属于前台的一个分支，都属于前台
- 前端：特指页面的开发 修饰 美化 页面的基本元素
- 后端：后端的服务，使页面能够正常跳转 页面直接可以进行变量共享 为页面更好的服务

### node.js
node.js ：给页面提供服务（后端）  
安装 ：https://www.nodejs.cn 

安装成功验证：   
node -v  
npm -v  
npm 是 node 引申出来的指令

### 常见的 dos 命令

- cd 打开文件夹
- md 创建新文件夹
- dir 查看文件夹内容
- cd .. 返回上一层文件夹
- d: 切换到 d 盘

### Vue-cli 安装

#### npm 与 cnpm 的区别

- npm (node package management) ：是 nodejs 的包管理器，用于 node 插件管理（包括安装、卸载、管理依赖等）

- cnpm ：因为 npm 安装插件是从国外服务器下载，受网络影响很大，可能出现异常，所以我们乐于分享的淘宝团队使用国内镜像来代替国外服务器

#### -g 参数

- 全局安装（global）,可以在命令行下直接使用
- 可以通过 npm root -g 查看全局安装的文件夹位置

#### Vue-cli 安装

- npm install -g vue-cli
- cnpm install -g vue-cli

手动指定从哪个镜像服务器获取资源
- npm install -gd express --registry=http://registry.npm.taobao.org

为了避免每次安装都需要 --registry 参数，可以使用如下命令进行永久设置
- npm config set registry http:// registry.npm.taobao.org

#### -S -D 参数

-S，--save 安装包信息将加入到 dependencies(生产阶段的依赖)
-D，--save--dev 安装包信息将加入到 devDependencies(开发阶段的依赖)，所以开发阶段一般用它
-i 是 install 的缩写（注意：前面没有"-"） 

## 使用 Vue-cli2 搭建项目

### 创建 vue-cli 项目

- vue init webpack projectName

创建 cli 过程中部分配置内容
- Install vue-router?(Y/N) 是否安装 vue-router，这是官方的路由（用于页面的跳转），大多数情况下都是使用，这里就输入 "Y" 后回车即可。
- Use ESLint to lint your code?(Y/N) 是否使用 ESLint 管理代码，ESLint 是个代码风格管理工具，是用来统一代码风格的，一般项目中都会使用。

成功后启动 vue-cli 项目, http://localhost:8080

## 使用 Vue-cli3 搭建项目及调试

## 使用 Vue-cli3 完成项目案例


