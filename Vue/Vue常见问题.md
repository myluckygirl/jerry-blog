

## 一、单独增加或者删除某些组件 eslint

默认安装在生产环境 dependencies，如果需要安装在测试环境，则需要 `--save-dev`

安装：
`npm install eslint --save-dev` 或 `npm i eslint --save-dev`

卸载：
`npm uninstall eslint --save-dev` 或 `npm uni eslint --save-dev`

运行完成后在 package.json 新增配置，如果要安装 vue-router ，则直接更换就行。

## 二、怎么导入 vue-cli 项目

1、切换到项目所在目录，运行 `npm install` 添加依赖；
2、运行 `npm run dev`, 查看项目运行起来

`npm -v`, `vue -V` 检查是否安装

index.js 定义的给 App.vue使用 `<router-view></router-view>`

