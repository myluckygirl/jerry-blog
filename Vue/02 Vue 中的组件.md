# Vue 中的组件

## 一、todolist 功能开发

1、定义一个 input 输入标签，和 Vue 实例里面的数据进行双向数据绑定 <input v-model="inputValue">；
2、定义一个空数组 list, 在列表里面循环读取展示 list 数组里面的值 <li v-for="(item, index) of list" :key="index">{{item}}</li>；
3、定义一个提交按钮，在点击时进行事件绑定，每次提交时将 input 标签输入的值 inputValue 插入到 list 数组中，且插入后清空 inputValue 的值

- ① 按钮提交时间  
<button @click="handleSubmit">提交</button>
- ② handleSubmit 时间处理，在方法进行业务处理。

```html
methods: {
        handleSubmit: function() {
            this.list.push(this.inputValue);
            this.inputValue=''
        }
    }
```

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vue 练习</title>
	<script src="./vue.js" type="text/javascript"></script>
</head>
<body>
	<div id="root">
		<input v-model="inputValue">
		<button @click="handleSubmit">提交</button>
		<ul>
			<li v-for="(item, index) of list" :key="index">{{item}}</li>
		</ul>
	</div>
	<script type="text/javascript">
		new Vue({
			el: "#root",
			data: {
				inputValue: '',
				list: []
			},
			methods: {
				handleSubmit: function() {
					this.list.push(this.inputValue);
					this.inputValue=''
				}
			}
		})
	</script>	
</body>
</html>
```

## 二、todolist 组件拆分

组件 ： 页面上的某一部分。大型的网页可以拆成几部分，就叫做组件。  

### 1. 如何定义组件？ 
- 全局组件
- 局部组件

全局组件： 
- 定义方式：Vue.component('组件名', {template: '模板内容'})
- 引用：<ul><todo-item></todo-item></ul> ，直接使用定义的组件标签
- 是否需要在 Vue 实例里面声明或注册：不需要

局部组件：
- 定义方式：var TodoItem = {template: '<li>item</li>'}
- 在 Vue 实例里面声明或注册：new Vue({el: "#root",components: {'todo-item': TodoItem} })
- 引用: <ul><todo-item></todo-item></ul> ，使用定义的组件标签
                  			
```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vue 练习</title>
	<script src="./vue.js" type="text/javascript"></script>
</head>
<body>
	<div id="root">
		<input v-model="inputValue">
		<button @click="handleSubmit">提交</button>
		<ul>
			<todo-item></todo-item>
		</ul>
	</div>
	<script type="text/javascript">

		// Vue.component('todo-item', {
		// template: '<li>item</li>'
		// })

		var TodoItem = {
			template: '<li>item</li>'
		}
		new Vue({
			el: "#root",
			components: {
				'todo-item': TodoItem
			},
			data: {
				inputValue: '',
				list: []
			},
			methods: {
				handleSubmit: function() {
					this.list.push(this.inputValue);
					this.inputValue=''
				}
			}
		})
	</script>	
</body>
</html>
```
### 2. 组件和组件直接如何通信？ 

Vue 中父组件向子组件是通过绑定属性的方式传值

1、在自定义标签里面，进行数据绑定 :content="item"  
2、在全局组件里面，使用 props 属性来接收外部传入的值 props: ['content'] 来接收外部传入名字为 content 的属性，一旦发生开始传送，自定义组件里面就可以开始使用

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vue 练习</title>
	<script src="./vue.js" type="text/javascript"></script>
</head>
<body>
	<div id="root">
		<input v-model="inputValue">
		<button @click="handleSubmit">提交</button>
		<ul>
			<todo-item 
				v-for="(item, index) of list" 
				:key="index"
				:content="item">
			</todo-item>
		</ul>
	</div>
	<script type="text/javascript">

		 Vue.component('todo-item', {
		 	props: ['content'],
			template: '<li>{{content}}</li>'
		 })
		 
		new Vue({
			el: "#root",
			data: {
				inputValue: '',
				list: []
			},
			methods: {
				handleSubmit: function() {
					this.list.push(this.inputValue);
					this.inputValue=''
				}
			}
		})
	</script>	
</body>
</html>
```

## 三、组件与实例的关系

- 每个组件都是 Vue 的实例, 反过来每个实例其实也是组件
- Vue 是由一个一个实例组成的
- 一个实例由 el、props、data、template、methods、computed、watch 等属性组成。跟实例如果没有定义 template，则是默认取绑定标签下的所有 dom 为模板内容。

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vue 练习</title>
	<script src="./vue.js" type="text/javascript"></script>
</head>
<body>
	<div id="root">
		<input v-model="inputValue">
		<button @click="handleSubmit">提交</button>
		<ul>
			<todo-item 
				v-for="(item, index) of list" 
				:key="index"
				:content="item">
			</todo-item>
		</ul>
	</div>
	<script type="text/javascript">

		 Vue.component('todo-item', {
		 	props: ['content'],
			template: '<li @click="handleClick">{{content}}</li>',
			methods: {
				handleClick: function() {
					alert("测试一下")
				}
			}
		 })

		new Vue({
			el: "#root",
			data: {
				inputValue: '',
				list: []
			},
			methods: {
				handleSubmit: function() {
					this.list.push(this.inputValue);
					this.inputValue=''
				}
			}
		})
	</script>	
</body>
</html>
```

## 四、实现 todolist 的删除功能

- 父组件向子组件传值：Vue 中父组件向子组件是通过属性的方式传值
- 子组件向父组件：通过发布订阅的模式，子组件发布事件，父组件订阅监听的方式


1. this.$emit('delete', this.index) : 子组件向外触发了一个自定义事件 delete，这个事件携带了一个 this.index 值
2. @delete="handleDelete" ：父组件创建时监听了子组件向外发出的 delete 事件，如果父组件监听到了事件，则触发 handleDelete 事件
3. handleDelete: function(index) {this.list.splice(index, 1)} ：父组件在 methods 定义 handleDelete 事件的业务处理

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Vue 练习</title>
	<script src="./vue.js" type="text/javascript"></script>
</head>
<body>
	<div id="root">
		<input v-model="inputValue">
		<button @click="handleSubmit">提交</button>
		<ul>
			<todo-item 
				v-for="(item, index) of list" 
				:key="index"
				:content="item"
				:index="index"
				@delete="handleDelete">
			</todo-item>
		</ul>
	</div>
	<script type="text/javascript">

		 Vue.component('todo-item', {
		 	props: ['content', 'index'],
			template: '<li @click="handleClick">{{content}}</li>',
			methods: {
				handleClick: function() {
					// alert(this.index),
					this.$emit('delete', this.index)
				}
			}
		 })
		new Vue({
			el: "#root",
			data: {
				inputValue: '',
				list: []
			},
			methods: {
				handleSubmit: function() {
					this.list.push(this.inputValue);
					this.inputValue=''
				},
				handleDelete: function(index) {
					this.list.splice(index, 1)
				}
			}
		})
	</script>	
</body>
</html>
```
