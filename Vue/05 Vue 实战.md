
# Vue-cli4 脚手架搭建

随着 vue.js 越来越火爆，更多项目都用到了 vue 进行开发，在实际的开发项目中如何搭建开发的脚手架呢，今天跟大家分享一下：

首先需要了解知识：
    - HTML
    - Css
    - JavaScript
    - Node.js 环境（npm 包管理工具）
    - Webpack 自动化构建工具

vue-cli 提供一个官方命令行工具，可用于快速搭建大型单页应用

1、安装：
    cnpm install -g @vue/cli

验证是否安装成功   
    vue -V   

2、搭建项目   
     vue create projectName   

3、运行  
    npm run dev (3.0 以下版本运行)  
    npm run serve (3.0 以上版本运行)  


node_modules: 存放下载的依赖文件（cnpm install）
public: 存放公共资源
yarn.lock: 项目文件备份，避免意外的修改
babel.config.js:
package.json: 


`new Date().toLocaleString()` ： 获取当前时间
`this.message = this.message.split('').reverse().join('')` : 消息逆转
MVVM 模型 ：
vm (ViewModel 的缩写) ：这个变量名表示 Vue 实例。

### 路由：
```html
<div>
    <!-- 路由展示 -->
    <div-view />
    <!-- 底部导航 -->
    <div class="nav-button">
        <router-link to="/home">
            <label>首页</label>
        </router-link>
    </div>
</div>
```

### 配置文件

手动添加： vue.config.js
```html
modul.exports={
    devServer:{
        port：8080, // 端口
        host:"localhost", // 访问IP
        open:true //配置浏览器自动访问
    }
}
```

### 字体图标引用

阿里巴巴字体图标库：https://www.iconfont.cn/  

图标放在 assets 目录下

```html
<div>
    <!-- 路由展示 -->
    <div-view />
    <!-- 底部导航 -->
    <div class="nav-button">
        <router-link to="/home">
            <!--配置字体图标-->
            <i class="iconfont iconfenlei"></i>
            <label>首页</label>
        </router-link>
    </div>
</div>
```


- 默认路由样式定义
- 组件的定义及传参
- 导航栏模块
- 轮播图模块

:style  动态加载样式
<i class="iconfont" :class="list.icon" :style="{background:list.color}"></i>

- {{}} : 表达式，按照 js 解析，输出对应的内容
- {} : 以键值对的形式解析

#### 地址解析：

```html

<div class="banner">
    <img alt="Vue logo" v-for"(v,i) in imgArr" :key="i" :src="v" v-show="n==i">
    <div class="banner-circle">
        <ul>
            <li></li>
        </ul>
    </div>
</div>

data() {
    return {
        timer:null,
        n=0,
        // 定义图片数组
        imgArr:[
            require('../assert/img/1.jpg),
            require('../assert/img/2.jpg),
            require('../assert/img/3.jpg)
        ]       
    }
},
methods: {
    play() {
        // 添加定时器，设置时间间隔 n 参数每隔2s n++
        this.timer = setInterval(this.autoPlay,2000)
    },
    autoPlay() {
        this.n++;
        // 等于数组长度时恢复0
        if(this.n == this.imgArr.length) {
            this.n=0;
        }
    }
},
// 挂载完成
mounted:function() {
    // 钩子函数 挂载 一进来就调用
    this.play();
},
// 销毁
destoryed:function() {
    clearInterval(this.timer);
}

```


