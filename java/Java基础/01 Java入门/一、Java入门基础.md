# 一、Java入门

## 计算机语言发展经历的三个阶段

- 第一代：机器语言
- 第二代：汇编语言
- 第三代：高级语言

> 1. 机器语言：就是 0/1 代码。计算机只能识别 0 和 1。难懂、难记忆
> 2. 汇编语言：就是将一串很枯燥无味的机器语言转化成一个英文单词。比如说：add 1, 2;
> 3. 高级语言：分为两个阶段，以 1980 年为分界线，前一阶段属于结构化语言或者称为面向过程的语言（如：最典型、最重要的C语言），后一阶段属于面向对象的语言（如：C++、C#、Java）。

## Java发展简史

## Java的三个版本

* **Java SE（Java Standard Edition**）：标准版。它允许开发和部署在桌面、服务器、嵌入式环境和实时环境中使用的 Java 应用程序。Java SE 包含了支持 Java Web 服务开发的类，并为 Java EE 提供基础。

* **Java EE（Java Enterprise Edition）**：企业版。帮助开发和部署可移植、健壮、可伸缩且安全的服务器端Java 应用程序。Java EE 是在 Java SE 的基础上构建的，它提供 Web 服务、组件模型、管理和通信 API，可以用来实现企业级的面向服务体系结构（service-oriented architecture，SOA）和 Web2.0 应用程序。

* **Java ME（Java Micro Edition）**：微型版。Java ME 为在移动设备和嵌入式设备（比如手机、PDA、电视机顶盒和打印机）上运行的应用程序提供一个健壮且灵活的环境。Java ME 包括灵活的用户界面、健壮的安全模型、许多内置的网络协议以及对可以动态下载的连网和离线应用程序的丰富支持。基于 Java ME 规范的应用程序只需编写一次，就可以用于许多设备，而且可以利用每个设备的本机功能。

> 提示：
JavaSE 是基础，要做 JavaEE、JavaMe 必须先要学习 JavaSE。

## Java的特性和优势

- 跨平台、可移植性
- 安全性
- 面向对象
- 简单性
- 高性能
- 分布式
- 多线程
- 健壮性

## Java 应用程序的运行机制

1. 首先利用文本编辑器编写 Java 源程序，源文件的后缀名为 .java；
2. 再利用编译器（javac）将源程序编译成字节码文件，字节码文件的后缀名为 .class；
3. 最后利用虚拟机，解释执行。

![Java运行机制](../../../img/java/JavaSE/JavaRun.png)

## JDK、JRE、JVM 的区别和作用

建议：如果只要运行 Java 程序，只需要 JRE 就可以。JRE 通常非常小，其中包含了JVM。如果要开发 Java 程序，就需要安装JDK。

JVM（Java Virtual Machine）就是虚拟的用于执行 bytecode 字节码的“虚拟计算机”

JRE（Java Runtime Environment）Java 运行时环境，包含：Java 虚拟机、库函数、运行 Java 英语程序所必须的文件

JDK（Java Development Kit）Java 开发包，包含JRE，以及增肌编译器和调试器等用于程序开发的文件

## JDK 下载&安装&环境配置

[JDK 安装参考](https://gitee.com/pifu/gorge4j-blog/blob/master/java/java%E5%BC%80%E5%8F%91%E5%9F%BA%E7%A1%80/Java%E5%BC%80%E5%8F%91%E5%9F%BA%E7%A1%80%E7%AC%AC01%E8%AE%B2-JDK%E5%AE%89%E8%A3%85.md)

## Java 程序基本结构

### 第一个 Java 程序

HelloWorld.java
```java
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
```
#### 类名要求

- 类名必须以英文字母开头，后接字母，数字和下划线的组合；
- 习惯以大写字母开头；

#### 方法
- 方法名命名规则和 class 一样，但是首字母小写；
- 一个类内部可以定义若干个方法；
- 方法定义了一组执行语句，方法内部的代码将会被依次顺序执行；
- 在方法内部，语句才是真正的执行代码。Java 的每一行语句必须以分号结束；

#### Java 入口程序规定

- 方法必须是静态方法
- 方法名必须为 main
- 括号内的参数必须是 String 数组
> 固定写法：public static void main(String[] args) {}

#### 注释

> 在 Java 程序中，注释是一种给人阅读的文本，不是程序的一部分，所以编译器会自动忽略注释。

**编写注释的意义**
- 增加代码的可读性。说明某段代码的作用，或者某个类的用途、某个方法的功能，以及该方法的参数和返回值的数据类型及意义等；
- 提高团队合作的效率；
- 代码即文档。程序源代码是程序文档的重要组成部分。

**Java 的注释三种注释方式**
- 单行注释（// 之后便是单行注释）
- 多行注释（/* 在这之间都是注释，可跨多行 */）
- 文档注释（/** 在这之间都是文档注释 */）

**使用 Javadoc 工具生成 API 文档**

> 可以利用 Javadoc 工具将 Java 源代码中的文档注释自动转化成 API 文档。   

javadoc 命令的基本用法

```
javadoc 选项 java 源文件|包
```


上述语法格式中，javadoc 支持通配符，比如 *.java 就表示当前路径下所有 java 文件。javadoc 常用选项有如下几个：

- **-d(directory)**：指定一个路径，用于将生成的 API 文档放到指定目录下；  
- **-windowtitle(text)**：指定一个字符串，用于设置 API 文档的浏览器窗口标题。  
- **-doctitle(html-code)**：指定一个 HTML 格式的文本，用于指定概述页面的标题。（注意：只有对处于多个包下的源文件来生成 API 文档时，才有概述页面）  
- **-header(html-code)**：指定一个 HTML 格式的文本，包含每个页面的页眉。  
- 除此之外，javadoc 命令还包含了大量其他选项，我们可以通过在命令行窗口执行 javadoc   
- **-help**：查看 javadoc 命令的所有选项。

举个栗子：
```
javadoc -d api_doc -windowtitle 测试API -doctitle 学习javadoc -header 我的类 *Test.java
```
> **说明**：API 文档类似于产品的使用说明书，通常只需要介绍那些暴露的、供用户使用的部分，在 java 类中就是以 public 或 protected 修饰的内容，因此 javadoc 默认只处理 public 或 protected 修饰的内容。如果开发者确实希望可以提取 private 修饰的内容，则可以在命令行使用 javadoc 工具时增加 -private 选项。还有，javadoc 工具只处理文档源文件在类、接口、方法、成员变量、构造器和内部类之前的文档注释，忽略其他地方的文档注释。

注释练习题：Annotate.java

```java
/**
 *
 * 1、文档注释（Javadoc 注释）
 * [Java语言的基本语法 - 注释] 这里是 javadoc 注释，生成 API 文档时使用，注释在编译时会被忽略
 *
 * @author jerry.li
 * @create 2019/10/19
 * @since 1.0.0
 */
public class Annotate {
    /*
     * 2、这里是
     * 多行
     * 注释
     */
    public static void main(String[] args) {
        // 3、这里是当行注释
        System.out.println("Annotate test!");
    }
}
```
 # 二、Java 语言的基本语法
 
 ## 标识符
 
1. Java 里的标识符只能是大/小写字母、数字、美元符号、下划线及上述组合；
2. 不能以数字开头；
3. 不能为 Java 关键字；
4. 最大长度为 65534 个字符;
5. 所有编程相关的命名均不能以下划线或美元符号开始;
6. 方法名、参数名、成员变量、局部变量都统一使用 lowerCamelCase，必须遵从驼峰形式

标识符小练习：Identifier.java
```java
// 类名 Identifier 是标识符，类名以大写字母开头
public class Identifier {
    // 方法名 main 是标识符，方法名以小写字母开头
    public static void main(String[] args) {
        // Java里的标识符可以是字母，变量名a是标识符，下同
        int a = 10;
        // Java里的标识符区分大小写，变量a与A是不同的变量
        int A = 20;
        // Java 里的标识符可以是汉字（Java 里标识符支持 Unicode 编码，理论上其它国家文字也支持）
        int 中国 = 30;
        // Java 里的标识符可以是下划线
        int _b = 40;
        // Java 里的标识符可以是美元符号
        int $c = 50;
        // Java 里的标识符可以是数字（注意不能以数字开头）
        int d4 = 60;
        // Java 里的标识符只能是大/小写字母、数字、美元符号、下划线及上述组合，且不能以数字开头，不能为 Java 关键字，最大长度为 65534 个字符
        int aA中国$_4 = 70;
        // 标识符以驼峰名字方式，这里 dayOfYear 是变量，365 是常量，此行语句的作用是将常量 365 赋值给变量 dayOfYear
        int dayOfYear = 365;
        // 这里的 isMondayOfWeek 是变量， false 是常量，此行语句的作用是将常量 false 赋值给变量 isMondayOfWeek
        boolean isMondayOfWeek = false;

        // 以下是各个变量值的打印
        System.out.println("a的值是：" + String.valueOf(a));
        System.out.println("A的值是：" + String.valueOf(A));
        System.out.println("中国的值是：" + String.valueOf(中国));
        System.out.println("_b的值是：" + String.valueOf(_b));
        System.out.println("$c的值是：" + String.valueOf($c));
        System.out.println("d4的值是：" + String.valueOf(d4));
        System.out.println("aA中国$_4的值是：" + String.valueOf(aA中国$_4));
        System.out.println(dayOfYear);
        System.out.println(isMondayOfWeek);
    }
}
```
## Java数据类型

![Java数据类型](../../../img/java/JavaSE/JavaType.png)

- 基本数据类型（Primitive Type）
- 对象数据类型（也叫引用数据类型，Object Type）

**一、基本数据类型分为4大类**
1. **逻辑型 - boolean** (Logical，也称为布尔型，1 个字节)
2. **字符型 - char** (Textual，2 字节)
3. **整型** (Integral)
> byte (1 个字节，-128～127)<br/>
> short (2 个字节，-32768～32767) <br/>
> int (4 个字节，-2147483648~2147483647) <br/>
> long (8 个字节，-9223372036854775808~9223372036854775807)
4. **实数类型** (Floating，也称为浮点数)
> float (单精度类型，4 个字节，-1.45E~3.4028235E+38) <br/>
> double (双精度类型，8 个字节，-4.9E-324~1.7976931348623157E+308)

|简单类型| boolean| byte| char| short| Int| long| float| double| void|
|:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|占用内存| 1字节| 1字节 |2字节| 2字节| 4字节| 8字节| 4字节| 8字节| --|
|二进制位数 |1| 8| 16| 16| 32| 64| 32| 64| --|
|封装器类 |Boolean |Byte |Character| Short| Integer| Long |Float |Double| Void|

**二、对象数据类型（引用数据类型）**
- **类**：class
- **接口**：interface
- **数组**：[]

1、基本数据类型小练习：PrimitiveType.java

```java
public class PrimitiveType {
    public static void main(String[] args) {

        // 1、[Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 字符型]Textual-char类型
        // 1.1 Java 使用字符型存储单个字符，占2个字节内存
        char a, b, c;
        a = 'a';
        b = '中';
        c = ' ';
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println("-------------------------------------------------");

        // 2、[Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 逻辑型] 逻辑型也称为布尔型（boolean）
        // 逻辑型是表达真（true）假（false）的数据类型，在内存中占一个字节
        boolean d = true;
        boolean e = false;
        System.out.println(d);
        System.out.println(e);
        System.out.println("-------------------------------------------------");

        // 3、[Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 整型]：byte/short/int/long
        // 3.1 byte，在内存中占一个字节，取值范围：-128~127
        byte by = 1;
        // 3.2 short，在内存中占2个字节，取值范围：-32768~32767
        short sh = 32000;
        // 3.3 int，在内存中占4个字节，取值范围：-2147483648~21474836477
        int in = -2100000000;
        // 3.4 long，在内存中占8个字节，取值范围：-9223372036854775808~9223372036854775807。定义时 L 不能漏，也可用小写 "l"
        long lo = 1234567890123456890L;

        System.out.println(by);
        System.out.println(sh);
        System.out.println(in);
        System.out.println(lo);
        System.out.println("-------------------------------------------------");

        // 4、[Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 实数型] | 实数型示例：float/double
        // 4.1 float，在内存中占4个字节，取值范围：-1.45E~3.4028235E+38。注意 “f” 不能省略
        float fl = 1.23f;
        // 4.2 double，在内存中占8个字节，取值范围：-4.9E-324~1.7976931348623157E+308。1.23 是 double 型常数，此处如定义成 float
        // 型变量是错误的，就比如不能把一个大容器放到小容器里一样
        double dou = 1.23;
        System.out.println(fl);
        System.out.println(dou);
        System.out.println("-------------------------------------------------");

    }
}
```
2、进制转换小练习：HexadecimalConversion.java

```java
public class HexadecimalConversion {
    private static char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static void main(String[] args) {
        int number = 20;
        System.out.println(number + " 转二进制: " + toNumber(number, 2));
        System.out.println(number + " 转八进制: " + toNumber(number, 8));
        System.out.println(number + " 转十六进制: " + toNumber(number, 16));

        // 整型转二进制
        System.out.println(Integer.toBinaryString(number));
        // 整型转八进制
        System.out.println(Integer.toOctalString(number));
        // 整型转十六进制
        System.out.println(Integer.toHexString(number));

    }

    public static String toNumber(int number, int n) {
        String str = "";
        for (int i = 0; i < n; i++) {
            if (number == i) {
                str = a[i] + str;
                return str;
            }
        }
        str = a[number % n] + str;
        str = toNumber(number / n, n) + str;
        return str;
    }
}
```
## 三、基本数据类型之间转换

Java 语言是一种强类型的语言。强类型的语言有以下几个要求：
- **变量或常量必须有类型**：要求声明变量或常量时必须声明类型，而且只能在声明以后才能使用。
- **赋值时类型必须一致**：值的类型必须和变量或常量的类型完全一致。
- **运算时类型必须一致**：参与运算的数据类型必须一致才能运算。

> 但是在实际的使用中，经常需要在不同类型的值之间进行操作，这就需要一种新的语法来适应这种需要，这个语法就是**数据类型转换**。
> 
> 在数值处理这部分，计算机和现实的逻辑不太一样，对于现实来说，1和 1.0 没有什么区别，但是对于计算机来说，1 是整数类型，而 1.0 是小数类型，其在内存中的存储方式以及占用的空间都不一样，所以类型转换在计算机内部是必须的。

### Java 语言中的两种数据类型转换

- **自动类型转换**：编译器自动完成类型转换，不需要在程序中编写代码。
- **强制类型转换**：强制编译器进行类型转换，必须在程序中编写代码。

> 由于基本数据类型中 boolean 类型不是数字型，所以基本数据类型的转换是出了 boolean 类型以外的其它 7 种类型之间的转换。

### 数据类型转换规则

**1、自动类型转换**

> 自动类型转换，也称隐式类型转换，是指不需要书写代码，由系统自动完成的类型转换。由于实际开发中这样的类型转换很多，所以Java语言在设计时，没有为该操作设计语法，而是由 JVM 自动完成。

**转换规则**：从存储范围小的类型到存储范围大的类型。

具体规则为：**byte → short (char) → int → long → float → double**

也就是说 byte 类型的变量可以自动转换为 short 类型，示例代码：


```
byte b=10;
short sh=b;
```

这里在给 sh 赋值时，JVM 首先将 b 的值转换成 short 类型然后再赋值给 sh。

当然，在类型转换的时候也可以跳跃，就是 byte 也可以自动转换为 int 类型的。

> 注意问题：在整数之间进行类型转换的时候数值不会发生变化，但是当将整数类型特别是比较大的整数类型转换成小数类型的时候，由于存储精度的不同，可能会存在数据精度的损失。

**2、强制类型转换**

> 强制类型转换，也称显式类型转换，是指必须书写代码才能完成的类型转换。该类类型转换很可能存在精度的损失，所以必须书写相应的代码，并且能够忍受该种损失时才进行该类型的转换。

**转换规则**:从存储范围大的类型到存储范围小的类型。

具体规则为：**double → float → long → int → short (char) → byte**

语法格式为：**(转换到的类型)需要转换的值**


```
double d=3.14;
int i=(int) d;
```


> 注意问题:强制类型转换通常都会存储精度的损失，所以使用时需要谨慎。

数据类型转化小练习：Casting.java

```java
public class Casting {
    public static void main(String[] args) {
        byte a = 7;
        int b = 2;
        double c = 3.15;
        // 按照自动类型转换的规则，a / b 会按照 int/int 的形式来进行运算，就是结果还是 int，后面 int / int + double 才会转换为 double类型
        double d = a / b + c;
        // (double) a 将变量 a 强制转换为双精度 double 类型
        double e = (double) a / b + c;
        // 结果：6.15
        System.out.println(d);
        // 结果：6.65
        System.out.println(e);

        // String 对象与基本数据类型的数据可以进行 ”+” 运算，其结果值为一个 String 型的对象，即”字符串+整数（实数）=字符串”。
        String s = "您好" + 12;
        System.out.println(s);

        byte b1 = 1;
        byte b2 = 2;
        // byte 和 byte 运算结果 int
        int i1 = b1 + b2;

        byte b3 = 3;
        short s1 = 4;
        // byte 和 short 运算结果 int
        int i2 = b3 + s1;

        byte b4 = 5;
        int i3 = 6;
        // byte 和 int 运算结果 int
        int i4 = b4 + i3;

        int i5 = 7;
        int i6 = 8;
        // int 和 int 运算结果 int
        int i7 = i5 + i6;

        int i8 = 9;
        long l1 = 10L;
        // int 和 long 运算结果 long
        long l2 = i8 + l1;

        long l3 = 11L;
        long l4 = 12L;
        // long 和 long 运算结果 long
        long l5 = l3 + l4;

        byte b5 = 13;
        float f1 = 1.5f;
        // byte 和 float 运算结果 float
        float f2 = b5 + f1;

        int i9 = 14;
        float f3 = 2.5f;
        // int 和 float 运算结果 float
        float f4 = i9 + f3;

        long l6 = 15;
        float f5 = 3.5f;
        // long 和 float 运算结果 float，特别注意：float（4个字节） 类型是比 long 类型（8个字节）更大的数据类型，主要原因是 float 能表示小数
        float f6 = l6 + f5;

        float f7 = 4.5f;
        double d1 = 1.5d;
        // float 和 double 运算结果 double
        double d2 = f7 + d1;

        double d3 = 2.5d;
        double d4 = 3.5d;
        // double 和 double 运算结果 double
        double d5 = d3 + d4;

        byte b6 = 33;
        // 单个操作符也能产生强制类型转换
        int i10 = b6 >> 1;

        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i4);
        System.out.println(i7);
        System.out.println(l2);
        System.out.println(l5);
        System.out.println(f2);
        System.out.println(f4);
        System.out.println(f6);
        System.out.println(d2);
        System.out.println(d5);
        System.out.println(i10);
    }
}
```
# 三、运算符

## 运算符优先级

运算符的优先级（由高到低）：
1. 单目运算符（!、~、++、--）
1. 算术运算符（*、/、%、+、-）
1. 位移运算符（<<、 >>、 >>>）
1. 关系元算符（< 、<= 、>、 >=、==、!=）
1. 逻辑元算符（&&、||、&、|、^）
1. 三目运算符（?=）
1. 赋值运算符（=、+=、-=、*=、/=、%=、&=、|=、^=、<<=、>>=、>>>=）

速记口诀：单算位关逻三赋

> **注意：小括弧 () 的优先级最高**


1、运算符小练习1：Operator.java

```java
public class Operator {
    public static void main(String[] args) {

        // 1、逻辑非（!）：单目运算符，把真（true）变为假（false），把假（false）变为真（true），例如：!true为假，!false为真
        boolean b = true;
        System.out.println(!b);
        System.out.println(!!b);
        System.out.println("---------------------");

        // 2、位非运算符：是表示位非的单目运算符，它的运算规则是：逢1变0，逢0变1
        int a2 = 7;
        // 结果为 -8，位非运算符 ~ 是表示位非的单目运算符，它的运算规则是：逢1变0，逢0变1
        int b2 = ~a2;
        System.out.println(b2);
        System.out.println("---------------------");

        // 3、符号运算符 +、- ：分别用于表示正数和负数
        int a3 = 7;
        int b3 = -a3;
        System.out.println(b3);
        System.out.println("---------------------");

        // 4、增减运算符 ++、-- 是使变量值增 1 或减 1 的单目运算符
        int a4 = 10, b4 = 20;
        // ++ 使变量值增1。需注意运算符跟变量之间不要有空格，没有语法错误，但是写法不规范，不推荐
        a4++;
        System.out.println("a4 = " + a4);
        // -- 使变量值减1。需注意运算符跟变量之间不要有空格，没有语法错误，但是写法不规范，不推荐
        b4--;
        System.out.println("b4 = " + b4);
        System.out.println("---------------------");

        int a5 = 10, b5 = 10;
        // 前置递增运算符，先运算后赋值
        int c5 = ++a5;
        // 后置递增运算符，先赋值后运算
        int d5 = b5++;
        System.out.println("a5 = " + a5);
        System.out.println("b5 = " + b5);
        System.out.println("c5 = " + c5);
        System.out.println("d5 = " + d5);
        System.out.println("---------------------");

        int a6 = 10, b6 = 10;
        // 前置递减运算符，先运算后赋值
        int c6 = --a6;
        // 后置递减运算符，先赋值后运算
        int d6 = b6--;
        System.out.println("a6 = " + a6);
        System.out.println("b6 = " + b6);
        System.out.println("c6 = " + c6);
        System.out.println("d6 = " + d6);
        System.out.println("---------------------");

        // 5、算术运算符指加 +、减 -、乘 *、除 /，还包括求余运算符 %
        int a7 = 20;
        int b7 = 3;
        int c7 = a7 / b7;
        int d7 = a7 % b7;
        System.out.println("20 除以 3 的商是：" + c7);
        System.out.println("20 除以 3 的余数是：" + d7);
        System.out.println("---------------------");

        // 6、位移运算符 <<、>>、>>> 用来移动比特位，向左移动n位，相当于乘以2的n次方；向右移动n位，相当于除以2的n次方
        int a8 = 100;
        // 表示左移 1 位，数值增加 1 倍，相当于 a * 2
        System.out.println(a8 << 1);
        // 表示左移 2 位，数值增加 2 倍，相当于 a * 4
        System.out.println(a8 << 2);
        // 表示右移 1 位，数值减半，相当于 a / 2
        System.out.println(a8 >> 1);
        // 表示右移 2 位，数值减半再减半，相当于 a / 4
        System.out.println(a8 >> 2);
        // 表示无符号右移 1 位，无论被移的数是正数还是负数，最左端均填 0
        System.out.println(a8 >>> 1);
        System.out.println();
        int b8 = -100;
        // 表示左移 1 位，数值增加 1 倍，相当于 b * 2
        System.out.println(b8 << 1);
        // 表示左移 2 位，数值增加 2 倍，相当于 b * 4
        System.out.println(b8 << 2);
        // 表示右移 1 位，数值减半，相当于 b / 2
        System.out.println(b8 >> 1);
        // 表示右移 2 位，数值减半再减半，相当于 b / 4
        System.out.println(b8 >> 2);
        // 表示无符号右移 1 位，无论被移的数是正数还是负数，最左端均填 0，因此负数无符号右移后将得到正数
        System.out.println(b8 >>> 1);
        System.out.println();
        // 特别注意，位移运算符操作的只是变量的副本，变量的原值不会发生变化
        System.out.println("a8 的值是：" + a8);
        System.out.println("b8 的值是：" + b8);
        System.out.println("---------------------");

    }
}
```

## 关系运算符

**>=、<=、==、!=** ：用来比较两个操作数的大小/相等/不相等等关系，比较所得的结果值为 boolean 型，它经常也成为比较运算符，常用在条件语句中

### && 运算（只要有一个假就为假）

|A | B | A && B|
|:---:|:---:|:---:|
|true | true | true|
|true | false | false|
|false | true | false|
|false | false | false|

### || 运算（只要有一个真就为真）

|A | B | A &#124;&#124; B|
|:---:|:---:|:---:|
|true | true | true|
|true | false | true|
|false | true | true|
|false | false | false|

- 快速逻辑与（&&）、快速逻辑或（||）称为短路运算符
- A && B：若 A 为 false，则整个表达式即为假，此时 B 将不会参与运算，因此将出现 false; 
- A || B：若 A 为 true，则整个表达式为真，此时 B 将不会参与运算，因此将出现 true;
- 概率最高的表达式放到前面是推荐的做法。

### 三目运算符
- 格式：**条件式 ? 值1 : 值2**
- 如果条件表达式为真，则表达式的值取值1，否则取值2
- 注意：三目运算符可以嵌套

### 赋值运算符
> 运算符 = 就是赋值运算符。除 = 之外，还有 +=、-=、*=、/=、&=、^=、|= 等赋值运算符，一般将 = 运算符称为纯赋值运算符

### 对象运算符
> 对象运算符 instanceof 结果值为 boolean 型，A instanceof B：如果 A 是 B 的对象，则返回 true，否则，返回 false

关系运算符练习：Operator2.

```java
public class Operator2 {
    public static void main(String[] args) {

        // 1、关系运算符 、>=、<=、==、!= 用来比较两个操作数的大小/相等/不相等等关系，比较所得的结果值为 boolean 型，它经常也成为比较运算符，常用在条件语句中
        int a1 = 1;
        int b1 = 2;
        System.out.println(a1 == b1);
        System.out.println(a1 > b1);
        System.out.println(!(a1 > b1));
        System.out.println(b1 == 2);
        System.out.println(!(a1 != b1));
        System.out.println();
        System.out.println(true == false);
        System.out.println(true == (1 > 0));
        System.out.println(!!!!true);
        System.out.println("------------------------------");

        // 括弧 () 里的优先级最高，关系运算符 >、!= 优先级比逻辑运算符 ||、&& 高
        System.out.println((30 > 20 || 10 != 10) && 20 < 10);
        System.out.println(30 > 20 || (10 != 10 && 20 < 10));
        System.out.println("------------------------------");

        int a2 = 10;
        System.out.println((a2 = 20) == 20);
        System.out.println(a2);
        System.out.println("------------------------------");

        int a3 = 10;
        System.out.println(a3 > 0 && (a3 = 20) > 10);
        System.out.println(a3);
        int b3 = 10;
        System.out.println((b3 < 10) && (b3 = 20) > 10);
        System.out.println(b3);
        System.out.println("------------------------------");
        
        // 2、三目运算符格式：
        // 条件式 ? 值1 : 值2
        // 如果条件表达式为真，则表达式的值取值1，否则取值2
        // 注意：三目运算符可以嵌套
        System.out.println(true ? 10 : 20);
        System.out.println(false ? 10 : 20);
        System.out.println((3 > 2) && (2 < 3) ? 10 : 20);
        System.out.println("------------------------------");

        int n = 20;
        String s1 = n > 0 ? "正数" : "负数或零";
        // 三目运算符可以嵌套，但是一般不推荐这么写，可读性太差
        String s2 = n > 0 ? "正数" : (n < 0 ? "负数" : "零");
        System.out.println(s1);
        System.out.println(s2);
        System.out.println("------------------------------");

        // 3、运算符 = 就是赋值运算符。除 = 之外，还有 +=、-=、*=、/=、&=、^=、|= 等赋值运算符，一般将 = 运算符称为纯赋值运算符
        int a = 2;
        a += 2;
        System.out.println(a);
        a ^= 2;
        System.out.println(a);
        a <<= 1;
        System.out.println(a);
        a |= 2;
        System.out.println(a);
        a %= 2;
        System.out.println(a);
    }
}

```
