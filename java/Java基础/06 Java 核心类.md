
# Java 核心类

## 一、 字符串和编码

### String

在 Java 中，`String` 是一个引用类型，它本身也是一个 class。但是，Java 编译器对 `String` 有特殊处理，即可以直接用 "..." 来表示一个字符串 ：  
`String s1 = "Hello!";`

实际上字符串在 String 内部是通过一个 char[] 数组表示的，因此，按下面的写法也是可以的 ：   
`String s2 = new String(new char[] {'H', 'e', 'l', 'l', 'o', '!'});`

因为 String 太常用了，所以 Java 提供了 "..." 这种字符串字面量表示方法。

Java 字符串的一个重要特点就是字符串不可变。这种不可变性是通过内部的 `private final char[]` 字段，以及没有任何修改 char[] 的方法实现的。

### 字符串比较

当我们想要比较两个字符串是否相同时，要特别注意，我们实际上是想比较字符串的内容是否相同。必须使用 equals() 方法而不能用 ==。

```
String s1 = "hello";
String s2 = "hello";
System.out.println(s1 == s2);
System.out.println(s1.equals(s2));
```

要忽略大小写比较，使用 `equalsIgnoreCase()` 方法。
```
String s1 = "hello";
System.out.println("HELLO".equalsIgnoreCase(s1));
```

### 提取字符子串
```
// 是否包含子串:
"Hello".contains("ll"); // true

"Hello".indexOf("l"); // 2
"Hello".lastIndexOf("l"); // 3
"Hello".startsWith("He"); // true
"Hello".endsWith("lo"); // true

// 提取子串, 注意索引号是从 0 开始的
"Hello".substring(2); // "llo"
"Hello".substring(2, 4); "ll"
```

### 去除首尾空白字符

- 使用 trim() 方法可以移除字符串首尾空白字符。空白字符包括空格，\t，\r，\n
`"  \tHello\r\n ".trim(); // "Hello"`
> 注意：trim() 并没有改变字符串的内容，而是返回了一个新字符串。

- strip() 方法也可以移除字符串首尾空白字符, JDK11 新增特性。它和 trim() 不同的是，类似中文的空格字符 \u3000 也会被移除
```
"\u3000Hello\u3000".strip(); // "Hello"
" Hello ".stripLeading(); // "Hello "
" Hello ".stripTrailing(); // " Hello"
```
String 还提供了 isEmpty() 和 isBlank() 来判断字符串是否为空和空白字符串

```
"".isEmpty(); // true，因为字符串长度为0
"  ".isEmpty(); // false，因为字符串长度不为0
"  \n".isBlank(); // true，因为只包含空白字符
" Hello ".isBlank(); // false，因为包含非空白字符
```

### 替换子串
```
String s = "hello";
s.replace('l', 'w'); // "hewwo"，所有字符'l'被替换为'w'
s.replace("ll", "~~"); // "he~~o"，所有子串"ll"被替换为"~~"

// 通过正则表达式替换
String s = "A,,B;C ,D";
s.replaceAll("[\\,\\;\\s]+", ","); // "A,B,C,D"
```

### 分割字符串

要分割字符串，使用 split() 方法，并且传入的也是正则表达式

```
String s = "A,B,C,D";
String[] ss = s.split("\\,"); // {"A", "B", "C", "D"}
```

### 拼接字符串

拼接字符串使用静态方法 join()，它用指定的字符串连接字符串数组

```
String[] arr = {"A", "B", "C"};
String s = String.join("***", arr); // "A***B***C"
```
### 类型转换

要把任意基本类型或引用类型转换为字符串，可以使用静态方法 valueOf()。这是一个重载方法，编译器会根据参数自动选择合适的方法：

```
String.valueOf(123); // "123"
String.valueOf(45.67); // "45.67"
String.valueOf(true); // "true"
String.valueOf(new Object()); // 类似java.lang.Object@636be97c

int n1 = Integer.parseInt("123"); // 123
int n2 = Integer.parseInt("ff", 16); // 按十六进制转换，255

boolean b1 = Boolean.parseBoolean("true"); // true
boolean b2 = Boolean.parseBoolean("FALSE"); // false
```

### 转换为 char[]

String 和 char[] 类型可以互相转换
```
char[] cs = "Hello".toCharArray(); // String -> char[]
String s = new String(cs); // char[] -> String
```

### 字符编码

在早期的计算机系统中，为了给字符编码，美国国家标准学会（American National Standard Institute：ANSI）制定了一套英文字母、数字和常用符号的编码，它占用一个字节，编码范围从 0 到 127，最高位始终为0，称为 ASCII 编码。例如，字符 'A' 的编码是 0x41，字符 '1' 的编码是 0x31。

如果要把汉字也纳入计算机编码，很显然一个字节是不够的。GB2312 标准使用两个字节表示一个汉字，其中第一个字节的最高位始终为1，以便和ASCII编码区分开。例如，汉字'中'的 GB2312 编码是 0xd6d0。

类似的，日文有 Shift_JIS 编码，韩文有 EUC-KR 编码，这些编码因为标准不统一，同时使用，就会产生冲突。

为了统一全球所有语言的编码，全球统一码联盟发布了 Unicode 编码，它把世界上主要语言都纳入同一个编码，这样，中文、日文、韩文和其他语言就不会冲突。

Unicode 编码需要两个或者更多字节表示，我们可以比较中英文字符在 ASCII、GB2312 和 Unicode 的编码：

**什么是 UTF-8**

那我们经常使用的 UTF-8 又是什么编码呢？因为英文字符的 Unicode 编码高字节总是 00，包含大量英文的文本会浪费空间，所以，出现了 UTF-8 编码，它是一种变长编码，用来把固定长度的 Unicode 编码变成 1～4 字节的变长编码。通过 UTF-8 编码，英文字符 'A' 的 UTF-8 编码变为 0x41，正好和 ASCII 码一致，而中文'中'的 UTF-8 编码为 3 字节 0xe4b8ad。

UTF-8 编码的另一个好处是容错能力强。如果传输过程中某些字符出错，不会影响后续字符，因为 UTF-8 编码依靠高字节位来确定一个字符究竟是几个字节，它经常用来作为传输编码。

在 Java 中，char 类型实际上就是两个字节的 Unicode 编码。如果我们要手动把字符串转换成其他编码，可以这样做：

```
byte[] b1 = "Hello".getBytes(); // 按系统默认编码转换，不推荐
byte[] b2 = "Hello".getBytes("UTF-8"); // 按UTF-8编码转换
byte[] b2 = "Hello".getBytes("GBK"); // 按GBK编码转换
byte[] b3 = "Hello".getBytes(StandardCharsets.UTF_8); // 按UTF-8编码转换
```

注意：转换编码后，就不再是 char 类型，而是 byte 类型表示的数组。

如果要把已知编码的 byte[]  转换为String，可以这样做：
```
byte[] b = ...
String s1 = new String(b, "GBK"); // 按GBK转换
String s2 = new String(b, StandardCharsets.UTF_8); // 按UTF-8转换
```
> 始终牢记：Java 的 String 和 char 在内存中总是以 Unicode 编码表示。
  
###小结
- Java 字符串 String 是不可变对象；
- 字符串操作不改变原字符串内容，而是返回新字符串；
- 常用的字符串操作：提取子串、查找、替换、大小写转换等；
- Java 使用 Unicode 编码表示 String 和 char；
- 转换编码就是将 String 和 byte[] 转换，需要指定编码；
- 转换为 byte[] 时，始终优先考虑 UTF-8 编码。

小练习 ：

```java
public class UtilTest {
    public static void main(String[] args) {
        UtilTest utilTest = new UtilTest();
        utilTest.stringTest();
    }

    public void stringTest() {
        String s1 = "hello";
        // 实际上字符串在 String 内部是通过一个 char[] 数组表示的，因此，按下面的写法也是可以的
        String s2 = new String(new char[]{'h', 'e', 'l', 'l', 'o'});

        // hello
        System.out.println(s1);
        s1 = s1.toUpperCase();
        // HELLO
        System.out.println(s1);

        String s3 = "hello";
        // false
        System.out.println(s1 == s3);
        // true
        System.out.println("hello".equals(s3));
        // false
        System.out.println(s1.equals(s3));

        // 忽略大小写比较，使用 equalsIgnoreCase() 方法
        // true
        System.out.println("Hello".equalsIgnoreCase(s3));

        // 是否包含子串
        // public boolean contains(CharSequence s)
        System.out.println("Hello".contains("lo"));

        System.out.println("Hello".indexOf("l"));
        System.out.println("Hello".lastIndexOf("l"));
        System.out.println("Hello".startsWith("H"));
        System.out.println("Hello".endsWith("o"));

        // 提取字符子串
        System.out.println("Hello".substring(2));
        System.out.println("Hello".substring(3,5));

        // 去除首尾空白字符
        System.out.println(" Hello ".trim());
        // 1. 使用 trim() 方法可以移除字符串首尾空白字符。空白字符包括空格，\t，\r，\n
        System.out.println(" \tHello\r\n ".trim());
        // 2. JDK11中新方法, strip() 方法也可以移除字符串首尾空白字符。它和 trim() 不同的是，类似中文的空格字符 \u3000 也会被移除
        // System.out.println("\u3000Hello\u3000".strip());

        // 替换子串
        System.out.println("Hello".replace('e', 'o'));
        System.out.println("Jerry".replace("r", "n"));
        // 通过正则表达式替换
        System.out.println("A,,B;C ,D".replaceAll("[\\,\\;\\s]+", ","));

        // 分割字符串
        System.out.println("A, B, C, D".split("\\,"));

        // 拼接字符串
        String[] arr = {"A", "B", "C", "D"};
        System.out.println(String.join("&", arr));

        // 类型转换
        // 要把任意基本类型或引用类型转换为字符串，可以使用静态方法 valueOf()。这是一个重载方法，编译器会根据参数自动选择合适的方法
        System.out.println(String.valueOf(123));
        System.out.println(String.valueOf(123.456));
        System.out.println(String.valueOf(false));
        System.out.println(String.valueOf(new Object()));

        // 把字符串转换为其他类型
        // 转换为 int 类型
        int n1 = Integer.parseInt("123");
        System.out.println(n1);
        // 按十六进制转换为
        System.out.println(Integer.parseInt("3453", 16));

        // 转化为 boolean 类型
        System.out.println(Boolean.parseBoolean("true"));
        System.out.println(Boolean.parseBoolean("FALSE"));

        // 要特别注意，Integer 有个 getInteger(String) 方法，它不是将字符串转换为 int，而是把该字符串对应的系统变量转换为Integer
        System.out.println(Integer.getInteger("java.version"));

        // 转化为 char[]
        char[] chr = "Hello".toCharArray();
        // 因为通过 new String(char[])创建新的 String 实例时，它并不会直接引用传入的 char[] 数组，而是会复制一份
        String ss = new String(chr);
        System.out.println(ss);
        chr[0] = 'L';
        System.out.println(ss);
    }
}
```

## 二、 StringBuilder

为了能高效拼接字符串，Java 标准库提供了 StringBuilder，它是一个可变对象，可以预分配缓冲区，这样，往 StringBuilder 中新增字符时，不会创建新的临时对象。

如果我们查看 StringBuilder 的源码，可以发现，进行链式操作的关键是，定义的 append() 方法会返回 this，这样，就可以不断调用自身的其他方法。

### 小结
- StringBuilder 是可变对象，用来高效拼接字符串；
- StringBuilder 可以支持链式操作，实现链式操作的关键是返回实例本身；
- StringBuffer 是 StringBuilder 的线程安全版本，现在很少使用。

小练习 ：

```java
public class StringBuilderTest {
    public static void main(String[] args) {
        //循环拼接字符串 ： 每次循环都会创建新的字符串对象，然后扔掉旧的字符串。这样，绝大部分字符串都是临时对象，不但浪费内存，还会影响GC效率。
        String s = "";
        for (int i = 0; i < 10; i++) {
            s = s + "," + i;
        }
        System.out.println(s);

        // Java 标准库提供了 StringBuilder，它是一个可变对象，可以预分配缓冲区，这样，往 StringBuilder 中新增字符时，不会创建新的临时对象
        StringBuilder sb = new StringBuilder(1024);
        for(int i=0; i<10; i++) {
            sb.append(i);
            sb.append(',');
        }
        System.out.println(sb.toString());

        // StringBuilder 还可以进行链式操作
        StringBuilder ss = new StringBuilder(1024);
        ss.append("Jerry ")
                .append("li ")
                .append("！")
                .insert(0, "Hello, ");
        System.out.println(ss.toString());
    }
}
```

## 三、 StringJoiner

- 要高效拼接字符串，应该使用 `StringBuilder`
- 分隔符拼接数组，Java 标准库还提供了一个 `StringJoiner`，**支持指定“开头”和“结尾”**。（StringJoiner 内部实际上就是使用了 StringBuilder）
- String 还提供了一个静态方法 join()，这个方法在内部使用了 StringJoiner 来拼接字符串，在不需要指定“开头”和“结尾”的时候，用 `String.join()` 更方便

小练习 ：
```java
public class StringJoinerTest {
    public static void main(String[] args) {

        // 1. 使用 StringBuilder 拼接字符串
        String[] names = {"jerry", "kemy", "clint", "janie"};
        StringBuilder sb = new StringBuilder();
        sb.append("Hello ");
        for(String name : names) {
            sb.append(name).append(", ");
        }
        // 注意去掉最后的", "
        sb.delete(sb.length()-2, sb.length());
        sb.append("!");
        System.out.println(sb.toString());

        // 2. 使用 StringJoiner, 可以设置分隔符、开头、结尾
        StringJoiner stringJoiner = new StringJoiner(", ", "Hello ", "!");
        for(String name : names) {
            stringJoiner.add(name);
        }
        System.out.println(stringJoiner.toString());

        // 3. join()。String 还提供了一个静态方法 join()，这个方法在内部使用了 StringJoiner 来拼接字符串，在不需要指定“开头”和“结尾”的时候，用 String.join() 更方便
        String string = String.join(", ", names);
        System.out.println(string);

        // 4. 使用 StringJoiner 构造一个 SELECT 语句
        String[] fields = { "name", "position", "salary" };
        String tableName = "users";
        String select = buildSelectSql(tableName, fields);
        System.out.println(select);
        System.out.println("SELECT name, position, salary FROM users".equals(select) ? "测试成功" : "测试失败");
    }

    static String buildSelectSql(String tableName, String[] fields) {
        StringJoiner stringJoiner = new StringJoiner(", ", "SELECT ", " FROM " +  tableName);
        for(String field : fields) {
            stringJoiner.add(field);
        }
        return stringJoiner.toString();
    }
}
```

### 小结
- 用指定分隔符拼接字符串数组时，使用 `StringJoiner` 或者 `String.join()` 更方便；
- 用 `StringJoiner` 拼接字符串时，还可以额外附加一个“开头”和“结尾”。

## 四、包装类型

**Java 数据类型**
- 基本类型 ：byte，short，int，long，boolean，float，double，char
- 引用类型 ：所有 class 和 interface 类型

> 引用类型可以赋值为 null，表示空，但基本类型不能赋值为 null

### 包装类型

Java 核心库为每种基本类型都提供了对应的包装类型 : 
 
| **基本类型** | **对应的引用类型** |
|:---:|:---:|
|boolean	|java.lang.Boolean|
|byte	|java.lang.Byte|
|short	|java.lang.Short|
|int	|java.lang.Integer|
|long	|java.lang.Long|
|float	|java.lang.Float|
|double	|java.lang.Double|
|char	|java.lang.Character|

我们可以直接使用，并不需要自己去定义。

### Auto Boxing

Java 编译器可以帮助我们自动在 int 和 Integer 之间转型 ：  
- 自动装箱（Auto Boxing）: 把 int 变为 Integer 的赋值写法
- 自动拆箱（Auto Unboxing）: 把 Integer 变为 int 的赋值写法

> 注意：自动装箱和自动拆箱只发生在编译阶段，目的是为了少写代码。  
> 装箱和拆箱会影响代码的执行效率，因为编译后的 class 代码是严格区分基本类型和引用类型的。并且，自动拆箱执行时可能会报 NullPointerException：

```
int i = 100;
// 包装类型与数据类型相互转化
Integer integer = Integer.valueOf(i);
int j = integer.intValue();

// 自动装箱（Auto Boxing）：编译器自动使用 Integer.valueOf(int)
Integer n = 300;
// 自动拆箱（Auto Unboxing）：编译器自动使用 Integer.intValue()
int x = n;
```

### 进制转换

Integer 类本身还提供了大量方法，例如，最常用的静态方法 parseInt() 可以把字符串解析成一个整数，还可以把整数格式化为指定进制的字符串

```
// 静态方法 parseInt() 可以把字符串解析成一个整数
int x1 = Integer.parseInt("100"); // 100
int x2 = Integer.parseInt("100", 16); // 256,因为按16进制解析

// Integer 还可以把整数格式化为指定进制的字符串
System.out.println(Integer.toString(100)); // "100",表示为10进制
System.out.println(Integer.toString(100, 36)); // "2s",表示为36进制
System.out.println(Integer.toHexString(100)); // "64",表示为16进制
System.out.println(Integer.toOctalString(100)); // "144",表示为8进制
System.out.println(Integer.toBinaryString(100)); // "1100100",表示为2进制
```

Java 的包装类型还定义了一些有用的静态变量
```
// boolean只有两个值true/false，其包装类型只需要引用Boolean提供的静态字段:
Boolean t = Boolean.TRUE;
Boolean f = Boolean.FALSE;
// int可表示的最大/最小值:
int max = Integer.MAX_VALUE; // 2147483647
int min = Integer.MIN_VALUE; // -2147483648
// long类型占用的bit和byte数量:
int sizeOfLong = Long.SIZE; // 64 (bits)
int bytesOfLong = Long.BYTES; // 8 (bytes)
```
最后，所有的整数和浮点数的包装类型都继承自 Number，因此，可以非常方便地直接通过包装类型获取各种基本类型：

```
// 向上转型为Number:
Number num = new Integer(999);
// 获取byte, int, long, float, double:
byte b = num.byteValue();
int n = num.intValue();
long ln = num.longValue();
float f = num.floatValue();
double d = num.doubleValue();
```

### 处理无符号整型

在 Java 中，并没有无符号整型（Unsigned）的基本数据类型。byte、short、int 和 long 都是带符号整型，最高位是符号位。而C语言则提供了 CPU 支持的全部数据类型，包括无符号整型。无符号整型和有符号整型的转换在 Java 中就需要借助包装类型的静态方法完成。

例如，byte 是有符号整型，范围是 -128~+127，但如果把 byte 看作无符号整型，它的范围就是 0~255。我们把一个负的 byte 按无符号整型转换为 int :   

```
byte x = -1;
byte y = 127;
System.out.println(Byte.toUnsignedInt(x)); // 255
System.out.println(Byte.toUnsignedInt(y)); // 127
```
因为 byte 的 -1 的二进制表示是 11111111，以无符号整型转换后的 int 就是 255。

类似的，可以把一个 short 按 unsigned 转换为 int，把一个 int 按 unsigned 转换为 long。

### 小结
- Java 核心库提供的包装类型可以把基本类型包装为 class；
- 自动装箱和自动拆箱都是在编译期完成的（JDK>=1.5）；
- 装箱和拆箱会影响执行效率，且拆箱时可能发生 NullPointerException；
- 包装类型的比较必须使用 equals()；
- 整数和浮点数的包装类型都继承自 Number；
- 包装类型提供了大量实用方法。

## 五、JavaBean

在 Java 中，有很多 class 的定义都符合这样的规范 ：  
- 若干 private 实例字段；
- 通过 public 方法来读写实例字段。

如果读写方法符合以下这种命名规范，那么这种 class 被称为 `JavaBean`
 
```java
class Fruit{
    private String name;
    private int price;
    private Boolean nice;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // 读方法
    public int getPrice() {
        return price;
    }

    // 写方法
    public void setPrice(int price) {
        this.price = price;
    }

    public Boolean getNice() {
        return nice;
    }

    public void setNice(Boolean nice) {
        this.nice = nice;
    }
}
```
通常把一组对应的读方法（getter）和写方法（setter）称为属性（property）
- 对应的读方法是 String getName()
- 对应的写方法是 setName(String)

### JavaBean 的作用

JavaBean 主要用来传递数据，即把一组数据组合成一个 JavaBean 便于传输。此外，JavaBean 可以方便地被IDE工具分析，生成读写属性的代码，主要用在图形界面的可视化设计中。

通过 IDE，可以快速生成 getter 和 setter。
- Eclipse，先输入以下代码，点击右键，在弹出的菜单中选择 “Source”，“Generate Getters and Setters”，在弹出的对话框中选中需要生成 getter 和 setter 方法的字段，点击确定；
- IntelliJ IDEA，先输入以下代码，按 "control + return", 选择 "Generate Getter and Setter"，在弹出的对话框中选中需要生成 getter 和 setter 方法的字段，点击确定；
- 使用 lombok ,在类上面 @Data
```java
@Data
class Fruit{
    private String name;
    private int price;
    private Boolean nice;
}
```
#### 枚举 JavaBean 属性

要枚举一个 JavaBean 的所有属性，可以直接使用Java核心库提供的 Introspector

```java
public class JavaBeanTest {
    public static void main(String[] args) throws IntrospectionException {
        // Java 核心库提供的 Introspector, 枚举 JavaBean 的所有属性
        BeanInfo beanInfo = Introspector.getBeanInfo(Fruit.class);
        for(PropertyDescriptor pd : beanInfo.getPropertyDescriptors()) {
            System.out.println(pd.getName());
            System.out.println(" " + pd.getReadMethod());
            System.out.println(" " + pd.getWriteMethod());
        }

    }
}
@Data
class Fruit{
    private String name;
    private int price;
    private Boolean nice;
}
```
运行上述代码，可以列出所有的属性，以及对应的读写方法。注意 class 属性是从 Object 继承的 getClass() 方法带来的。

### 小结
- JavaBean 是一种符合命名规范的 class，它通过 getter 和 setter 来定义属性；
- 属性是一种通用的叫法，并非 Java 语法规定；
- 可以利用 IDE 快速生成 getter 和 setter；
- 使用 Introspector.getBeanInfo() 可以获取属性列表。

## 六、枚举类

### enum

为了让编译器能自动检查某个值在枚举的集合内，并且，不同用途的枚举需要不同的类型来标记，不能混用，我们可以使用 `enum` 来定义枚举类：

```
enum Weekday {
    SUN, MON, TUE, WED, THU, FRI, SAT;
}
```

### enum 类型与 class 的区别
> 答案是没有任何区别。enum 定义的类型就是 class，只不过它有以下几个特点 ：
- 定义的 enum 类型总是继承自 java.lang.Enum，且无法被继承；
- 只能定义出 enum 的实例，而无法通过 new 操作符创建 enum 的实例；
- 定义的每个实例都是引用类型的唯一实例；
- 可以将 enum 类型用于 switch 语句。

> enum 类型的每个常量在 JVM中 只有一个唯一实例，所以可以直接用 == 比较
```
System.out.println(day == Weekday.FRI);  // true
System.out.println(day.equals(Weekday.FRI)); // true
```
### name()
返回常量名，例如：   
`String s = Weekday.SUN.name(); // "SUN"`

### ordinal()
返回定义的常量的顺序，从0开始计数，例如：   
`int n = Weekday.MON.ordinal(); // 1`

```java
public class EnumTest {
    public static void main(String[] args) {
        Weekday day = Weekday.FRI;
        if(day == Weekday.MON || day == Weekday.THU) {
            System.out.println("Meeting!");
        } else {
            System.out.println("No Meet!");
        }

        // enum 类型的每个常量在 JVM 中只有一个唯一实例，所以可以直接用 == 比较
        System.out.println(day == Weekday.FRI);
        System.out.println(day.equals(Weekday.FRI));

        // 返回常量名 name()
        System.out.println(Weekday.SUM.name());

        // 返回常量的顺序 ordinal(), 依赖枚举顺序，枚举顺序变了，值也会跟着变
        System.out.println(Weekday.SAT.ordinal());

        // 枚举类可以应用在 switch 语句中。因为枚举类天生具有类型信息和有限个枚举常量，所以比 int、String 类型更适合用在 switch 语句中：
        switch (day) {
            case MON:
            case THU:
            case WED:
            case TUE:
            case FRI:
                System.out.println("Today is " + day + " working day!");
                break;
            case SAT:
            case SUM:
                System.out.println("Today is " + day + "  have a good rest!");
                break;
            default:
                System.out.println("Data error!");
                throw new RuntimeException("cannot process " + day);
        }
    }
}

enum Weekday {
    SUM, MON, TUE, WED, THU, FRI, SAT;
}

enum NewWeekday {
    // 给枚举新增常量，自定义枚举顺序，这样就不受枚举排序影响
    SUM(1), MON(2), TUE(3), WED(4), THU(5), FRI(6), SAT(7);

    public final int dayValue;

    private NewWeekday(int dayValue) {
        this.dayValue = dayValue;
    }
}

```

### 小结

- Java 使用 enum 定义枚举类型，它被编译器编译为 final class Xxx extends Enum { … }；
- 通过 name() 获取常量定义的字符串，注意不要使用 toString()；
- 通过 ordinal() 返回常量定义的顺序（无实质意义）；
- 可以为 enum 编写构造方法、字段和方法
- enum 的构造方法要声明为 private，字段强烈建议声明为 final；
- enum 适合用在 switch 语句中。

## 七、BigInteger

在 Java 中，由 CPU 原生提供的整型最大范围是 64 位 long 型整数。使用 long 型整数可以直接通过 CPU 指令进行计算，速度非常快。

如果我们使用的整数范围超过了 long 型怎么办？这个时候，就只能用软件来模拟一个大整数。`java.math.BigInteger` 就是用来表示任意大小的整数。BigInteger 内部用一个 int[] 数组来模拟一个非常大的整数

和 long 型整数运算比，BigInteger 不会有范围限制，但缺点是速度比较慢。也可以把 BigInteger 转换成 long 型 ：
```
BigInteger i = new BigInteger("123456789000");
System.out.println(i.longValue()); // 123456789000
System.out.println(i.multiply(i).longValueExact()); // java.lang.ArithmeticException: BigInteger out of long range
```
使用 `longValueExact()` 方法时，如果超出了 long 型的范围，会抛出 `ArithmeticException`。

BigInteger 和 Integer、Long 一样，也是不可变类，并且也继承自 Number 类。因为 Number 定义了转换为基本类型的几个方法：
- 转换为 byte：byteValue()
- 转换为 short：shortValue()
- 转换为 int：intValue()
- 转换为 long：longValue()
- 转换为 float：floatValue()
- 转换为 double：doubleValue()
因此，通过上述方法，可以把 BigInteger 转换成基本类型。如果 BigInteger 表示的范围超过了基本类型的范围，转换时将丢失高位信息，即结果不一定是准确的。如果需要准确地转换成基本类型，可以使用 intValueExact()、longValueExact() 等方法，在转换时如果超出范围，将直接抛出 ArithmeticException 异常。

```java
public class BigIntegerTest {
    public static void main(String[] args) {

        // java.math.BigInteger : 用来表示任意大小的整数
        BigInteger bigInteger = new BigInteger("9999999999999");
        // 9999999999996000000000000599999999999960000000000001
        System.out.println(bigInteger.pow(4));

        // BigInteger 做运算的时候，只能使用实例方法
        BigInteger i1 = new BigInteger("2411413242424234");
        BigInteger i2 = new BigInteger("8888888888888");
        BigInteger sum = i1.add(i2);

        // BigInteger 转换成 long 型
        BigInteger i = new BigInteger("7777777777");
        long x = i.longValue();
        // longValueExact() 超过 long 型的范围，会抛异常 ： java.lang.ArithmeticException: BigInteger out of long range
        System.out.println(i.multiply(i).longValueExact());
   
        BigInteger big = new BigInteger("7777777777").pow(100);
        float f = big.floatValue();
        // BigInteger 的值超过了 float 的最大范围（3.4x1038）,返回 ： Infinity
        System.out.println(f);
    }
}
```
### 小结
- BigInteger 用于表示任意大小的整数；
- BigInteger 是不变类，并且继承自 Number；
- 将 BigInteger 转换成基本类型时可使用 longValueExact() 等方法保证结果准确。

## 八、BigDecimal

和 `BigInteger` 类似，BigDecimal 可以表示一个任意大小且精度完全准确的浮点数。

> 查看 BigDecimal 的源码，可以发现，实际上一个 BigDecimal 是通过一个 BigInteger 和一个 scale 来表示的，即 BigInteger 表示一个完整的整数，而 scale 表示小数位数

```java
public class BigDecimal extends Number implements Comparable<BigDecimal> {
    private final BigInteger intVal;
    private final int scale;
}
```

### scale() 

表示小数位数
```
BigDecimal d1 = new BigDecimal("123.45");
BigDecimal d2 = new BigDecimal("123.4500");
BigDecimal d3 = new BigDecimal("1234500");
System.out.println(d1.scale()); // 2,两位小数
System.out.println(d2.scale()); // 4
System.out.println(d3.scale()); // 0
```
### setScale
设置小数位数。如果精度比原始值低，那么按照指定的方法进行四舍五入或者直接截断。

> 对 BigDecimal 做加、减、乘时，精度不会丢失，但是做除法时，存在无法除尽的情况，这时，就必须指定精度以及如何进行截断

```
BigDecimal d1 = new BigDecimal("123.456789");
BigDecimal d2 = d1.setScale(4, RoundingMode.HALF_UP); // 四舍五入，123.4568
BigDecimal d3 = d1.setScale(4, RoundingMode.DOWN); // 直接截断，123.4567
```

### stripTrailingZeros() 

将一个 BigDecimal 格式化为一个相等的，但去掉了末尾 0 的 BigDecimal

```
BigDecimal d1 = new BigDecimal("123.4500");
BigDecimal d2 = d1.stripTrailingZeros();
System.out.println(d1.scale()); // 4
System.out.println(d2.scale()); // 2,因为去掉了00

BigDecimal d3 = new BigDecimal("1234500");
BigDecimal d4 = d3.stripTrailingZeros();
System.out.println(d3.scale()); // 0
System.out.println(d4.scale()); // -2
```

### divideAndRemainder()

返回的数组包含两个 BigDecimal，分别是商和余数，其中商总是整数，余数不会大于除数。我们可以利用这个方法判断两个 BigDecimal 是否是整数倍数
```
BigDecimal n = new BigDecimal("12.75");
BigDecimal m = new BigDecimal("0.15");
// divideAndRemainder() 方法时，返回的数组包含两个 BigDecimal，分别是商和余数，其中商总是整数，余数不会大于除数。
// 我们可以利用这个方法判断两个 BigDecimal 是否是整数倍数
BigDecimal[] bigDecimals = n.divideAndRemainder(m);
// 如果余数 bigDecimals[1] 等于 0，说明是整数倍。 signum() 返回正数 1、零 0、负数 -1
if(bigDecimals[1].signum() == 0) {
    System.out.println(n + " 是 " + m + " 的整数倍！" );
}
```

### 比较 BigDecimal
- equals() : 不推荐。不但要求两个 BigDecimal 的值相等，还要求它们的 scale() 相等
- compareTo() ： 推荐。它根据两个值的大小分别返回负数、正数和 0，分别表示小于、大于和等于

```
// equals() 方法不但要求两个 BigDecimal 的值相等，还要求它们的 scale() 相等
BigDecimal ss = new BigDecimal("123.456");
BigDecimal dd = new BigDecimal("123.45600");
System.out.println(ss.equals(dd)); // false,因为scale不同
System.out.println(ss.equals(dd.stripTrailingZeros())); // true,因为d2去除尾部0后scale变为2

// 必须使用 compareTo() 方法来比较，它根据两个值的大小分别返回负数、正数和 0，分别表示小于、大于和等于
System.out.println(ss.compareTo(dd)); // 0
```
###小结
- BigDecimal 用于表示精确的小数，常用于财务计算；
- 比较 BigDecimal 的值是否相等，必须使用 compareTo() 而不能使用 equals()。

小练习 ：

```java
public class BigDecimalTest {
    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal("123456.789");
        //15241578750.190521
        System.out.println(bigDecimal.multiply(bigDecimal));
        // scale() : 获取小数位 3
        System.out.println(bigDecimal.scale());

        BigDecimal d1 = new BigDecimal("123456.78700");
        // 5
        System.out.println(d1.scale());

        BigDecimal d2 = d1.stripTrailingZeros();
        // stripTrailingZeros() : 将一个 BigDecimal 格式化为一个相等的，但去掉了末尾 0 的 BigDecimal 123456.787
        System.out.println(d2);
        // 3
        System.out.println(d2.scale());

        System.out.println(new BigDecimal("123.00").scale());

        // 1.保留两位小数，四舍五入 123456.79
        System.out.println(d1.setScale(2, RoundingMode.HALF_UP));
        // 2.保留两位小数，直接截断 123456.78
        System.out.println(d1.setScale(2, RoundingMode.DOWN));

        // 对 BigDecimal 做加、减、乘时，精度不会丢失，但是做除法时，存在无法除尽的情况，这时，就必须指定精度以及如何进行截断：
        BigDecimal a = new BigDecimal("123.456");
        BigDecimal b = new BigDecimal("23.456789");
        // 保留10位小数并四舍五入
        System.out.println(a.divide(b, 10, RoundingMode.HALF_UP));
        // 报错：ArithmeticException，因为除不尽。java.lang.ArithmeticException: Non-terminating decimal expansion; no exact representable dec
        // System.out.println(a.divide(b));

        BigDecimal n = new BigDecimal("12.75");
        BigDecimal m = new BigDecimal("0.15");
        // divideAndRemainder() 方法时，返回的数组包含两个 BigDecimal，分别是商和余数，其中商总是整数，余数不会大于除数。
        // 我们可以利用这个方法判断两个 BigDecimal 是否是整数倍数
        BigDecimal[] bigDecimals = n.divideAndRemainder(m);
        // 如果余数 bigDecimals[1] 等于 0，说明是整数倍。 signum() 返回正数 1、零 0、负数 -1
        if(bigDecimals[1].signum() == 0) {
            System.out.println(n + " 是 " + m + " 的整数倍！" );
        }

        // equals() 方法不但要求两个 BigDecimal 的值相等，还要求它们的 scale() 相等
        BigDecimal ss = new BigDecimal("123.456");
        BigDecimal dd = new BigDecimal("123.45600");
        System.out.println(ss.equals(dd)); // false,因为scale不同
        System.out.println(ss.equals(dd.stripTrailingZeros())); // true,因为d2去除尾部0后scale变为2

        // 必须使用 compareTo() 方法来比较，它根据两个值的大小分别返回负数、正数和 0，分别表示小于、大于和等于
        System.out.println(ss.compareTo(dd)); // 0

    }
}
```

## 九、常用工具类

### Random

Random 用来创建伪随机数。所谓伪随机数，是指只要给定一个初始的种子，产生的随机数序列是完全一样的。
    
要生成一个随机数，可以使用 `nextInt()`、`nextLong()`、`nextFloat()`、`nextDouble()`：
- 如果不给定种子，就使用系统当前时间戳作为种子，因此每次运行时，种子不同，得到的伪随机数序列就不同。
- 如果我们在创建 Random 实例时指定一个种子，就会得到完全确定的随机数序列

### SecureRandom

有伪随机数，就有真随机数。实际上真正的真随机数只能通过量子力学原理来获取，而我们想要的是一个不可预测的安全的随机数，SecureRandom 就是用来创建安全的随机数的：
```
SecureRandom sr = new SecureRandom();
System.out.println(sr.nextInt(100));
```
SecureRandom 无法指定种子，它使用 RNG（random number generator）算法。JDK 的 SecureRandom 实际上有多种不同的底层实现，有的使用安全随机种子加上伪随机数算法来产生安全的随机数，有的使用真正的随机数生成器。实际使用的时候，可以优先获取高强度的安全随机数生成器，如果没有提供，再使用普通等级的安全随机数生成器：

```java
import java.util.Arrays;
import java.security.SecureRandom;
import java.security.NoSuchAlgorithmException;

public class MathTest {
    public static void main(String[] args) {
        SecureRandom se = null;
                        try {
                            // 获取高强度安全随机数生成器
                            se = SecureRandom.getInstanceStrong();
                        } catch (NoSuchAlgorithmException e) {
                            // 获取普通的安全随机数生成器
                            se = new SecureRandom();
                        }
                        byte[] bytes = new byte[16];
                        se.nextBytes(bytes);
                        System.out.println(Arrays.toString(bytes));
    }
}
```
SecureRandom 的安全性是通过操作系统提供的安全的随机种子来生成随机数。这个种子是通过 CPU 的热噪声、读写磁盘的字节、网络流量等各种随机事件产生的“熵”。

在密码学中，安全的随机数非常重要。如果使用不安全的伪随机数，所有加密体系都将被攻破。因此，时刻牢记必须使用 SecureRandom 来产生安全的随机数。

> **需要使用安全随机数的时候，必须使用 SecureRandom，绝不能使用 Random！**

### 小结

Java 提供的常用工具类有：
- Math：数学计算
- Random：生成伪随机数
- SecureRandom：生成安全的随机数
 
小练习 ：
```java
public class MathTest {
    public static void main(String[] args) {

        // 1. 绝对值
        System.out.println(Math.abs(-123));
        System.out.println(Math.abs(-89.24));
        System.out.println(Math.abs(342));

        // 2. 最大值或最小值
        System.out.println(Math.max(897, 1233));
        System.out.println(Math.min(-23, 24));

        // 3. 计算xy次方
        System.out.println(Math.pow(2, 4));

        // 4. 计算√x
        System.out.println(Math.sqrt(2));

        // 5. 计算ex次方
        System.out.println(Math.exp(2));

        // 6. 计算以e为底的对数
        System.out.println(Math.log(4));

        // 7. 计算以10为底的对数
        System.out.println(Math.log10(100));

        // 8. 三角函数
        System.out.println(Math.sin(1));
        System.out.println(Math.cos(3.14));
        System.out.println(Math.tan(0.5));
        System.out.println(Math.acos(1));

        // 9. Math还提供了几个数学常量
        System.out.println(Math.PI);
        System.out.println(Math.E);
        System.out.println(Math.PI/6);

        // 10. 生成一个随机数x，x的范围是0 <= x < 1
        // Math.random() 实际上内部调用了 Random 类，所以它也是伪随机数，只是我们无法指定种子
        System.out.println(Math.random());

        // 11. Random 用来创建伪随机数。
        // 所谓伪随机数，是指只要给定一个初始的种子，产生的随机数序列是完全一样的。如果不给定种子，就使用系统当前时间戳作为种子，因此每次运行时，种子不同，得到的伪随机数序列就不同。
        // 要生成一个随机数，可以使用 nextInt()、nextLong()、nextFloat()、nextDouble()
        Random random = new Random();
        System.out.println(random.nextInt());
        // 生成一个 [0,10) 之间的 int
        System.out.println(random.nextInt(10));
        System.out.println(random.nextLong());
        // 生成一个 [0,1) 之间的 float
        System.out.println(random.nextFloat());
        // 生成一个 [0,1) 之间的 double
        System.out.println(random.nextDouble());

        // 12. SecureRandom 真随机数
        SecureRandom secureRandom = new SecureRandom();
        // SecureRandom 无法指定种子，它使用 RNG（random number generator）算法
        System.out.println(secureRandom.nextInt(100));
        
        
        SecureRandom se = null;
                try {
                    // 获取高强度安全随机数生成器
                    se = SecureRandom.getInstanceStrong();
                } catch (NoSuchAlgorithmException e) {
                    // 获取普通的安全随机数生成器
                    se = new SecureRandom();
                }
                byte[] bytes = new byte[16];
                se.nextBytes(bytes);
                System.out.println(Arrays.toString(bytes));
    }
}
```