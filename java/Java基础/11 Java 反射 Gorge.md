# Java开发实战经验-反射相关知识  

说明：本文将通过代码加实例的方式来讲解反射相关的知识。  

> 注：本文基于的硬 / 软件环境  

> 硬件：MacBook Pro    
> 操作系统：macOS Mojave 10.14.4  
> JDK版本：1.8.0_211  

## 目录  
* [反射简介](#0)  
* [反射机制相关的类](#1)  
    * [`Class` 类](#1.1)
      * [获得类的相关信息的方法](#1.1.1)
      * [获得类的属性相关的方法](#1.1.2)
      * [获得类的注解相关的方法](#1.1.3)
      * [获得类的构造器相关的方法](#1.1.4)
      * [获得类的方法的相关的方法](#1.1.5)
      * [`Class` 类中其它重要的方法](#1.1.6)
    * [`Field` 类](#1.2)
    * [`Method` 类](#1.3)
    * [`Constructor` 类](#1.4)
* [反射机制的示例](#2)  
* [反射机制的作用](#3)  
* [常见问题](#4)  
    * [1、`.getClass()` 与 `.class` 的区别？](#4.1)
    * [2、`Object` 类与 `Class` 类的区别？](#4.2)
    * [3、通过构造函数创建对象实例与通过反射创建对象的实例性能有多大差异？](#4.3)

<a name="0"></a>

## 反射简介  
`Java` 反射机制是在运行状态中，对于任意一个实体类，都能够动态获取这个类的所有属性和方法；对于任意一个对象，都能够调用它的任意方法和属性；这种动态获取信息以及动态调用对象方法的功能称为 `Java` 语言的反射机制。  
简单来说，所谓反射，就是在程序运行时动态获取或调用实体类或对象的一些信息的操作，信息主要是指实体类或对象的属性及方法等。 

<a name="1"></a>

## 反射机制相关的类  

`Java` 反射相关的类如下：  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>类名</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>Class 类</td>
            <td>代表类的实体，在运行的 Java 应用程序中表示类和接口</td>
        </tr>
        <tr>
            <td>Field 类</td>
            <td>代表类的成员变量（成员变量也称为类的属性）</td>
        </tr>
        <tr>
            <td>Method 类</td>
            <td>代表类的成员方法（成员方法也称为类的动作）</td>
        </tr>
        <tr>
            <td>Constructor 类</td>
            <td>代表类的构造方法</td>
        </tr>
</table>

下面分别说明：  

<a name="1.1"></a>

**`Class` 类**  
`Class` 类代表类的实体，在 `Java` 运行程序中用来表示类或接口。`Class` 类提供了很多有用的方法，下面简单分类介绍一下：  

<a name="1.1.1"></a>

**获得类的相关信息的方法**  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>asSubclass(Class&#60;U&#62; clazz)</td>
            <td>把传递的类的对象转换成代表其子类的对象</td>
        </tr>
        <tr>
            <td>cast(Object obj)</td>
            <td>把对象转换成代表类或是接口的对象</td>
        </tr>
        <tr>
            <td>getClassLoader()</td>
            <td>获得类的加载器</td>
        </tr>
        <tr>
            <td>getClasses()</td>
            <td>返回一个数组，数组中包含该类中所有公共类和接口类的对象</td>
        </tr>
        <tr>
            <td>getDeclaredClasses()</td>
            <td>返回一个数组，数组中包含该类中所有类和接口类的对象</td>
        </tr>
        <tr>
            <td>forName(String className)</td>
            <td>根据类名返回类的对象</td>
        </tr>
        <tr>
            <td>getName()</td>
            <td>获得类的完整路径名字</td>
        </tr>
        <tr>
            <td>newInstance()</td>
            <td>创建类的实例</td>
        </tr>
        <tr>
            <td>getPackage()</td>
            <td>获得类的包</td>
        </tr>
        <tr>
            <td>getSimpleName()</td>
            <td>获得类的名字</td>
        </tr>
        <tr>
            <td>getSuperclass()</td>
            <td>获得当前类继承的父类的名字</td>
        </tr>
        <tr>
            <td>getInterfaces()</td>
            <td>获得当前类实现的类或是接口</td>
        </tr>
</table>

<a name="1.1.2"></a>

**获得类的属性相关的方法**  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>getField(String name)</td>
            <td>获得某个公有的属性对象</td>
        </tr>
        <tr>
            <td>getFields()</td>
            <td>获得所有公有的属性对象</td>
        </tr>
        <tr>
            <td>getDeclaredField(String name)</td>
            <td>获得某个属性对象</td>
        </tr>
        <tr>
            <td>getDeclaredFields()</td>
            <td>获得所有属性对象</td>
        </tr>
</table>

<a name="1.1.3"></a>

**获得类的注解相关的方法**  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>getAnnotation(Class&#60;A&#62; annotationClass)</td>
            <td>返回该类中与参数类型匹配的公有注解对象</td>
        </tr>
        <tr>
            <td>getAnnotations()</td>
            <td>返回该类所有的公有注解对象</td>
        </tr>
        <tr>
            <td>getDeclaredAnnotation(Class&#60;A&#62; annotationClass)</td>
            <td>返回该类中与参数类型匹配的所有注解对象</td>
        </tr>
        <tr>
            <td>getDeclaredAnnotations()</td>
            <td>返回该类所有的注解对象</td>
        </tr>
</table>

<a name="1.1.4"></a>

**获得类的构造器相关的方法**  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>getConstructor(Class...<?> parameterTypes)</td>
            <td>获得该类中与参数类型匹配的公有构造方法</td>
        </tr>
        <tr>
            <td>getConstructors()</td>
            <td>获得该类的所有公有构造方法</td>
        </tr>
        <tr>
            <td>getDeclaredConstructor(Class...<?> parameterTypes)</td>
            <td>获得该类中与参数类型匹配的构造方法</td>
        </tr>
        <tr>
            <td>getDeclaredConstructors()</td>
            <td>获得该类所有构造方法</td>
        </tr>
</table>

<a name="1.1.5"></a>

**获得类的方法的相关的方法**  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>getMethod(String name, Class...<?> parameterTypes)</td>
            <td>获得该类某个公有的方法</td>
        </tr>
        <tr>
            <td>getMethods()</td>
            <td>获得该类所有公有的方法</td>
        </tr>
        <tr>
            <td>getDeclaredMethod(String name, Class...<?> parameterTypes)</td>
            <td>获得该类某个方法</td>
        </tr>
        <tr>
            <td>`getDeclaredMethods()`</td>
            <td>获得该类所有方法</td>
        </tr>
</table>

<a name="1.1.6"></a>

**`Class` 类中其它重要的方法**  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>isAnnotation()</td>
            <td>如果是注解类型则返回 true</td>
        </tr>
        <tr>
            <td>isAnnotationPresent(Class<? extends Annotation> annotationClass)</td>
            <td>如果是指定类型注解类型则返回 true</td>
        </tr>
        <tr>
            <td>isAnonymousClass()</td>
            <td>如果是匿名类则返回 true</td>
        </tr>
        <tr>
            <td>isArray()</td>
            <td>如果是一个数组类则返回 true</td>
        </tr>
        <tr>
            <td>isEnum()</td>
            <td>如果是枚举类则返回 true</td>
        </tr>
        <tr>
            <td>isInstance(Object obj)</td>
            <td>如果 obj 是该类的实例则返回 true</td>
        </tr>
        <tr>
            <td>isInterface()</td>
            <td>如果是接口类则返回 true</td>
        </tr>
        <tr>
            <td>isLocalClass()</td>
            <td>如果是局部类则返回 true</td>
        </tr>
        <tr>
            <td>isMemberClass()</td>
            <td>如果是内部类则返回 true</td>
        </tr>
</table>

<a name="1.2"></a>

**`Field` 类**

Field 代表类的成员变量（也称为类的属性） 
 
<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>equals(Object obj)</td>
            <td>属性与 obj 相等则返回 true</td>
        </tr>
        <tr>
            <td>get(Object obj)</td>
            <td>获得 obj 中对应的属性值</td>
        </tr>
        <tr>
            <td>set(Object obj, Object value)</td>
            <td>设置 obj 中对应属性值</td>
        </tr>
</table>

<a name="1.3"></a>

**`Method` 类**

`Method` 代表类的成员方法  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>invoke(Object obj, Object... args)</td>
            <td>传递 `object` 对象及参数调用该对象对应的方法</td>
        </tr>
</table>

<a name="1.4"></a>

**`Constructor` 类**  

`Constructor` 代表类的构造方法

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>方法</b></td>
            <td><b>用途</b></td>
        </tr>
        <tr>
            <td>newInstance(Object... initArgs)</td>
            <td>根据传递的参数创建类的对象</td>
        </tr>
</table>

<a name="2"></a>

## 反射机制的示例
首先定义一个 `User` 类：  

	package com.gorge4j;
	public class User {
	
	    // 显式声明无参构造方法（也称为构造函数）
	    public User() {}
	
	    // 声明私有的构造方法（也称为构造函数）
	    private User(String name, Boolean sex, Integer age) {
	        this.name = name;
	        this.sex = sex;
	        this.age = age;
	    }
	
	    // 用户名
	    private String name;
	    // 性别
	    private Boolean sex;
	    // 年龄
	    private Integer age;
	
	    public String getName() {
	        return name;
	    }
	
	    public void setName(String name) {
	        this.name = name;
	    }
	
	    public Boolean getSex() {
	        return sex;
	    }
	
	    public void setSex(Boolean sex) {
	        this.sex = sex;
	    }
	
	    public Integer getAge() {
	        return age;
	    }
	
	    public void setAge(Integer age) {
	        this.age = age;
	    }
	
	    // 定义私有方法
	    private String doSomething(String s) {
	        return s;
	    }
	
	    @Override
	    public String toString() {
	        return "User [name=" + name + ", sex=" + sex + ", age=" + age + "]";
	    }
	
	}

反射机制的应用示例：  

	package com.gorge4j;
	
	import java.lang.annotation.Annotation;
	import java.lang.reflect.Constructor;
	import java.lang.reflect.Field;
	import java.lang.reflect.InvocationTargetException;
	import java.lang.reflect.Method;
	import java.lang.reflect.Modifier;
	
	public class UserReflect {
    
	    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	        User user = new User();
	        user.setName("zhangsan");
	        user.setSex(true);
	        user.setAge(18);
	        
	        Class<User> clazz1 = User.class; // 获取 User 类对应的 Class 对象方式 1
	        Class<?> clazz2 = Class.forName("com.gorge4j.User"); // 获取 User 类对应的 Class 对象方式 2
	        
	        String clazzName = clazz1.getName(); // 获取类名，含包名
	        String clazzSimpleName = clazz1.getSimpleName(); // 获取类名，不含包名
	        Package package1 = clazz1.getPackage(); // 获取包
	        
	        int iMod = clazz1.getModifiers(); // 获取类修饰符
	        boolean isPublic = Modifier.isPublic(iMod); // 判断修饰符
	        boolean isProtected = Modifier.isProtected(iMod); // 判断修饰符
	        
	        Class<? super User> superClass = clazz1.getSuperclass(); // 获取父类
	        Class<?>[] interfaces = clazz1.getInterfaces(); // 获取所有实现接口
	        Constructor<?>[] constructors = clazz1.getConstructors(); // 获取所有构造函数
	        
	        Method[] methods = clazz1.getMethods(); // 获取所有公有方法（包括继承的方法，但是不包括私有的方法）
	        Method[] declaredMethods = clazz1.getDeclaredMethods(); // 获取所有方法（包括私有，但是不包括继承的方法）
	        Method method = clazz1.getMethod("toString"); // 获取指定的公有方法
	        Method declaredMethod = clazz1.getDeclaredMethod("doSomething", String.class); // 获取指定的私有方法
	        Object methodInvokeValue = method.invoke(user); // 调用获取的公有方法
	        declaredMethod.setAccessible(true); // 注意：调用反射获取的私有方法前，需要将私有方法设置成可访问
	        Object declaredMethodInvokeValue = declaredMethod.invoke(user, "xxx"); // 调用获取的私有方法
	        
	        Field[] fields = clazz1.getFields(); // 获取所有公有属性
	        Field[] declaredFields = clazz1.getDeclaredFields(); // 获取所有私有属性
	        
	        Annotation[] annotations = clazz1.getAnnotations(); // 获取类上的所有注解
	        
	        System.out.print("获取 User 类对应的 Class 对象方式 1" + " ——> ");
	        System.out.println("clazz1：" + clazz1);
	        System.out.println();
	        
	        System.out.print("获取 User 类对应的 Class 对象方式 2" + " ——> ");
	        System.out.println("clazz2：" + clazz2);
	        System.out.println();
	        
	        System.out.print("获取类名，含包名" + " ——> ");
	        System.out.println("clazzName：" + clazzName);
	        System.out.println();
	        
	        System.out.print("获取类名，不含包名" + " ——> ");
	        System.out.println("clazzSimpleName：" + clazzSimpleName);
	        System.out.println();
	        
	        System.out.print("获取包" + " ——> ");
	        System.out.println("package1：" + package1);
	        System.out.println();
	        
	        System.out.print("获取类修饰符" + " ——> ");
	        System.out.println("iMod：" + iMod);
	        System.out.println();
	        
	        System.out.print("判断修饰符" + " ——> ");
	        System.out.println("isPublic：" + isPublic);
	        System.out.println();
	        
	        System.out.print("判断修饰符" + " ——> ");
	        System.out.println("isProtected：" + isProtected);
	        System.out.println();
	        
	        System.out.print("获取父类" + " ——> ");
	        System.out.println("superClass：" + superClass);
	        System.out.println();
	        
	        System.out.print("获取所有实现接口数量及明细" + " ——> ");
	        System.out.println("interfaces：" + interfaces.length);
	        for (Class<?> class1 : interfaces) {
	            System.out.println("interface：" + class1.getName());
	        }
	        System.out.println();
	        
	        System.out.print("获取所有构造函数数量及明细" + " ——> ");
	        System.out.println("constructors：" + constructors.length);
	        for (Constructor<?> constructor : constructors) {
	            System.out.println("constructor：" + constructor.getName());
	        }
	        System.out.println();
	        
	        System.out.print("获取所有公有方法（包括继承的方法，但是不包括私有的方法）" + " ——> ");
	        System.out.println("methods：" + methods.length);
	        for (Method method1 : methods) {
	            System.out.println("method：" + method1.getName());
	        }
	        System.out.println();
	        
	        System.out.print("获取所有方法（包括私有，但是不包括继承的方法）" + " ——> ");
	        System.out.println("declaredMethods：" + declaredMethods.length);
	        for (Method method2 : declaredMethods) {
	           System.out.println("method：" + method2.getName());
	        }
	        System.out.println();
	        
	        System.out.print("调用获取的公有方法" + " ——> ");
	        System.out.println("methodInvokeValue：" + methodInvokeValue);
	        System.out.println();
	        
	        System.out.print("调用获取的私有方法" + " ——> ");
	        System.out.println("declaredMethodInvokeValue：" + declaredMethodInvokeValue);
	        System.out.println();
	        
	        System.out.print("获取所有公有属性" + " ——> ");
	        System.out.println("fields：" + fields.length);
	        for (Field field : fields) {
	            System.out.println("field：" + field.getName());
	        }
	        System.out.println();
	        
	        System.out.print("获取所有私有属性" + " ——> ");
	        System.out.println("declaredFields：" + declaredFields.length);
	        for (Field field : declaredFields) {
	            System.out.println("declaredField：" + field.getName());
	        }
	        System.out.println();
	        
	        System.out.print("获取类上的所有注解" + " ——> ");
	        System.out.println("annotations：" + annotations.length);
	        for (Annotation annotation : annotations) {
	            System.out.println("annotation：" + annotation.annotationType());
	        }
	        System.out.println();
	        
	    }
	    
	}

运行结果：

	获取 User 类对应的 Class 对象方式 1 ——> clazz1：class com.gorge4j.User
	
	获取 User 类对应的 Class 对象方式 2 ——> clazz2：class com.gorge4j.User
	
	获取类名，含包名 ——> clazzName：com.gorge4j.User
	
	获取类名，不含包名 ——> clazzSimpleName：User
	
	获取包 ——> package1：package com.gorge4j
	
	获取类修饰符 ——> iMod：1
	
	判断修饰符 ——> isPublic：true
	
	判断修饰符 ——> isProtected：false
	
	获取父类 ——> superClass：class java.lang.Object
	
	获取所有实现接口数量及明细 ——> interfaces：0
	
	获取所有构造函数数量及明细 ——> constructors：1
	constructor：com.gorge4j.User
	
	获取所有公有方法（包括继承的方法，但是不包括私有的方法） ——> methods：15
	method：toString
	method：getName
	method：setName
	method：setAge
	method：setSex
	method：getSex
	method：getAge
	method：wait
	method：wait
	method：wait
	method：equals
	method：hashCode
	method：getClass
	method：notify
	method：notifyAll
	
	获取所有方法（包括私有，但是不包括继承的方法） ——> declaredMethods：8
	method：toString
	method：getName
	method：setName
	method：setAge
	method：setSex
	method：doSomething
	method：getSex
	method：getAge
	
	调用获取的公有方法 ——> methodInvokeValue：User [name=zhangsan, sex=true, age=18]
	
	调用获取的私有方法 ——> declaredMethodInvokeValue：xxx
	
	获取所有公有属性 ——> fields：0
	
	获取所有私有属性 ——> declaredFields：3
	declaredField：name
	declaredField：sex
	declaredField：age
	
	获取类上的所有注解 ——> annotations：0

<a name="3"></a>

## 反射机制的作用  
1、反射机制是 `Java` 中的一种强大的工具，通过反射能够很方面灵活的创建代码，这些代码可以在运行时再装配，组件及服务之间无需通过源代码连接（仅仅通过接口来引用或依赖），从而达到解耦的目的；

<a name="4"></a>

## 常见问题  

<a name="4.1"></a>

**1、`.getClass()` 与 `.class` 的区别？**  
`.getClass()` 是类的实例（即对象）才有的方法，而具体的类是没有这个方法的。举个例子：`String s = new String();` 这里可以通过 `s.getClass()` 来获取实例（即对象）的类型类，但是不能通过 `String.getClass()` 来获取实例（即对象）的类型类，`Java` 语法不支持。  

一般的类默认是没有 `getClass()` 方法的，但是所有类（除了 `Object` 类）都是直接或间接继承自 `Object` 类，而 `Object` 类是包含 `getClass()` 方法的，所以所有的类都可以使用 `getClass()` 这个方法。  

从下面的执行结果可以看出，代码中的 `a.getClass()` 与 `A.class` 是等价的，结果都是 `A` 的类型类。  

	package com.gorge4j;
	public class A {
	    public static void main(String[] args) {
	        A a = new A();
	        if (a.getClass().equals(A.class)) {
	            System.out.println("Equal");
	        } else {
	            System.out.println("Unequal");
	        }
	    }
	}
	
	// 运行结果：Equal
	
需要特别注意的是，类型类是一一对应的，父类的类类型和子类的类类型是不一样的，如下面示例：  

	package com.gorge4j;
	public class A {
	    public static void main(String[] args) {
	        A a = new A();
	        if (a.getClass().equals(B.class)) {
	            System.out.println("Equal");
	        } else {
	            System.out.println("Unequal");
	        }
	    }
	}
	
	public class B extends A {   
	}
	
	// 运行结果：Unequal
	
综上，获得类型类的方式有两种：  
1、在知道类的实例的情况下，通过 `getClass()` 方法的方式获取；  
2、在知道类的类型的时候，通过 `.class` 的方式获取。强烈建议通过 `.class` 的方式获取类型类，因为只为调用 `getClass()` 方法获取类型类而创建一个对象是在浪费资源（内存的分配及回收）。  

其它的差异：  
`getClass()` 方法是在运行时加载，属于动态加载。而 `.class` 是编译时加载，属于静态加载。另外，有一个相似的知识点这里也补充一下，构造函数创建对象 `A a = new A();` 属于静态加载，而 `Class.forName("com.gorge4j.A")` 属于动态加载。区别在于静态加载的类在编译时就需要提供，而动态加载的类在运行时才需要提供，在编译阶段是不需要的。  
举个例子：  

	package com.gorge4j;
	public class A {
	    public static void main(String[] args) {
	        A a = new B();
	        if (a.getClass().equals(B.class)) {
	            System.out.println("Equal");
	        } else {
	            System.out.println("Unequal");
	        }
	        System.out.println(a.getClass().getName());
	        System.out.println(B.class.getName());
	    }
	}
	
	public class B extends A {   
	}
	
	// 运行结果：
	// Equal
	// com.gorge4j.B
	// com.gorge4j.B
	
上边示例结果也说明，`getClass()` 方法是程序运行时获取对象的类类型，而 `.class` 是在编译阶段就确定了的，与运行时状态无关。

综上，除了内部类的其它类，`.class` 功能上完全等同于 `getClass()`。区别在于一个是用类直接获得的，一个是通过实例获得的。  

<a name="4.2"></a>

**2、`Object` 类与 `Class` 类的区别？**  
实际上 `Object` 类与 `Class` 类并没有什么直接的关系，下面分别说明：  
`Object` 类是所有其它类的父类，其它类默认继承了 `Object`类，比如其它类就可以很轻松的直接使用 `Object` 类的 `getClass()` 及 `toString()` 等方法。  
`Class` 类是用于 `Java` 反射机制的，所有 `Java` 类都有一个对应的 `Class` 对象，`Class` 类是一个 `final` 类。`Class` 类的实例表示运行中的 `Java` 应用程序中的类或者接口。  
在 `Java` 程序编写中，当编写完一个类，编译完成后，在生成的 `.class` 文件中，就会生成一个 `Class` 对象，用来表示这个类的类型信息。  
获取 `Class` 实例的三种方式：  
1、通过类的实例（即对象）调用 `getClass()` 方法获取该对象的 `Class` 实例；  
2、通过 `Class` 类的静态方法 `forName()`，通过类的路径及名字获取一个 `Class` 实例；  
3、通过 `.class`	的方式来获取 `Class` 实例，对于基本数据类型的包装类，还可以通过 `.TYPE` 来获取对应的基本数据类型的 `Class` 实例。  
举个例子：  

	package com.gorge4j;
	
	public class A {
	    public static void main(String[] args) throws ClassNotFoundException {
	        A a = new A();
	        Class<?> cx = a.getClass();
	        Class<?> cf = Class.forName("com.gorge4j.A"); // 注意：类名的字符串必须是完整的包名+类名
	        Class<A> ca = A.class;
	        if (cx.equals(A.class) && cx.equals(ca) && ca.equals(A.class) && ca.equals(cf)) {
	            System.out.println("Equal");
	        } else {
	            System.out.println("Unequal");
	        }
	    }
	}
	
	// 运行结果：Equal

<a name="4.3"></a>

**3、通过构造函数创建对象实例与通过反射创建对象的实例性能有多大差异？**  
以下是个简单的代码示例，供参考：  

	package com.gorge4j;
	
	public class ReflectTest {
	
	    public static void main(String[] args) throws Exception {
	        noReflection();
	        hasReflection();
	    }
	
	    public static void noReflection() throws Exception {
	        long start = System.currentTimeMillis();
	        for (int i = 0; i < 10000000; i++) {
	            A a = new A();
	            a.toString();
	        }
	        System.out.println(System.currentTimeMillis() - start);
	    }
	
	    public static void hasReflection() throws Exception {
	        long start = System.currentTimeMillis();
	        for (int i = 0; i < 10000000; i++) {
	            A a = (A) Class.forName("com.gorge4j.A").newInstance();
	            a.toString();
	        }
	        System.out.println(System.currentTimeMillis() - start);
	    }
	
	}

	// 运行结果：
	1579
	5312
	
上面的运行示例可以看出，两种方式执行一千万次的时间差异才区区几秒钟，当然这个执行逻辑比较简单，这里想说明的意思是性能的损耗在这种场景下其实并不多。主要的损耗之处在于，通过反射查找类代价会更大。由于反射涉及动态解析类型，因此无法执行某些 Java 虚拟机优化。因此，反射操作的性能比非反射操作要慢，因此在性能敏感的应用程序中经常调用的代码中（例如高并发的场景、循环次数极多的场景等）应该避免反射操作，一般的使用场景下可以不用太考虑性能的问题。但是如果仅仅是创建对象的实例的话，性能的差异就会非常大，因为 JVM 会进行一些优化。如下示例：  

	package com.gorge4j;
	
	public class ReflectTest {
	
	    public static void main(String[] args) throws Exception {
	        noReflection();
	        hasReflection();
	    }
	
	    public static void noReflection() throws Exception {
	        long start = System.currentTimeMillis();
	        for (int i = 0; i < 10000000; i++) {
	            // 此处仅仅是创建对象的实例
	            A a = new A();
	        }
	        System.out.println(System.currentTimeMillis() - start);
	    }
	
	    public static void hasReflection() throws Exception {
	        long start = System.currentTimeMillis();
	        for (int i = 0; i < 10000000; i++) {
	            // 此处仅仅是创建对象的实例
	            A a = (A) Class.forName("com.gorge4j.A").newInstance();
	        }
	        System.out.println(System.currentTimeMillis() - start);
	    }
	
	}

	// 运行结果：
	4
	4263
