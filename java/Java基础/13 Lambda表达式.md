# Java开发实战经验-Lambda表达式相关知识  

说明：本文将通过图文并茂的方式来讲解 `Lambda` 表达式相关的知识。  

> 注：本文基于的硬 / 软件环境  

> 硬件：MacBook Pro    
> 操作系统：macOS Mojave 10.14.4  
> JDK版本：1.8.0_211  

## 目录
* [`Lambda` 简介](#0)  
* [`Lambda` 表达式的语法格式](#1)  
* [自定义 `Lambda` 表达式](#2)
* [实例讲解 `Lambda` 表达式的简化](#3)
    * [`Lambda` 表达式之完整版](#3.1)
    * [`Lambda` 表达式之省略参数类型版](#3.2)
    * [`Lambda` 表达式之省略小括弧版](#3.3)
    * [`Lambda` 表达式之省略大括弧版](#3.4)
    * [`Lambda` 表达式之方法引用省略版](#3.5)
* [`Lambda` 表达式之方法引用](#4)
* [`Lambda` 表达式简化的流程](#5)
* [`Lambda` 表达式的实现原理](#6)
* [`Java 8` 新特性：`Stream`](#7)
    * [`Stream` 示例](#7.1)
    * [什么是 `Stream`](#7.2)
    * [集合接口生成流](#7.3)
    * [`Stream` 的操作类型](#7.4)
    * [`Stream` 统计](#7.5)
    * [`Stream` 的操作流程](#7.6)
    * [`Stream Collectors`](#7.7)
    * [`Stream 综合应用示例`](#7.8)
    * [未使用 `Stream` 以及 使用了 `Stream` 对比示例](#7.9)
    * [`Stream` 的调试](#7.9)
* [部分参考网站](#8)

<a name="0"></a>

## `Lambda` 简介  
`Java SE 8` 的一个大亮点是引入了 `Lambda` 表达式，使用它设计的代码会更加简洁。当开发者在编写 `Lambda` 表达式时，也会随之被编译成一个函数式接口，`Lambda` 允许把函数作为一个方法的参数传递进方法中。`Lambda` 表达式本质是匿名函数（或者叫匿名方法），也可称之为闭包，像方法一样， `Lambda` 表达式具有带类型的参数、主体和返回类型。可使用 `Lambda` 语法来代替匿名的内部类，代码不仅简洁而且还可读。当然，不足之处可能就是可读性稍差及调试稍麻烦一些。

<a name="1"></a>

## `Lambda` 表达式的语法格式  
`Lambda` 表达式语法形如：`(……) -> {……}` ，前面的小括弧里代表了参数体，后面的大括弧里代表了方法体（处理逻辑及返回结果）。能够用于 `Lambda` 表达式的只能是 `interface`，且 `interface` 中只有一个方法。这里提到的参数和方法体对应的就是 `interface` 中的参数及方法体。如下图：  
![](../../image/java/kfszjy/lambda/lambda_001.png)   
格式也有下面的表现形式： 
 
	(parameters) -> {statements;}  
	
或  

	(parameters) -> expression // 当后面的主体仅包含一条语句时，“{}” 和 return 都可以省掉了，编译器认为，既然只有一条语句，那么这个语句的执行结果应该就是返回值  
	
或

	parameter -> expression // 当仅有一个参数，且后面的主体只包含一条语句时，“()” 和 “{}” 都可以省掉
	
或

	() -> expression // 当没有参数时，可用 “()”
	
总结一下，表现形式大概如下：

	(parameters) | parameter | () -> {statements;} | expression

为了说的更详细一些，还有下面更复杂一点的表现形式，本质是一样的：  

	(Type1 param1, Type2 param2, ..., TypeN paramN) -> {  
	  statment1;  
	  statment2;  
	  //......  
	  return result;  
	}  

或

	(param1, param2, ..., paramN) -> { // 绝大多数情况下，编译器可以从上下文环境中推断出 Lambda 表达式的参数类型，所以参数的类型声明也可以省掉
	  statment1;
	  statment2;
	  //......
	  return result;
	}

通过以上表现形式，总结 `Lambda` 表达式的以下几个特征：
> 1、参数的类型声明可选，因为在绝大多数情况下编译器可以从上下文环境中推断出参数的类型；  
> 2、参数的圆括弧可选，一个参数无需圆括弧，多个参数或者没有参数都需要圆括弧；  
> 3、逻辑主体的大括弧可选，如果主体只包含一个语句，则无需大括弧；  
> 4、逻辑主体的返回关键字可选，如果主体只包含一个语句，编译器会自动返回值，大括弧需要指明返回值。  

<a name="2"></a>

## 自定义 `Lambda` 表达式  

示例如下：  

	package com.gorge4j;
	
	public class LambdaDemo {
	
	    public static void main(String[] args) {
	
	        LambdaDemo lambdaDemo = new LambdaDemo();
	
	        // 标准完整的写法
	        IMathOperation addition = (int x, int y) -> {
	            int z = x + y;
	            return z;
	        };
	
	        // 当后面的主体只有一条语句时可以省略大括弧
	        IMathOperation subtraction = (int x, int y) -> x - y;
	
	        // 绝大多数情况下，编译器可以从上下文环境中推断出 Lambda 表达式的参数类型
	        IMathOperation multiplication = (x, y) -> {
	            int z = x * y;
	            return z;
	        };
	
	        // 编译器可以从上下文环境中推断出 Lambda 表达式的参数类型，当逻辑主体只有一条语句时可以省略大括弧
	        IMathOperation division = (x, y) -> x / y;
	
	        // 当仅有一个参数，且后面的主体只包含一条语句时，“()” 和 “{}” 都可以省掉
	        ISingleOperation singleOperation = x -> ++x;
	
	        // 当没有参数的时候需要有小括弧，当后面的主体只有一条语句时可以省略大括弧
	        IOperation iOperation = () -> 5;
	
	        // 仅有一个参数，无返回值的例子
	        IVoidOperation iVoidOperation = message -> System.out.println("Hello " + message);
	
	        // 无参数，无返回值的例子
	        INoOperation iNoOperation = () -> System.out.println("Hello!");
	
	        System.out.println(lambdaDemo.operation(6, 2, addition));
	        System.out.println(lambdaDemo.operation(6, 2, subtraction));
	        System.out.println(lambdaDemo.operation(6, 2, multiplication));
	        System.out.println(lambdaDemo.operation(6, 2, division));
	        System.out.println(lambdaDemo.operation(6, singleOperation));
	        System.out.println(lambdaDemo.operation(iOperation));
	
	        lambdaDemo.operation("World!", iVoidOperation);
	        iVoidOperation.operation("World!");
	
	        lambdaDemo.operation(iNoOperation);
	        iNoOperation.operation();
	
	    }
	
	    // 对于重载的方法，Lambda 会根据表达式中的参数和返回值确定用的是哪个 operation 方法
	    private int operation(int x, int y, IMathOperation iMathOperation) {
	        return iMathOperation.operation(x, y);
	    }
	
	    // 对于重载的方法，Lambda 会根据表达式中的参数和返回值确定用的是哪个 operation 方法
	    private int operation(int x, ISingleOperation iSingleOperation) {
	        return iSingleOperation.operation(x);
	    }
	
	    // 对于重载的方法，Lambda 会根据表达式中的参数和返回值确定用的是哪个 operation 方法
	    private int operation(IOperation iOperation) {
	        return iOperation.operation();
	    }
	
	    // 对于重载的方法，Lambda 会根据表达式中的参数和返回值确定用的是哪个 operation 方法
	    private void operation(String s, IVoidOperation iVoidOperation) {
	        iVoidOperation.operation(s);
	    }
	
	    // 对于重载的方法，Lambda 会根据表达式中的参数和返回值确定用的是哪个 operation 方法
	    private void operation(INoOperation iNoOperation) {
	        iNoOperation.operation();
	    }
	
	    // 首先能够用于 Lambda 表达式的只能是 interface，且 interface 中只有一个方法
	    interface IMathOperation {
	        int operation(int x, int y); // 两个参数有返回值
	    }
	
	    // 首先能够用于 Lambda 表达式的只能是 interface，且 interface 中只有一个方法
	    interface ISingleOperation {
	        int operation(int x); // 一个参数有返回值
	    }
	
	    // 能够用于 Lambda 表达式的只能是 interface，且 interface 中只有一个方法。可以加上 @FunctionalInterface 表明这个接口是一个函数式接口，这样添加多个方法时编译器会提示报错
	    @FunctionalInterface
	    interface IOperation {
	        int operation(); // 无参数有返回值
	    }
	
	    // 首先能够用于 Lambda 表达式的只能是 interface，且 interface 中只有一个方法
	    interface IVoidOperation {
	        void operation(String s); // 有参数无返回值
	    }
	
	    // 首先能够用于 Lambda 表达式的只能是 interface，且 interface 中只有一个方法
	    interface INoOperation {
	        void operation(); // 无参数无返回值
	    }
	
	}
	
运行结果：  

	8
	4
	12
	3
	7
	5
	Hello World!
	Hello World!
	Hello!
	Hello!

<a name="3"></a>

## 实例讲解 `Lambda` 表达式的简化  

<a name="3.1"></a>

**`Lambda` 表达式之完整版**  

	package com.gorge4j;
	
	import java.util.Arrays;
	import java.util.List;
	
	public class LambdaSimplify {
	    
	    public static void main(String[] args) {
	        List<String> lst = Arrays.asList("C", "B", "A");
	        // Lambda 表达式之完整版
	        List<String> lstSimplify = lst.stream().map((String s) -> {
	            return s.toLowerCase();
	        }).collect(Collectors.toList());
	        System.out.println(lstSimplify);
	    }
	    
	}
	
运行结果：  

	[c, b, a]
	
<a name="3.2"></a>
	
**`Lambda` 表达式之省略参数类型版**  

	package com.gorge4j;
	
	import java.util.Arrays;
	import java.util.List;
	
	public class LambdaSimplify {
	    
	    public static void main(String[] args) {
	        List<String> lst = Arrays.asList("C", "B", "A");
	        // Lambda 表达式之省略参数类型版
	        List<String> lstSimplify = lst.stream().map((s) -> {
	            return s.toLowerCase();
	        }).collect(Collectors.toList());
	        System.out.println(lstSimplify);
	    }
	    
	}
	
运行结果：  

	[c, b, a]
	
<a name="3.3"></a>
	
**`Lambda` 表达式之省略小括弧版**  

	package com.gorge4j;
	
	import java.util.Arrays;
	import java.util.List;
	
	public class LambdaSimplify {
	    
	    public static void main(String[] args) {
	        List<String> lst = Arrays.asList("C", "B", "A");
	        // Lambda 表达式之参数类型省略小括弧版
	        List<String> lstSimplify = lst.stream().map(s -> {
	            return s.toLowerCase();
	        }).collect(Collectors.toList());
	        System.out.println(lstSimplify);
	    }
	    
	}
	
运行结果：  

	[c, b, a]
	
<a name="3.4"></a>
	
**`Lambda` 表达式之省略大括弧版**  

	package com.gorge4j;
	
	import java.util.Arrays;
	import java.util.List;
	import java.util.stream.Collectors;
	
	public class LambdaSimplify {
	
	    public static void main(String[] args) {
	        List<String> lst = Arrays.asList("C", "B", "A");
	        // Lambda 表达式方法体省略大括弧版本
	        List<String> lstSimplify = lst.stream().map(s -> s.toLowerCase())
	        	.collect(Collectors.toList());
	        System.out.println(lstSimplify);
	    }
	
	}
	
运行结果：  

	[c, b, a]
	
<a name="3.5"></a>
	
**`Lambda` 表达式之方法引用省略版**  

	package com.gorge4j;
	
	import java.util.Arrays;
	import java.util.List;
	import java.util.stream.Collectors;
	
	public class LambdaSimplify {
	
	    public static void main(String[] args) {
	        List<String> lst = Arrays.asList("C", "B", "A");
	        // Lambda 表达式方法引用省略版本
	        List<String> lstSimplify = lst.stream().map(String::toLowerCase)
	        	.collect(Collectors.toList());
	        System.out.println(lstSimplify);
	    }
	
	}
	
运行结果：  

	[c, b, a]
	
<a name="4"></a>

## `Lambda` 表达式之方法引用   
上面的示例可以看到，`Lambda` 表达式 `s -> s.toLowerCase()` 换成了 `String::toLowerCase` 的写法，这实际上是某些符合条件的 `Lambda` 表达式简洁写法。方法引用包含以下几种情况：  

>- 静态方法引用  
>- 构造方法引用  
>- 类成员方法引用  
>- 对象方法引用  

示例如下：  

	package com.gorge4j;
	
	import java.util.Arrays;
	import java.util.List;
	import java.util.function.Consumer;
	
	public class Car {
	    
	    private String company;
	    private String type;
	    
	    public Car() {}
	    
	    public Car(String company, String type) {
	        this.company = company;
	        this.type = type;
	    }
	
	    public static void main(String[] args) {
	        // 1、构造方法引用
	        // 原生的写法：
	        Car carWhole = Car.create(new Supplier<Car>() {
	            public Car get() {
	                return new Car();
	            }
	        });
	        // Lambda 表达式的形式
	        Car carSimplify = Car.create(() -> new Car());
	        // Lambda 表达式构造方法引用的形式
	        // Car 类存在一个不带参数的构造方法，编译器不需要根据参数列表猜测构造方法的参数（因为都是空的），简写如下
	        // 如果 Lambda 的参数个数和类的构造方法个数一致，也可以改写为类似的形式，只要是没有歧义即可，就是对于有参构造函数也适用
	        Car carContructor = Car.create(Car::new);
	        // 有参构造方法引用的示例
	        Car carContructorPlus = Car.createPlus("吉利", "沃尔沃", Car::new);
	        // 打印出各个生成的对象
	        System.out.println(carWhole.toString());
	        System.out.println(carSimplify.toString());
	        System.out.println(carContructor.toString());
	        System.out.println(carContructorPlus.toString());
	
	        // 2、静态方法引用
	        // 创建一个 Car 对象，然后将其添加进 List
	        final Car carStatic = Car.create(Car::new);
	        final List<Car> cars = Arrays.asList(carStatic);
	        // 遍历集合
	        cars.forEach(new Consumer<Car>() {
	            @Override
	            public void accept(Car car) {
	                Car.collide(car);
	            }
	        });
	        // 遍历改成 Lambda 的形式
	        cars.forEach(c -> Car.collide(c));
	        // Lambda 静态方法引用的形式
	        // 就是对传进来的 Car 对象执行静态方法。对于静态方法，编译器也不需要推断调用者（类名），当传入参数和静态方法所需参数个数一致时，就不存在歧义
	        cars.forEach(Car::collide);
	
	        // 3、类成员方法引用
	        // 类的成员方法不能通过类名直接调用，只能通过对象来调用，也就是 Lambda 表达式的第一个参数，是方法的调用者
	        cars.forEach(c -> c.repair());
	        // Lambda 类成员方法的引用形式
	        cars.forEach(Car::repair);
	
	        // 4、对象方法引用
	        // 与类方法引用不同的是，对象方法引用方法的调用者是一个外部的对象（下面示例中的 carObject）
	        final Car carObject = Car.create(Car::new);
	        cars.forEach(c -> carObject.follow(c));
	        // Lambda 对象方法引用的形式
	        cars.forEach(carObject::follow);
	    }
	
	    // 通过 create 获取 LambdaMethodQuote 实例
	    private static Car create(Supplier<Car> supplier) {
	        return supplier.get();
	    }
	    
	    // 通过 create 获取 LambdaMethodQuote 实例
	    private static Car createPlus(String company, String type, SupplierPlus<Car> supplierPlus) {
	        return supplierPlus.get(company, type);
	    }
	
	    // 静态方法，一个入参 Car 对象
	    private static void collide(final Car lambdaMethodQuote) {
	        System.out.println("Collide " + lambdaMethodQuote.toString());
	    }
	
	    // 一个入参 Car
	    private void follow(final Car lambdaMethodQuote) {
	        System.out.println("Follow " + lambdaMethodQuote.toString());
	    }
	
	    // 没有入参
	    private void repair() {
	        System.out.println("Repair " + this.toString());
	    }
	
	    @FunctionalInterface
	    interface Supplier<T> {
	        T get();
	    }
	    
	    @FunctionalInterface
	    interface SupplierPlus<T> {
	        T get(String company, String type);
	    }
	    
	    public String getCompany() {
	        return company;
	    }
	
	    public void setCompany(String company) {
	        this.company = company;
	    }
	
	    public String getType() {
	        return type;
	    }
	
	    public void setType(String type) {
	        this.type = type;
	    }
	    
	    @Override
	    public String toString() {
	        return "Car [company=" + company + ", type=" + type + "]";
	    }
	
	}

运行结果：  

	Car [company=null, type=null]
	Car [company=null, type=null]
	Car [company=null, type=null]
	Car [company=吉利, type=沃尔沃]
	Collide Car [company=null, type=null]
	Collide Car [company=null, type=null]
	Collide Car [company=null, type=null]
	Repair Car [company=null, type=null]
	Repair Car [company=null, type=null]
	Follow Car [company=null, type=null]
	Follow Car [company=null, type=null]
	
<a name="5"></a>

## `Lambda` 表达式的实现原理  
`Lambda` 表达式本质是通过内部类来实现的。举个例子：  

	@FunctionalInterface
	interface Print<T> {
	    public void print(T x);
	}
	public class Lambda {   
	    public static void PrintString(String s, Print<String> print) {
	        print.print(s);
	    }
	    public static void main(String[] args) {
	        PrintString("test", (x) -> System.out.println(x));
	    }
	}
	
等价于下面的形式：  

	@FunctionalInterface
	interface Print<T> {
	    public void print(T x);
	}
	public class Lambda {   
	    public static void PrintString(String s, Print<String> print) {
	        print.print(s);
	    }
	    private static void lambda$0(String x) {
	        System.out.println(x);
	    }
	    final class $Lambda$1 implements Print{
	        @Override
	        public void print(Object x) {
	            lambda$0((String)x);
	        }
	    }
	    public static void main(String[] args) {
	        PrintString("test", new Lambda().new $Lambda$1());
	    }
	}
	
<a name="6"></a>

## `Lambda` 表达式简化的流程  
如下图：  
![](../../image/java/kfszjy/lambda/lambda_002.png)   

<a name="7"></a>

## `Java 8` 新特性：`Stream`  
>- `Java 8` 新增了新的特性称为流 `Stream`（包 `java.util.stream.Stream`），作为 `Java 8` 的一大亮点，它与常见的流操作（`InputStream` 及 `OutputStream`）是完全不同的概念。`Java 8` 中的 `Stream` 是对集合（`Collection`）对象功能的增强，适用于对集合对象进行各种便利、高效的过滤、映射、排序及聚合等操作，可以独立也可以组合成复杂的操作。`Stream API` 配合同样是新的特性 `Lambda` 表达式，可以极大的提高编程效率。并且 `Stream` 还同时支持串行和并行两种操作模式，并行模式能够充分利用目前主流的多核处理器的优势，使用 Fork/Join 并行方式来拆分和加速任务的处理过程。`Stream API` 无需编写多线程代码，通过并发模式很容易就支持任务的并行处理。有了 `Lambda` 表达式及目前主流的多核处理器加持，`Stream API` 的特性在集合处理中大放异彩。  
>- `Stream API` 使用一种类似 SQL 语句从数据库查询数据的直观方式来操作 `Java` 集合数据，可以写出高效率、干净、简洁的代码，极大的提高集合操作的生产力。可以将要处理的集合元素看作一种流，流在管道中传输，并且可以在管道传输的各个节点上进行筛选、排序及聚合等操作。流在管道中经过中间操作（intermediate operation）的处理，最后由最终操作（terminal operation）得到前面处理的结果。  

<a name="7.1"></a>

**`Stream` 示例**  

	package com.gorge4j;
	
	import java.util.List;
	import java.util.stream.Collectors;
	import java.util.stream.Stream;
	
	public class StreamDemo {
	    
	    public static void main(String[] args) {
	        
	        List<String> peekList = Stream.of("one", "tow", "three", "four", "five", "six", "seven")
	                .filter(e -> e.length() > 3) // 过滤出字符串长度大于 3 的元素
	                .peek(e -> System.out.println("Filtered value: " + e))
	                .map(String::toUpperCase) // 将元素转为大写
	                .peek(e -> System.out.println("Mapped value: " + e))
	                .collect(Collectors.toList()); // 将输出 Stream 转换为 List
	        
	        System.out.println("集合打印：");
	        for (String string : peekList) {
	            System.out.println(string);
	        }
	        
	    }
	
	}

	// 运行结果 
	Filtered value: three
	Mapped value: THREE
	Filtered value: four
	Mapped value: FOUR
	Filtered value: five
	Mapped value: FIVE
	Filtered value: seven
	Mapped value: SEVEN
	集合打印：
	THREE
	FOUR
	FIVE
	SEVEN
	
<a name="7.2"></a>
	
**什么是 `Stream`**    
`Java Stream`（流） 是一个来自于数据源（集合、数组等）的元素队列并支持筛选、排序及聚合等操作。  
>- “元素”是指特定类型的对象，形成一个队列。`Java` 中的 `Stream` 不是数据结构，并不存储元素的数据，而是按需要进行各种筛选、排序及聚合等操作；  
>- “数据源”是指数据的来源。可以是集合、数组、I/O channel、产生器 generator 等；  
>- “操作”是指类似于 SQL 语句的操作。例如：filter、map、reduce、find、match、sorted 等。  

和 `Collection`（集合）操作不同的是，`Java Stream` 还有两个基础的特征：  
>- Pipelining：中间操作都会返回流对象本身。 这样多个操作可以串联成一个管道， 如同流式风格（fluent style）。 这样做可以对操作进行优化， 比如延迟执行（laziness）和短路（short-circuiting）；  
>- 内部迭代：以前对集合遍历都是通过 `Iterator` 或者 `For-Each` 的方式, 显式的在集合外部进行迭代， 这叫做外部迭代。 `Java Stream` 提供了内部迭代的方式， 通过访问者模式（Visitor）实现。  

另外，`Java Stream` 的并行操作依赖于 `Java 7` 中引入的 `Fork/Join` 框架（JSR166y）来拆分任务进行并行计算，总体来说 `Java` 的并行 `API` 演变如下：  
>- JDK 1.0~1.4：java.lang.Thread  
>- JDK 5.0：java.util.concurrent  
>- JDK 6.0：Phaser 等  
>- JDK 7.0：Fork/Join  
>- JDK 8.0：Lambda  

<a name="7.3"></a>

**集合接口生成流**  
> `stream()`：为集合创建串行流，默认创建的就是串行的  
> `parallelStream()`：为集合创建并行流，可以对流进行并行处理  

示例如下：  

	List<String> list = Arrays.asList("asd", "", "fgh", "jkl", "", "qwerty");
	List<String> filtered = list.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
	
	List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
	numbers.parallelStream().forEach(System.out::println);
	
	List<String> list = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
	// 获取空字符串的数量
	long count = list.parallelStream().filter(string -> string.isEmpty()).count();

<a name="7.4"></a>

**`Stream` 的操作类型**  
>- 中间操作（Intermediate Operation）：  
一个流可以后面跟随零个或多个 intermediate 操作。其目的主要是打开流，做出某种程度的数据映射/过滤，然后返回一个新的流，交给下一个操作使用。这类操作都是惰性化的（lazy），就是说，仅仅调用到这类方法，并没有真正开始流的遍历。  
常见操作：map (mapToInt, flatMap 等)、 filter、distinct、peek、limit、skip、sorted、parallel、sequential、unordered。  

>- 终结操作（Terminal Operation）：  
一个流只能有一个 terminal 操作，当这个操作执行后，流就被使用“光”了，无法再被操作。所以这必定是流的最后一个操作。Terminal 操作的执行，才会真正开始流的遍历，并且会生成一个结果，或者一个 side effect（副作用）。  
常见操作：forEach、forEachOrdered、toArray、reduce、collect、min、max、count、anyMatch、allMatch、noneMatch、findFirst、findAny、iterator。  

`map`：对于 `Stream` 中包含的元素使用给定的转换函数进行转换操作，新生成的 `Stream` 只包含转换生成的元素。  

	List<Integer> numberList = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
	// 获取对应的倍数
	List<Integer> multipleList = numberList.stream().map( i -> 2*i).distinct().collect(Collectors.toList());
	multipleList.forEach(System.out::println);
	
	List<String> wordList = Arrays.asList("a", "b", "c");
	// 转换为大写
	List<String> upperWordList = wordList.stream().map(String::toUpperCase).collect(Collectors.toList());
	upperWordList.forEach(System.out::println);
	
图示如下：  
![](../../image/java/kfszjy/lambda/stream_map.png)  

`flatMap`：和 `map` 类似，不同的是其每个元素转换得到的是 `Stream` 对象，会把子 `Stream` 中的元素压缩到父集合中。  

	// 从 map 的例子可以看出，map 生成的是 1:1 映射，每个输入元素，都按照规则转换成为另外一个元素。还有一些场景是一对多映射关系的，此时需要 flatMap。
	// 一对多：flatMap 把 input Stream 中的层级结构扁平化，就是将最底层元素抽出来放到一起，最终 output 的新 Stream 里面已经没有 List 了，都是直接的数字。
	Stream<List<Integer>> inputStream = Stream.of(
		Arrays.asList(1, 2, 3),
		Arrays.asList(30, 600, 9),
		Arrays.asList(300, 400, 500)
	);
	// List<Integer> numberList = inputStream.flatMap(c -> c.stream()).collect(Collectors.toList()); // 可简写为下面的
	List<Integer> numberList = inputStream.flatMap(Collection::stream).collect(Collectors.toList());
	numberList.forEach(System.out::println);
	
图示如下：  
![](../../image/java/kfszjy/lambda/stream_flatMap.png)  
	
`filter`：用于通过设置的条件过滤出元素，新生成的 `Stream` 只包含符合条件的元素。  

	List<String> list = Arrays.asList("abc", "", "cd", "efg", "hij", "", "jkl");
	// 过滤出空的字符串
	long count = list.stream().filter(string -> string.isEmpty()).count();
	
	Integer[] numberArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	// 过滤出偶数
	List<Integer> list = Stream.of(numberArray).filter(n -> n % 2 == 0).collect(Collectors.toList());
	
图示如下：  
![](../../image/java/kfszjy/lambda/stream_filter.png)   

`distinct`：对于 `Stream` 中包含的元素进行去重操作（去重逻辑依赖元素的 `equals` 方法），新生成的 `Stream` 中没有重复的元素。  

	List<String> wordList = Arrays.asList("a", "l", "i", "b", "a", "b", "a");
	List<String> list = wordList.stream().distinct().collect(Collectors.toList());

图示如下：  
![](../../image/java/kfszjy/lambda/stream_distinct.png)   
	
`peek`：生成一个包含原 `Stream` 的所有元素的新 `Stream`，同时会提供一个消费函数（`Consumer` 实例），新 `Stream` 每个元素被消费的时候都会执行给定的消费函数。  

	List<String> peekList = Stream.of("one", "two", "three", "four", "five", "six").peek(System.out::println).collect(Collectors.toList());
	
图示如下：  
![](../../image/java/kfszjy/lambda/stream_peek.png)  

> peek 的常用场景：
`forEach` 是 `Terminal` 操作，因此它执行后，`Stream` 的元素就被“消费”掉了，无法对一个 `Stream` 进行两次 `Terminal` 运算。相反，具有相似功能的 `Intermediate` 操作 `peek` 可以达到上述目的。
	
`limit`：用于获取指定数量的流，对一个 `Stream` 进行截断操作，获取其前 N 个元素，如果原 `Stream` 中包含的元素个数小于 N，那就获取其所有的元素。    

	Random random = new Random();
	// 截断打印出 10 条数据
	random.ints().limit(10).forEach(System.out::println);
	
	List<String> list = Arrays.asList("one", "two", "three", "four", "five");
	// 截断打印出集合中的前两个元素
	list.stream().limit(2).forEach(System.out::println);
	// 或者
	Stream.of("one", "two", "three", "four", "five", "six").limit(2).forEach(System.out::println);
	
图示如下：  
![](../../image/java/kfszjy/lambda/stream_limit.png)  

`skip`：返回一个丢弃原 `Stream` 的前 N 个元素后剩下元素组成的新 `Stream`，如果原 `Stream` 中包含的元素个数小于 N，那么返回空 Stream。  

	List<String> skipList = Stream.of("one", "two", "three", "four", "five", "six").skip(2).collect(Collectors.toList());
	
图示如下：  
![](../../image/java/kfszjy/lambda/stream_skip.png)  
	
`sorted`：用于对流进行排序，以下代码片段示例使用 `sorted` 对输出的 10 个随机数进行排序：  

	Random random = new Random();
	random.ints().limit(10).sorted().forEach(System.out::println);

`Stream` 操作的综合应用：  

	package com.gorge4j;
	
	import java.util.Arrays;
	import java.util.List;
	import java.util.Objects;
	
	public class StreamDemo {
	
	    public static void  main(String[] args) {
	        
	        List<Integer> numberList =  Arrays.asList(1, 1, null, 2, 3, 5, null, 8, 13, 21, 24, 45);
	        System.out.println("sum is：" + numberList
	            .stream() // 获取 List 的 Stream 对象
	            .filter(Objects::nonNull) // 过滤掉 null 值
	            .distinct() // 去掉重复的值
	            .mapToInt(num -> num * 2) // 每个元素乘以 2
	            .peek(System.out::println) // 每个元素被消费时打印自身
	            .skip(2) // 跳过前两个元素
	            .limit(4) // 最后取前 4 个元素
	            .sum()); // 求和
	        
	    }
	
	}

	// 运行结果：
	2
	4
	6
	10
	16
	26
	sum is：58

<a name="7.5"></a>

**`Stream` 统计**  
一些产生统计结果的收集器也比较常用，主要用于 `int`、`double`、`long` 等基本数据类型，可以用来统计结果：  

	List<Integer> list = Arrays.asList(3, 5, 3, 8, 7, 4, 2);
	IntSummaryStatistics statistics = list.stream().mapToInt((x) -> x).summaryStatistics();
	System.out.println("列表中最大的数 : " + statistics.getMax());
	System.out.println("列表中最小的数 : " + statistics.getMin());
	System.out.println("所有数之和 : " + statistics.getSum());
	System.out.println("平均数 : " + statistics.getAverage());

<a name="7.6"></a>

**`Stream` 的操作流程**  
>- 第一步，获取数据源，创建/构造 `Stream` 流；【创建/生成】  
>- 第二步，通过中间操作，进行 `Stream` 的转换；【 操作、变换（可以多次）】  
>- 第三步，通过终止操作，获取处理的结果。  【消费/消耗（只有一次）】

构造 `Stream` 流的示例：  

	// 独立创建
	Stream<String> stream = Stream.of("a", "b", "c");
	// 通过数组创建
	String[] strArray = new String[]{"a", "b", "c"};
	stream = Stream.of(strArray);
	stream = Arrays.stream(strArray);
	// 通过集合创建
	List<String> list = Arrays.asList(strArray);
	stream = list.stream();

<a name="7.7"></a>

**`Stream Collectors`**  
Collectors 类的静态工厂方法：  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <head>
            <th>工厂方法</th>
            <th>返回类型</th>
            <th>作用</th>
        </head>
        <tr>
            <td>toCollection</td>
            <td>Collection<T></td>
            <td>返回一个 Collector ，按照遇到的顺序将输入元素累加到一个新的 Collection 中。stream.collect(toCollection(), ArrayList::new)</td>
        </tr>
        <tr>
            <td>toList</td>
            <td>List<T></td>
            <td>返回一个 Collector ，它将输入元素 List 到一个新的 List </td>
        </tr>
        <tr>
            <td>toSet</td>
            <td>Set<T></td>
            <td>返回一个 Collector ，将输入元素 Set 到一个新的 Set，删除重复项</td>
        </tr>
        <tr>
            <td>joining</td>
            <td>String</td>
            <td>连接对流中每个项目调用 toString 方法所生成的字符串，有三个可用的方法，例如 collect(joining(", ")) 或 collect(joining()) 或 collect(joining(", ", "Log_", "_2019-10-23"))</td>
        </tr>
        <tr>
            <td>mapping</td>
            <td>String</td>
            <td>适应一个 Collector 类型的接受元件 U 至类型的一个接受元件 T 通过积累前应用映射函数到每个输入元素。</td>
        </tr>
        <tr>
            <td>counting</td>
            <td>Long</td>
            <td>返回 Collector 类型的元素 T 计数输入元件的数量，计算流中元素的个数</td>
        </tr>
        <tr>
            <td>sumInt</td>
            <td>Integer</td>
            <td>返回一个 Collector ，它产生应用于输入元素的整数值函数的和，简单来说就是对流中的一个整数属性求和</td>
        </tr>
        <tr>
            <td>averagingInt</td>
            <td>Double</td>
            <td>返回一个 Collector ，它产生应用于输入元素的整数值函数的算术平均值。简单来说就是计算流中的一个整数属性的平均值</td>
        </tr>
        <tr>
            <td>averagingLong</td>
            <td>Double</td>
            <td>返回一个Collector ，它产生应用于输入元素的长整型函数的算术平均值。如果没有元素，结果为 0。简单来说就是计算流中的一个长整型数属性的平均值。</td>
        </tr>
        <tr>
            <td>averagingDouble</td>
            <td>Double</td>
            <td>返回一个Collector ，它产生应用于输入元素的双精度函数的算术平均值。如果没有元素，结果为 0。简单来说就是计算流中的一个双精度数属性的平均值。</td>
        </tr>
        <tr>
            <td>summarizingInt</td>
            <td>IntSummaryStatistics</td>
            <td>收集关于流中项目 Integer 属性的统计值，例如最大、最小、总和与平均值</td>
        </tr>
        <tr>
            <td>maxBy</td>
            <td>Optional<T></td>
            <td>一个包裹了流中按照给定比较器选出的最大元素的 Optional，或如果流为空则为 Optional.empty()</td>
        </tr>
        <tr>
            <td>minBy</td>
            <td>Optional<T></td>
            <td>一个包裹了流中按照给定比较器选出的最小元素的 Optional，或如果流为空则为 Optional.empty()</td>
        </tr>
        <tr>
            <td>reducing</td>
            <td>归约操作产生的类型</td>
            <td>从一个作为累加器的初始值开始，利用 BinaryOperator 与流中的元素逐个结合，从而将流归约为单个值累加 int totalCalories = stream.collect(reducing(0, Dish::getCalories, Integer::sum));</td>
        </tr>
        <tr>
            <td>collectingAndThen</td>
            <td>转换函数返回的类型</td>
            <td>包裹另外一个收集器，对其结果应用转换函数 int howManyDishes = stream.collect(collectingAndThen(toList, List::size));</td>
        </tr>
        <tr>
            <td>groupingBy</td>
            <td>Map<K, List<T>></td>
            <td>根据项目的一个属性的值对流中的项目作为组，并将属性值作为结果 Map 键</td>
        </tr>
        <tr>
            <td>partitioningBy</td>
            <td>Map<Boolean, List<T>></td>
            <td>根据对流中每个项目应用谓词的结果来对项目进行分区</td>
        </tr>
</table>

`Collectors` 类实现了很多规约操作，例如将流转换成集合或者聚合元素，下面的示例是返回列表和字符串：  

	List<String>strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
	List<String> list = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList()); // 筛选列表
	String string = strings.stream().filter(s -> !s.isEmpty()).collect(Collectors.joining(", ")); // 合并字符串

下面示例是 `Stream` 转换为其它数据结构：  

	Stream<String> stream = Stream.of("a", "b", "c");
	// 转换为字符串
	String string = stream.collect(Collectors.joining()); // string = abc
	// 转换为数组
	String[] array = stream.toArray(String[]::new); // array = [a, b, c]
	// 转换为常用集合
	List<String> list = stream.collect(Collectors.toList()); // list = [a, b, c]
	Set set = stream.collect(Collectors.toSet()); // set = [a, b, c]
	Stack stack = stream.collect(Collectors.toCollection(Stack::new)); // stack = [a, b, c]

常见应用示例：  
将一个对象的集合转化成另一个对象的集合  

    List<OrderDetail> orderDetailList = orderDetailService.listOrderDetails();
    List<CartDTO> cartDTOList = orderDetailList.stream()
                .map(e -> new CartDTO(e.getProductId(), e.getProductQuantity()))
                .collect(Collectors.toList());
                
交集 (list1 + list2)  

    List<T> intersect = list1.stream().filter(list2::contains).collect(Collectors.toList());
                         
差集  

    // (list1 - list2)
    List<String> reduce1 = list1.stream().filter(item -> !list2.contains(item)).collect(toList());
    
    // (list2 - list1)
    List<String> reduce2 = list2.stream().filter(item -> !list1.contains(item)).collect(toList());

并集  

    //使用并行流 
    List<String> listAll1 = list1.parallelStream().collect(toList());
    List<String> listAll2 = list2.parallelStream().collect(toList());
    listAll1.addAll(listAll2);

去重并集  

    List<String> listAllDistinct = listAll.stream().distinct().collect(toList());

从 List 中过滤出一个元素  

    User match = users.stream().filter((user) -> user.getId() == 1).findAny().get();

Map 集合转 List  

    List<Person> list = map.entrySet().stream().sorted(Comparator.comparing(e -> e.getKey()))
		.map(e -> new Person(e.getKey(), e.getValue())).collect(Collectors.toList());
		
    List<Person> list = map.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue))
          .map(e -> new Person(e.getKey(), e.getValue())).collect(Collectors.toList());
    
    List<Person> list = map.entrySet().stream().sorted(Map.Entry.comparingByKey())
          .map(e -> new Person(e.getKey(), e.getValue())).collect(Collectors.toList());

Collectors toList  

    streamArr.collect(Collectors.toList());
    List<Integer> collectList = Stream.of(1, 2, 3, 4).collect(Collectors.toList());
    System.out.println("collectList: " + collectList);
    // 打印结果 collectList: [1, 2, 3, 4]

Collectors toMap  

    map value 为对象 Student
    Map<Integer, Student> map = list.stream().collect(Collectors.toMap(Student::getId, student -> student));
    // 遍历打印结果
    map.forEach((key, value) -> {
        System.out.println("key: " + key + "  value: " + value);
    });
    map value 为对象中的属性
    Map<Integer, String> map = list.stream().collect(Collectors.toMap(Student::getId, Student::getName));
    map.forEach((key, value) -> {
        System.out.println("key: " + key + "  value: " + value);
    });

List 集合转 Map  

    /*使用Collectors.toMap形式*/
    Map result = peopleList.stream().collect(Collectors.toMap(p -> p.name, p -> p.age, (k1, k2) -> k1));
    // 其中Collectors.toMap方法的第三个参数为键值重复处理策略，如果不传入第三个参数，当有相同的键时，会抛出一个IlleageStateException。
    // 或者
    Map<Integer, String> result1 = list.stream().collect(Collectors.toMap(Hosting::getId, Hosting::getName));
    // List<People> -> Map<String, Object>
    List<People> peopleList = new ArrayList<>();
    peopleList.add(new People("test1", "111"));
    peopleList.add(new People("test2", "222"));
    Map result = peopleList.stream().collect(HashMap::new, (map, p)->map.put(p.name,p.age), Map::putAll);

List 转 Map<Integer, Apple>  

    /**
     * List<Apple> -> Map<Integer, Apple>
     * 需要注意的是：
     * toMap 如果集合对象有重复的key，会报错Duplicate key ....
     *  apple1,apple12的id都为1。
     *  可以用 (k1, k2) -> k1 来设置，如果有重复的key,则保留key1,舍弃key2
     */
    Map<Integer, Apple> appleMap = appleList.stream().collect(Collectors.toMap(Apple::getId, a -> a, (k1, k2) -> k1));

List 转 List<Map<String, Object>>  

    List<Map<String, Object>> personToMap = peopleList.stream().map((p) -> {
        Map<String, Object> map = new HashMap<>();
        map.put("name", p.name);
        map.put("age", p.age);
        return map;
    }).collect(Collectors.toList());
    // 或者
    List<Map<String,Object>> personToMap = peopleList.stream().collect(ArrayList::new, (list, p) -> {
        Map<String, Object> map = new HashMap<>();
        map.put("name", p.name);
        map.put("age", p.age);
        list.add(map);
    }, List::addAll);
	
<a name="7.8"></a>	
	
**Stream 综合应用示例**  
下面示例展示了 `Stream` 多个方法的综合应用，尤其是 Collectors 的使用。    
	
    package com.gorge4j;
    
    import java.util.AbstractMap.SimpleEntry;
    import java.util.Arrays;
    import java.util.Map;
    import java.util.stream.Collectors;
    import java.util.stream.Stream;
    
    public class StreamDemo1 {
    
        public static void main(String[] args) {
            String[] words = new String[] {
                "Target Target either the JVM or JavaScript. Write code in Kotlin and decide where you want to deploy to",
                "Use any existing library on the JVM, as there’s 100% compatibility, including SAM support",
                "Coursera Android app is partially written in Kotlin"};
    
            Map<String, Long> wordCount =
                Stream.of(words) // 字符串数组转化为流
                        .parallel() // 并行处理
                        .flatMap(line -> Arrays.stream(line.trim().split("\\s+"))) // 对字符串按空格、回车及换行等字符分割，分割后就是一个个单词
                        .map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase().trim()) // 将所有不是字母的内容替换为空，全部转为小写，并清除空格
                        .filter(word -> word.length() > 0) // 过滤掉空字符串
                        .map(word -> new SimpleEntry<>(word, 1)) // 将所有字符串作为key，value为1的值存储map中
                        .collect(Collectors.groupingBy(SimpleEntry::getKey, Collectors.counting())); // 分组汇总
            wordCount.forEach((k, v) -> System.out.println(String.format("%s ==>> %d", k, v)));  // 格式化输出
        }
    
    }
    

    // 运行结果：
    coursera ==>> 1
    code ==>> 1
    use ==>> 1
    android ==>> 1
    decide ==>> 1
    deploy ==>> 1
    either ==>> 1
    library ==>> 1
    theres ==>> 1
    and ==>> 1
    where ==>> 1
    written ==>> 1
    write ==>> 1
    sam ==>> 1
    you ==>> 1
    on ==>> 1
    app ==>> 1
    jvm ==>> 2
    including ==>> 1
    or ==>> 1
    in ==>> 2
    want ==>> 1
    kotlin ==>> 2
    is ==>> 1
    any ==>> 1
    javascript ==>> 1
    target ==>> 2
    the ==>> 2
    existing ==>> 1
    as ==>> 1
    to ==>> 2
    compatibility ==>> 1
    support ==>> 1
    partially ==>> 1
	
调试结果如下图所示：  
![](../../image/java/kfszjy/lambda/stream_trace1.png)  
![](../../image/java/kfszjy/lambda/stream_trace2.png)  
	
<a name="7.9"></a>
	
**未使用 `Stream` 以及 使用了 `Stream` 对比示例**  
下面示例展示了未使用 `Stream` 以及使用了 `Stream` 之后的效果：  

	package com.gorge4j;
	
	import java.util.ArrayList;
	import java.util.Arrays;
	import java.util.IntSummaryStatistics;
	import java.util.List;
	import java.util.Random;
	import java.util.stream.Collectors;
	
	public class StreamDemo {
	
	    public static void main(String[] args) {
	        System.out.println("使用 Java 7（No Stream）: ");
	
	        // 计算空字符串
	        List<String> strings = Arrays.asList("abc", "", "bcd", "efg", "hi", "", "jkl");
	        System.out.println("列表: " + strings);
	        long count = getCountEmptyStringUsingJava7(strings);
	
	        System.out.println("空字符的数量为: " + count);
	        count = getCountLength3UsingJava7(strings);
	
	        System.out.println("字符串长度为 3 的数量为: " + count);
	
	        // 删除空字符串
	        List<String> filtered = deleteEmptyStringsUsingJava7(strings);
	        System.out.println("筛选后的列表: " + filtered);
	
	        // 删除空字符串，并使用逗号把它们合并起来
	        String mergedString = getMergedStringUsingJava7(strings, ", ");
	        System.out.println("合并字符串: " + mergedString);
	        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
	
	        // 获取列表元素平方数
	        List<Integer> squaresList = getSquares(numbers);
	        System.out.println("平方数列表: " + squaresList);
	        List<Integer> integers = Arrays.asList(1, 2, 13, 4, 15, 6, 17, 8, 19);
	
	        System.out.println("列表: " + integers);
	        System.out.println("列表中最大的数 : " + getMax(integers));
	        System.out.println("列表中最小的数 : " + getMin(integers));
	        System.out.println("所有数之和 : " + getSum(integers));
	        System.out.println("平均数 : " + getAverage(integers));
	        System.out.println("随机数: ");
	
	        // 输出10个随机数
	        Random random = new Random();
	
	        for (int i = 0; i < 10; i++) {
	            System.out.println(random.nextInt());
	        }
	
	        System.out.println("==================== 分割线 ===================");
	
	        System.out.println("使用 Java 8 Stream: ");
	
	        System.out.println("列表: " + strings);
	        count = strings.stream().filter(String::isEmpty).count();
	        System.out.println("空字符串数量为: " + count);
	
	        count = strings.stream().filter(string -> string.length() == 3).count();
	        System.out.println("字符串长度为 3 的数量为: " + count);
	
	        filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
	        System.out.println("筛选后的列表: " + filtered);
	
	        mergedString = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.joining(", "));
	        System.out.println("合并字符串: " + mergedString);
	
	        squaresList = numbers.stream().map(i -> i * i).distinct().collect(Collectors.toList());
	        System.out.println("Squares List: " + squaresList);
	        System.out.println("列表: " + integers);
	
	        IntSummaryStatistics stats = integers.stream().mapToInt((x) -> x).summaryStatistics();
	
	        System.out.println("列表中最大的数 : " + stats.getMax());
	        System.out.println("列表中最小的数 : " + stats.getMin());
	        System.out.println("所有数之和 : " + stats.getSum());
	        System.out.println("平均数 : " + stats.getAverage());
	        System.out.println("随机数: ");
	
	        random.ints().limit(10).sorted().forEach(System.out::println);
	
	        // 并行处理
	        count = strings.parallelStream().filter(String::isEmpty).count();
	        System.out.println("空字符串的数量为: " + count);
	    }
	
	    private static int getCountEmptyStringUsingJava7(List<String> strings) {
	        int count = 0;
	        for (String string : strings) {
	            if (string.isEmpty()) {
	                count++;
	            }
	        }
	        return count;
	    }
	
	    private static int getCountLength3UsingJava7(List<String> strings) {
	        int count = 0;
	        for (String string : strings) {
	            if (string.length() == 3) {
	                count++;
	            }
	        }
	        return count;
	    }
	
	    private static List<String> deleteEmptyStringsUsingJava7(List<String> strings) {
	        List<String> filteredList = new ArrayList<>();
	        for (String string : strings) {
	            if (!string.isEmpty()) {
	                filteredList.add(string);
	            }
	        }
	        return filteredList;
	    }
	
	    private static String getMergedStringUsingJava7(List<String> strings, String separator) {
	        StringBuilder stringBuilder = new StringBuilder();
	        for (String string : strings) {
	            if (!string.isEmpty()) {
	                stringBuilder.append(string);
	                stringBuilder.append(separator);
	            }
	        }
	        String mergedString = stringBuilder.toString();
	        return mergedString.substring(0, mergedString.length() - 2);
	    }
	
	    private static List<Integer> getSquares(List<Integer> numbers) {
	        List<Integer> squaresList = new ArrayList<>();
	        for (Integer number : numbers) {
	            Integer square = number.intValue() * number.intValue();
	            if (!squaresList.contains(square)) {
	                squaresList.add(square);
	            }
	        }
	        return squaresList;
	    }
	
	    private static int getMax(List<Integer> numbers) {
	        int max = numbers.get(0);
	        for (int i = 1; i < numbers.size(); i++) {
	            Integer number = numbers.get(i);
	            if (number.intValue() > max) {
	                max = number.intValue();
	            }
	        }
	        return max;
	    }
	
	    private static int getMin(List<Integer> numbers) {
	        int min = numbers.get(0);
	        for (int i = 1; i < numbers.size(); i++) {
	            Integer number = numbers.get(i);
	            if (number.intValue() < min) {
	                min = number.intValue();
	            }
	        }
	        return min;
	    }
	
	    private static int getSum(List<Integer> numbers) {
	        int sum = (int) (numbers.get(0));
	        for (int i = 1; i < numbers.size(); i++) {
	            sum += (int) numbers.get(i);
	        }
	        return sum;
	    }
	
	    private static int getAverage(List<Integer> numbers) {
	        return getSum(numbers) / numbers.size();
	    }
	
	}

	// 运行结果：
	使用 Java 7（No Stream）: 
	列表: [abc, , bcd, efg, hi, , jkl]
	空字符的数量为: 2
	字符串长度为 3 的数量为: 4
	筛选后的列表: [abc, bcd, efg, hi, jkl]
	合并字符串: abc, bcd, efg, hi, jkl
	平方数列表: [9, 4, 49, 25]
	列表: [1, 2, 13, 4, 15, 6, 17, 8, 19]
	列表中最大的数 : 19
	列表中最小的数 : 1
	所有数之和 : 85
	平均数 : 9
	随机数: 
	-967800321
	989433471
	1811431767
	1900938594
	-786271536
	48221550
	-882352801
	1549968219
	-678033902
	-1044895798
	==================== 分割线 ===================
	使用 Java 8 Stream: 
	列表: [abc, , bcd, efg, hi, , jkl]
	空字符串数量为: 2
	字符串长度为 3 的数量为: 4
	筛选后的列表: [abc, bcd, efg, hi, jkl]
	合并字符串: abc, bcd, efg, hi, jkl
	Squares List: [9, 4, 49, 25]
	列表: [1, 2, 13, 4, 15, 6, 17, 8, 19]
	列表中最大的数 : 19
	列表中最小的数 : 1
	所有数之和 : 85
	平均数 : 9.444444444444445
	随机数: 
	-1999240435
	-1457045696
	-1260426671
	-1016954209
	-865938647
	-437454022
	-119218528
	-35032400
	111372213
	1688995345
	空字符串的数量为: 2
	
<a name="7.9"></a>
	
**`Stream` 的调试**  
目前就 IDE 来说，Eclipse 对 `Stream` 的调试支持的不是太好，虽然将 `Stream` 的语句拆分成多行来写，支持单个操作打断点，但是查看结果不太方便。这方面 IEDA 有些优势，拿上面的一个例子通过 IDEA 来讲解调试的过程：  

	package com.gorge4j;
	
	import java.util.Arrays;
	import java.util.List;
	import java.util.Objects;
	
	public class StreamDemo {
	
	    public static void  main(String[] args) {
	        
	        List<Integer> numberList =  Arrays.asList(1, 1, null, 2, 3, 5, null, 8, 13, 21, 24, 45);
	        System.out.println("sum is：" + numberList
	            .stream() // 获取 List 的 Stream 对象
	            .filter(Objects::nonNull) // 过滤掉 null 值
	            .distinct() // 去掉重复的值
	            .mapToInt(num -> num * 2) // 每个元素乘以 2
	            .peek(System.out::println) // 每个元素被消费时打印自身
	            .skip(2) // 跳过前两个元素
	            .limit(4) // 最后取前 4 个元素
	            .sum()); // 求和
	        
	    }
	
	}
	
需要安装 Java Stream Debugger 插件，具体如何安装插件请自行参考相关文档，重点是使用 Stream Trace 功能，如下图所示：  
![](../../image/java/kfszjy/lambda/stream_trace.png)  

<a name="8"></a>

## `Java 8` 新特性：`Optional` 类  

<a name="8.1"></a>

**`Optional` 类的介绍** 
从 Java 8 引入的一个很有趣的特性是 Optional  类。Optional 类主要是为了解决臭名昭著的空指针异常（NullPointerException）问题。本质上，Optional  类是一个包含可选值的包装类，这意味着 Optional 类既可以持有对象，也可以为空（null）。Optional 类配合 Lambda 表达式、Stream 可以写出更简洁优雅的代码，副作用是可能会略微降低代码的可读性，综合来看这个特性只是个语法糖的设计。  

> `Optional` 类是一个可以为 `null` 的容器对象。如果值存在则 `isPresent()` 方法会返回 `true`，调用 `get()` 方法会返回该对象；  
> `Optional` 可以保存类型 `T` （类定义中的泛型参数）的值，或者仅仅保存 `null`。`Optional` 提供很多有用的方法，这样我们就不用显式进行空值检测；  
> `Optional` 类的引入很好的解决空指针异常。  

<a name="8.2"></a>

**`Optional` 类声明**  
以下是一个 java.util.Optional<T> 类的声明：  
public final class Optional<T> {……}

<a name="8.3"></a>

**`Optional` 类方法**  

<table border = "0" cellspacing="3" cellpadding="3" style="border:1px solid #000;border-width:1px 0 0 1px;text-align:left;border-collapse:collapse;">
        <tr>
            <td><b>修饰符和类型</b></td>
            <td><b>方法及描述</b></td>
        </tr>
        <tr>
            <td>static &#60;T&#62; Optional&#60;T&#62;</td>
            <td><b>empty()</b> <br /> 返回一个空的 Optional 实例</td>
        </tr>
        <tr>
            <td>boolean</td>
            <td><b>equals(Object obj)</b> <br /> 指示某个其他对象是否等于此可选项</td>
        </tr>
        <tr>
            <td>Optional&#60;T&#62;</td>
            <td><b>filter(Predicate&#60;? super &#60;T&#62; predicate)</b> <br /> 如果一个值存在，并且该值给定的谓词相匹配时，返回一个 Optional 描述的值，否则返回一个空的 Optional </td>
        </tr>
        <tr>
            <td>&#60;U&#62; Optional&#60;U&#62;</td>
            <td><b>flatMap(Function&#60;? super T,Optional&#60;U&#62;&#62; mapper)</b> <br /> 如果值存在，返回基于 Optional 包含的映射方法的值，否则返回一个空的 Optional </td>
        </tr>
        <tr>
            <td>T</td>
            <td><b>get()</b> <br /> 如果在这个 Optional 中包含这个值，返回该值，否则抛出异常：NoSuchElementException </td>
        </tr>
        <tr>
            <td>int</td>
            <td><b>hashCode()</b> <br /> 返回当前值的哈希码值（如果有的话），如果没有值，则返回 0（零） </td>
        </tr>
        <tr>
            <td>void</td>
            <td><b>ifPresent(Consumer&#60;? super T&#62; consumer)</b> <br /> 如果存在值，则使用该值调用指定的消费者（consumer），否则不执行任何操作 </td>
        </tr>
        <tr>
            <td>boolean</td>
            <td><b>isPresent()</b> <br /> 如果值存在则方法会返回 true，否则返回 false </td>
        </tr>
        <tr>
            <td>&#60;U&#62;Optional&#60;U&#62;</td>
            <td><b>map(Function&#60;? super T,? extends U&#62; mapper)</b> <br /> 如果有值，则对其执行调用映射函数得到返回值。如果返回值不为 null，则创建包含映射返回值的 Optional 作为 map 方法返回值，否则返回空 Optional </td>
        </tr>
        <tr>
            <td>static &#60;T&#62; Optional&#60;T&#62;</td>
            <td><b>of(T value)</b> <br /> 返回一个指定非 null 值的 Optional </td>
        </tr>
        <tr>
            <td>static &#60;T&#62; Optional&#60;T&#62;</td>
            <td><b>ofNullable(T value)</b> <br /> 如果为非空，返回 Optional 描述的指定值，否则返回空的 Optional  </td>
        </tr>
        <tr>
            <td>T</td>
            <td><b>orElse(T other)</b> <br /> 如果存在该值，则返回该值， 否则返回 other </td>
        </tr>
        <tr>
            <td>T</td>
            <td><b>orElseGet(Supplier&#60;? extends T&#62; other)</b> <br /> 如果存在该值，返回该值，否则触发 other，并返回 other 调用的结果 </td>
        </tr>
        <tr>
            <td>&#60;X extends Throwable&#62; T</td>
            <td><b>orElseThrow(Supplier&#60;? extends X&#62; exceptionSupplier)</b> <br /> 如果存在该值，返回包含的值，否则抛出由 Supplier 继承的异常 </td>
        </tr>
        <tr>
            <td>String</td>
            <td><b>toString()</b> <br /> 返回一个 Optional 的非空字符串，用来调试 </td>
        </tr>
</table>

<a name="8.4"></a>

**`Optional` 类实际应用**  
先来看一个例子：  

	public static String getName(User user) {
	    if (user == null) {
	        return "Unknown";
	    }
	    return user.getName(); // 上面判断 user 是否为空，避免运行时在此处出现空指针异常（NullPointerException）
	}
	
通过使用 Optional 类来改写一下：  

	public static String getName(User user) {
	    return Optional.ofNullable(user) // 当 user 存在时返回 user，不存在时返回空的 Optional（即 Optional.empty）
	        .(u -> u.getName()).orElse("Unknown"); // 如果存在该值，则返回该值，否则返回 “Unknown”
	}

再看一个综合的例子：  

	package com.gorge4j;
	
	import java.util.Optional;
	
	public class OptionalDemo {
	
	    public static void main(String[] args) throws Exception {
	
	        Address address = new Address("Guangdong", "Shenzhen", "Futian");
	
	        Address addressNull = new Address("Guangdong", null, "Futian");
	
	        User user = new User("John", true, 33, address);
	        User userNull = new User("Jack", null, 18, addressNull);
	
	        Optional<User> optionalUser = Optional.ofNullable(user).filter(u -> u.getName().length() < 6);
	        System.out.println(optionalUser.toString());
	
	        System.out.println(getArea(user));
	        System.out.println(getArea(userNull));
	
	        System.out.println(getAreaOptional(user));
	        System.out.println(getAreaOptional(userNull));
	        
	        System.out.println(getUser(user));
	        System.out.println(getUser(userNull));
	
	        System.out.println(getUserOptional(user));
	        System.out.println(getUserOptional(userNull));
	
	    }
	
	    // 获取用户信息，Java 7.0 及以下版本的写法
	    public static User getUser(User user) throws Exception {
	        if (user != null) {
	            String name = user.getName();
	            if ("Jack".equals(name)) {
	                return user;
	            } else {
	                user.setName("Jack");
	            }
	        } else {
	            user = new User();
	            user.setName("Jack");
	        }
	        return user;
	    }
	    
	    // 获取用户信息，Java 8.0 里使用 Optional 类的写法
	    public static User getUserOptional(User user) throws Exception {
	        return  Optional.ofNullable(user).filter(u -> "Jack".equals(u.getName())).orElseGet(() -> {
	            User userTemp = new User();
	            userTemp.setName("Jack");
	            return userTemp;
	        });
	    }
	
	    // 获取地区信息，Java 7.0 及以下版本的写法
	    public static String getArea(User user) throws Exception {
	        if (user != null && user.getAddress() != null) {
	            Address address = user.getAddress();
	            if (address.getArea() != null) {
	                return address.getArea();
	            }
	        }
	        throw new Exception("取值错误");
	    }
	
	    // 获取地区信息，Java 8.0 里使用 Optional 类的写法
	    public static String getAreaOptional(User user) throws Exception {
	        // .map(User::getAddress).map(Address::getArea) 是 .map(u -> u.getAddress()).map(a -> a.getArea()) 的简洁写法
	        return Optional.ofNullable(user)
	                   .map(User::getAddress)
	                   .map(Address::getArea)
	                   .orElseThrow(() -> new Exception("取值错误"));
	    }
	
	}
	
	// 运行结果：
	Optional[User [name=John, sex=true, age=33]]
	Futian
	Futian
	Futian
	Futian
	User [name=Jack, sex=true, age=33]
	User [name=Jack, sex=null, age=18]
	User [name=Jack, sex=true, age=33]
	User [name=Jack, sex=null, age=18]

// 依赖的 User 对象

	package com.gorge4j;
	
	public class User {
	
	    public User() {}
	
	    public User(String name, Boolean sex, Integer age, Address address) {
	        this.name = name;
	        this.sex = sex;
	        this.age = age;
	        this.address = address;
	    }
	
	    private String name;
	    private Boolean sex;
	    private Integer age;
	    
	    private Address address;
	
	    public String getName() {
	        return name;
	    }
	
	    public void setName(String name) {
	        this.name = name;
	    }
	
	    public Boolean getSex() {
	        return sex;
	    }
	
	    public void setSex(Boolean sex) {
	        this.sex = sex;
	    }
	
	    public Integer getAge() {
	        return age;
	    }
	
	    public void setAge(Integer age) {
	        this.age = age;
	    }
	
	    public Address getAddress() {
	        return address;
	    }
	
	    public void setAddress(Address address) {
	        this.address = address;
	    }
	
	    @Override
	    public String toString() {
	        return "User [name=" + name + ", sex=" + sex + ", age=" + age + "]";
	    }
	
	}

// 依赖的 Address 对象

	package com.gorge4j;
	
	public class Address {
	    
	    public Address() {}
	
	    public Address(String province, String city, String area) {
	        this.province = province;
	        this.city = city;
	        this.area = area;
	    }
	
	    private String province;
	    private String city;
	    private String area;
	
	    public String getProvince() {
	        return province;
	    }
	
	    public void setProvince(String province) {
	        this.province = province;
	    }
	
	    public String getCity() {
	        return city;
	    }
	
	    public void setCity(String city) {
	        this.city = city;
	    }
	
	    public String getArea() {
	        return area;
	    }
	
	    public void setArea(String area) {
	        this.area = area;
	    }
	
	    @Override
	    public String toString() {
	        return "Address [province=" + province + ", city=" + city + ", area=" + area + "]";
	    }
	
	}

<a name="8"></a>

## 部分参考网站  

[Oracle 官方的介绍 - 第一部分](https://www.oracle.com/technical-resources/articles/java/architect-lambdas-part1.html)  
[Oracle 官方的介绍 - 第二部分](https://www.oracle.com/technical-resources/articles/java/architect-lambdas-part2.html)  

其它介绍：  

[Java 8 Stream](https://www.runoob.com/java/java8-streams.html)  
[IBM Developer/Java technology：java.util.stream 库简介](https://www.ibm.com/developerworks/cn/java/j-java-streams-1-brian-goetz/index.html)  
[Java8初体验（一）lambda表达式语法](http://ifeve.com/lambda/)  
[Java8初体验（二）Stream语法详解](http://ifeve.com/stream/)