
## HashMap 的原理

### 一、HashMap 是无序的

- 遍历输出的顺序与 put 存进去的顺序无关
- 肯定不是随机输出的，那是按照什么规律输出的呢？

1. 初始化一个 0 - 15 的数组，长度 16  
2. 将 key 转化成 HashCode, 然后再进行 16 取模，进行取址（实际代码中采用的位运算）

```java
public class HashMapTest2 {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(120, "a");  // 120 % 16 = 8
        map.put(37, "a");   // 37 % 16 = 5
        map.put(61, "a");   // 61 % 16 = 13
        map.put(40, "a");   // 40 % 16 = 8
        map.put(92, "a");   // 92 % 16 = 12
        map.put(78, "a");   // 78 % 16 = 14
        // 打印顺序 {37=a, 120=a, 40=a, 92=a, 61=a, 78=a}
        System.out.println(map);
    }
}
```

### 二、关键的两个方法

#### 1、 final int hash(Object k)

用 hashCode() 方法将 key 转化成 hash 码后并进行优化，得到优化后的 hash 码

例如：将 "yuwen" 这个字符串优化后的 hash 码是 115347492

#### 2、static int indexFor(int h, int length)

对优化后的 hash 码，进行取址，确定在 HashMap 的位置

例如：115347492 在长度是 16 的 HashMap 中，取址坐标是 4

> (一)理解 HashMap 不带参数的构造方法

- Map map = new HashMap()
- 默认长度是 16
- 默认负载因子是 0.75（当长度占用超过 0.75 就默认需要进行扩容，扩容长度默认是 2 的倍数）
- 等同于：Map map = new HashMap(16, 0.75f)

> (二)理解 HashMap 带参数的构造方法

- Map map = new HashMap(int initialCapacity)：例如 new HashMap（3）
- HashMap(int initialCapacity, float loadFactor)

例如：传入的initialCapacity为n，则创建的长度是大于n的2的n次方
- 传入3，实际上长度是大于3的2的n次方，所以长度是4
- 传入5，实际上长度是大于5的2的n次方，所以长度是8

> Map map = new HashMap(10000, 0.75f)，要录入的数据有10000条，会产生扩容吗？  
>解：2的14次方 = 16384 * 0.75 = 12288

注意：需要尽量减少扩容的次数，提高运行效率






### 三、