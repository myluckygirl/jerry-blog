
## 一、Map 通用方法

### 1. Map 接口及实现类

- Map
    - HashMap - LinkedHashMap
    - SortedMap - TreeMap

### 2. Map 接口通用方法

- V put(K key, V value)：存入 Map 中一个 key/value 映射
- V get(Object key)：返回到指定键所映射的值
- int size()：返回此 Map 中键值映射的数量
- V remove(Object key)：从该 Map 中删除一个键的映射
- boolean containsKey(Object key)：是否包含指定键的 key

## 二、HashMap 的使用

### 1. HashMap 的构造方法

- HashMap()
- HashMap(int initialCaoacity) 指明初始化大小
- HashMap(int initialCapacity, float loadFactor) 指明初始化大小，负载因子（指明什么情况下会进行扩容）

### 2. HashMap 的基本用法

`Map<String, Object> userMap = new HashMap<>()`

userMap.put("zhangsan", new Integer(120))

userMap.get("zhangsan")

### 3. HashMap 的 Entry 结构

```java
static class Entry<K, V> implements Map.Entry<K, V> {
    final K key;
    V value;
    Entry<K, V> next;
    final int hash;
}
```
### 4. HashMap 的输出顺序和 key 的取址

```java
public class MapTest {
    public static void main(String[] args){
      Map<String, Integer> userMap = new HashMap<>();
      userMap.put("zhang1", new Integer(1));
      userMap.put("zhang2", new Integer(10));
      userMap.put("zhang3", new Integer(8));
      userMap.put("zhang4", new Integer(2));
      userMap.put("zhang5", new Integer(6));
      System.out.println(userMap.get("zhang1"));
      // {zhang2=10, zhang3=8, zhang1=1, zhang4=2, zhang5=6}
      System.out.println(userMap);
    }
}
```

## 三、HashMap 遍历

### 1. KeySet

```
// 利用 key 进行遍历，只能获取 key， 通过 map.get(key)) 获取 value
    private static void showMap1(Map<String, Integer>  map) {
        for(String key : map.keySet()) {
            System.out.println(key + "****" + map.get(key));
        }
    }
```

### 2. values

```
// 利用 value 进行遍历，只获取 value
    private static void showMap2(Map<String, Integer> map) {
        for(Integer value : map.values()) {
            System.out.println(value);
        }
    }
```

### 3. entrySet

```
// 利用 entrySet 进行遍历，获取 key 和 value
    private static void showMap3(Map<String, Integer> map) {
        for(Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "***" + entry.getValue());
        }
    }
```

### 4. Iterator

```
// 利用 iterator 进行遍历
    private static void showMap4(Map<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            System.out.println(entry.getKey() + "***" + entry.getValue());
        }
    }
```

### 5. HashMap 遍历代码合集

```java
public class MapTest {
    public static void main(String[] args) {
        Map<String, Integer> userMap = inputMap();
        System.out.println(userMap.get("zhang1"));
        // {zhang2=10, zhang3=8, zhang1=1, zhang4=2, zhang5=6}
        System.out.println(userMap);

        showMap1(userMap);
        System.out.println("========================");
        showMap2(userMap);
        System.out.println("========================");
        showMap3(userMap);
        System.out.println("========================");
        showMap4(userMap);

    }

    // 利用 iterator 进行遍历
    private static void showMap4(Map<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            System.out.println(entry.getKey() + "***" + entry.getValue());
        }
    }

    // 利用 entrySet 进行遍历，获取 key 和 value
    private static void showMap3(Map<String, Integer> map) {
        for(Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "***" + entry.getValue());
        }
    }

    // 利用 value 进行遍历，只获取 value
    private static void showMap2(Map<String, Integer> map) {
        for(Integer value : map.values()) {
            System.out.println(value);
        }
    }

    // 利用 key 进行遍历，只能获取 key， 通过 map.get(key)) 获取 value
    private static void showMap1(Map<String, Integer>  map) {
        for(String key : map.keySet()) {
            System.out.println(key + "****" + map.get(key));
        }
    }

    private static Map<String, Integer> inputMap() {
        Map<String, Integer> userMap = new HashMap<>();
        userMap.put("zhang1", new Integer(1));
        userMap.put("zhang2", new Integer(10));
        userMap.put("zhang3", new Integer(8));
        userMap.put("zhang4", new Integer(2));
        userMap.put("zhang5", new Integer(6));
        return userMap;
    }
}
```

## 四、HashMap 遍历性能分析

> HashMap 遍历方法中，那种方法最好？

iterator、entrySet、values、keySet（最慢）

```java
public class HashMapPc {  
    public static void main(String[] args) {
        Map<String, Integer> userMap = inputMap();

        showMap1(userMap);
        System.out.println("========================");
        showMap2(userMap);
        System.out.println("========================");
        showMap3(userMap);
        System.out.println("========================");
        showMap4(userMap);

    }

    private static Map<String, Integer> inputMap() {
        Map<String, Integer> userMap = new HashMap<>();
        // 设置 key 生成随机数
        String[] strings = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
        int m = (int) (Math.random() * 10);
        String key;
        Integer value;
        // 循环遍历，构造比较大的数值，方便对比性能
        for(int i=0; i<1000000; i++) {
            key = String.valueOf(strings[m] + i*100);
            value = i;
            userMap.put(key, value);
        }
        return userMap;
    }

    // 利用 key 进行遍历，只能获取 key， 通过 map.get(key)) 获取 value
    private static void showMap1(Map<String, Integer>  map) {
        Long start = System.currentTimeMillis();
        Integer value;
        for(String key : map.keySet()) {
            // System.out.println(key + "****" + map.get(key));
            value = map.get(key);
        }
        Long end = System.currentTimeMillis();
        // keySet 执行时间为：86
        System.out.println("keySet 执行时间为：" + (end-start));
    }

    // 利用 value 进行遍历，只获取 value
    private static void showMap2(Map<String, Integer> map) {
        Long start = System.currentTimeMillis();
        Integer v;
        for(Integer value : map.values()) {
            // System.out.println(value);
            v = value;

        }
        Long end = System.currentTimeMillis();
        // values 执行时间为：60
        System.out.println("values 执行时间为：" + (end-start));
    }

    // 利用 entrySet 进行遍历，获取 key 和 value
    private static void showMap3(Map<String, Integer> map) {
        Long start = System.currentTimeMillis();
        String key;
        Integer value;
        for(Map.Entry<String, Integer> entry : map.entrySet()) {
            // key = entry.getKey();
            value = entry.getValue();
            // System.out.println(entry.getKey() + "***" + entry.getValue());
        }
        Long end = System.currentTimeMillis();
        // entrySet 执行时间为：51
        System.out.println("entrySet 执行时间为：" + (end-start));
    }

    // 利用 iterator 进行遍历
    private static void showMap4(Map<String, Integer> map) {
        Long start = System.currentTimeMillis();
        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
        String key;
        Integer value;
        while(iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            // key = entry.getKey();
            value = entry.getValue();
            // System.out.println(entry.getKey() + "***" + entry.getValue());
        }
        Long end = System.currentTimeMillis();
        // iterator 执行时间为：52
        System.out.println("iterator 执行时间为：" + (end-start));
    }

}
```