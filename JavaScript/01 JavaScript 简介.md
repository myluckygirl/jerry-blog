
## 一、JavaScript 简介

> 为何学习 JavaScript？
JavaScript 是 web 开发者必学的三种语言之一：

- HTML 定义网页的内容
- CSS 规定网页的布局
- JavaScript 对网页行为进行编程

完整的 [JavaScript 参考手册](https://www.w3school.com.cn/js/js_examples.asp)

JavaScript 能够改变 HTML 内容

### 1.1 getElementById()

`getElementById()` 是多个 Java Script HTML 方法之一。

本例使用该方法来“查找” id="demo" 的 HTML 元素，并把元素内容（innerHTML）更改为 "Hello JavaScript"：

```javascript
document.getElementById("demo").innerHTML = "Hello JavaScript";
```
提示：JavaScript 同时接受双引号和单引号：

实例
```javascript
document.getElementById("demo").innerHTML = 'Hello JavaScript';
```
亲自试一试
JavaScript 能够改变 HTML 属性
本例通过改变 <img> 标签的 src 属性（source）来改变一张 HTML 图像：
```html
<!DOCTYPE html>
<html>
<body>
<h2>JavaScript 能做什么？</h2>
<p>JavaScript 能够改变 HTML 属性值。</p>
<p>在本例中，JavaScript 改变了图像的 src 属性值。</p>

<button onclick="document.getElementById('myImage').src='/i/eg_bulbon.gif'">开灯</button>
<img id="myImage" border="0" src="/i/eg_bulboff.gif" style="text-align:center;">
<button onclick="document.getElementById('myImage').src='/i/eg_bulboff.gif'">关灯</button>

</body>
</html>
```

JavaScript 能够改变 HTML 样式 (CSS)
改变 HTML 元素的样式，是改变 HTML 属性的一种变种：

```javascript
document.getElementById("demo").style.fontSize = "25px";
```
JavaScript 能够隐藏 HTML 元素
可通过改变 display 样式来隐藏 HTML 元素：


```javascript
document.getElementById("demo").style.display="none";
```
JavaScript 能够显示 HTML 元素
可通过改变 display 样式来显示隐藏的 HTML 元素：

```javascript
document.getElementById("demo").style.display="block";
```

### JavaScript 使用

1\. **`<script>`标签**

在 HTML 中，JavaScript 代码必须位于 `<script>` 与 `</script>` 标签之间。

```
<script>
document.getElementById("demo").innerHTML="我的第一段 JavaScript";
</script>
```

>注释：旧的 JavaScript 例子也许会使用 type 属性：`<script type="text/javascript">`。   
注释：type 属性不是必需的。JavaScript 是 HTML 中的默认脚本语言。   

2\. JavaScript 函数和事件

JavaScript 函数是一种 JavaScript 代码块，它可以在调用时被执行。

例如，当发生事件时调用函数，比如当用户点击按钮时。

3\. `<head>` 或 `<body>` 中的 JavaScript

您能够在 HTML 文档中放置任意数量的脚本。脚本可被放置与 HTML 页面的 `<body>` 或 `<head>` 部分中，或兼而有之。

- `<head>` 中的 JavaScript

在本例中，JavaScript 函数被放置于 HTML 页面的 `<head>` 部分。

该函数会在按钮被点击时调用：

```html
<!DOCTYPE html>
<html>
<head>
<script>
function myFunction() {
    document.getElementById("demo").innerHTML = "段落被更改。";
}
</script>
</head>

<body>

<h1>一张网页</h1>
<p id="demo">一个段落</p>
<button type="button" onclick="myFunction()">试一试</button>

</body>
</html>
```

- `<body>` 中的 JavaScript

在本例中，JavaScript 函数被放置于 HTML 页面的 `<body>` 部分。

该函数会在按钮被点击时调用：

```html
<!DOCTYPE html>
<html>
<body> 
<h1>A Web Page</h1>
<p id="demo">一个段落</p>
<button type="button" onclick="myFunction()">试一试</button>
<script>
function myFunction() {
   document.getElementById("demo").innerHTML = "段落被更改。";
}
</script>
</body>
</html>
```
> 提示：把脚本置于 `<body>` 元素的底部，可改善显示速度，因为脚本编译会拖慢显示。

4\. 外部脚本

脚本可放置与外部文件中：
   
外部文件：myScript.js

```javascript
function myFunction() {
  document.getElementById("demo").innerHTML = "段落被更改。";
}
```

外部脚本很实用，如果相同的脚本被用于许多不同的网页。

JavaScript 文件的文件扩展名是 .js。如需使用外部脚本，请在 `<script>` 标签的 src (source) 属性中设置脚本的名称：

实例
`<script src="myScript.js"></script>`

5\. 您可以在 `<head>` 或 `<body>` 中放置外部脚本引用。

该脚本的表现与它被置于 `<script>` 标签中是一样的。注释：外部脚本不能包含 `<script> `标签。

外部 JavaScript 的优势：
- 分离了 HTML 和代码
- 使 HTML 和 JavaScript 更易于阅读和维护
- 已缓存的 JavaScript 文件可加速页面加载
- 如需向一张页面添加多个脚本文件 - 请使用多个 script 标签：

实例
```
<script src="myScript1.js"></script>
<script src="myScript2.js"></script>
```

6\. 外部引用

可通过完整的 URL 或相对于当前网页的路径引用外部脚本：

- 本例使用完整的 URL 来链接至脚本：`<script src="https://www.w3school.com.cn/js/myScript1.js"></script>`

- 本例使用了位于当前网站上指定文件夹中的脚本：`<script src="/js/myScript1.js"></script>`

- 本例链接了与当前页面相同文件夹的脚本：`<script src="myScript1.js"></script>`
