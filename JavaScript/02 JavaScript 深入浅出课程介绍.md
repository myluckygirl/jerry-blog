
## JavaScript 深入浅出介绍

> 再无特殊说明时，本教程内容涉及的 JS 版本为 ECMAScript5，运行结果以 Chrome 39 为准

- 学习资料：《JavaScript 权威指南》  
- MDN：https://developer.mozilla.org/zh-CN/learn/javascript
- 学习方式：多动手实践 + 参与讨论

### 一、数据类型

弱类型特性

（1） 无需声明数据类型？

```javascript
var number = 32;
num = "this is a string";
```

（2） 运算结果展示？

32 + 32  // 数字相加 64  
"32" + 32 // 加双引号，默认是字符拼接 "3232"  
"32" - 32 // 减号都是默认数字相减 0


#### 1.1 JavaScript 六种数据类型**

原始类型：
- number
- string
- boolean
- null
- undefined
- **object 对象**
    - Function
    - Array
    - Date
    - ...
    
#### 1.2 隐式转换  

（1）巧用 +/- 规则转换类型  
- numb -0   -> 减号，变为数字运算
- num + ''  -> 加空字符，变为字符串拼接

（2） a == b

- 类型相同，同 ===
- 类型不同，尝试类型转换和比较：
    - null == undefined 相等
    - number == String 转 number 1 == "1.0" // true
    - boolean == ? 转 number 1 == true // true
    - object == number | string 尝试对象转为基本类型 new String('hi') == 'hi' // true
    - 其他：false

"1.23" == 1.23   // true,当等号两边有个是数字时，会先尝试把字符串转化成数字，再做比较
0 == false  
null == undefined  
new Object() == new Object()  
[1, 2] == [1, 2]  

（3）严格等于 a === b

- 类型不同，返回 false
- 类型相同：
    - null === null
    - undefined === undefined
    - NaN ≠ NaN
    - new Object ≠ new Object

#### 1.3 包装对象

- 'str' -> String Object
- 124  -> Number Object
- true -> Boolean Object;

当调用原始类型的对象类型时，如: a.length 或者给他赋值 a.t = 3，则相当于 new 了一个对象类型，
当调用完成之后才会自动销毁这个对象，则导致后面 `alert(a.t);` 时返回的 undefined

```javascript
var a = "string";
// 6
alert(a.length);
// 3
a.t = 3;
// undefined
alert(a.t);
```

#### 1.4 类型检测

- typeof
- instanceof
- Object.prototype.toString
- constructor
- duck type

(1)**typeof 使用**：适用于基本类型、函数对象的判断。对于其他对象的数据类型判断比较不友好，比如：数组
- typeof 100 "number"
- typeof true "boolean"
- typeof function "function"
- typeof(undefined) "undefined"
- typeof new Object() "object"
- typeof [1, 2] "object"
- typeof NaN "number"
- typeof null "object"(历史的问题，解决兼容性问题)

(2)**instanceof** ：
`obj instanceof Object`  
左操作必须是一个对象，如果是基本数据类型之类的会直接返回 false，右操作数必须是个函数对象或者函数构造器，否则或报函数类型异常。

- [1, 2] instanceof Array === true  
- new Object() instanceof Array == false  
**不同** window 或 iframe 之间的对象类型检测**不能**使用 instanceof!

(3)**Object.prototype.toString**
- Object.prototype.toString.apply([]); === "[object Array]";
- Object.prototype.toString.apply(function(){}); === "[object Function]";
- Object.prototype.toString.apply(null); === "[object Null]"
- Object.prototype.toString.apply(undefined); === "[object Undefined]";

> IE 6/7/8 Object.prototype.toString.apply(null) 返回 "[object Object]"

(4)constructor

(5)duck type


**类型检测小结**
- typeof：适合基本类型及 function 检测，遇到 null 失效
- [[Class]]：通过 {}.toString 拿到，适合内置对象和基元类型，遇到 null 和 undefined 失效（IE 6/7/8 Object.prototype.toString.apply(null) 返回 "[object Object]"）
- instanceof：适合自定义对象，也可以用来检测原生态对象，在不同 iframe 和 window 间检测时失效。

### 二、表达式和运算符


### 三、语句


### 四、对象


### 五、数组


### 六、函数


### 七、this


### 八、闭包和作用域


### 九、OOP


### 十、正则与模式匹配