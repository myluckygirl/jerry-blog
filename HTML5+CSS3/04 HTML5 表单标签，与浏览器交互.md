
## HTML5 表单标签，与浏览器交互

### 一、使用`<form>`创建表单

> 网站怎样与用户进行交互？  
答案是使用HTML表单(form)。表单是可以把浏览者输入的数据传送到服务器端，这样服务器端程序就可以处理表单传过来的数据。

语法：

`<form   method="传送方式"   action="服务器文件">`

讲解：

1. `<form>`：`<form>` 标签是成对出现的，以 `<form>` 开始，以`</form>`结束。

2. action ：浏览者输入的数据被传送到的地方,比如一个PHP页面(save.php)。

3. method ： 数据传送的方式（get/post）。

```html
<form method="post" action="save.php">
        <label for="username">用户名:</label>
        <input type="text" name="username" />
        <label for="pass">密码:</label>
        <input type="password" name="pass" />
</form>
```

注意:

1、所有表单控件（文本框、文本域、按钮、单选框、复选框等）都必须放在 `<form></form>` 标签之间（否则用户输入的信息可提交不到服务器上哦！）。

2、method : post/get 的区别这一部分内容属于后端程序员考虑的问题。感兴趣的小伙伴可以查看本小节的 wiki，里面有详细介绍。

### 二、文本输入框、密码输入框 type="text/password"

当用户要在表单中键入字母、数字等内容时，就会用到文本输入框。文本框也可以转化为密码输入框。

语法：

```html
<form>
   <input type="text/password" name="名称" value="文本" />
</form>
```
1、type：

- 当 type="text" 时，输入框为文本输入框;

- 当 type="password" 时, 输入框为密码输入框。

2、name：为文本框命名，以备后台程序 ASP 、PHP 使用。

3、value：为文本输入框设置默认值。(一般起到提示作用)

举例：

```html
<form>
  姓名：
  <input type="text" name="myName">
  <br/>
  密码：
  <input type="password" name="pass">
</form>
```

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>文本输入框、密码输入框</title>
</head>
<body>
    <form method="post" action="save.php">
        账户:
        <input type="text" name="account" value="请输入账号" />
        <br />
        密码:
        <input type="password" name="password" />
    </form>
</body>
</html>
```
### 三、提示信息 - placeholder属性的使用

这一项章节我们来学习 input 标签中占位符 placeholder 属性，有时候需要提示用户输入框需要输入框的内容，那么就会用到我们的占位符。

语法：

`<input type="text" name="name" placeholder="请输入用户名" /><br />`

技术点的解释：

1、placeholder 属性为输入框占位符,里面可以放提示的输入信息。

2、placeholder 属性的值可以任意填写,当输入框输入内容时,占位符内容消失,输入框无内容时,占位符内容显示。

3、占位符内容不是输入框真正的内容。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>input-placeholder</title>
</head>
<body>
    <form>
        <label for="name">
            用户名：
        </label>
        <input type="text" name="name" placeholder="请输入用户名" /><br />
        <label for="password">
            密码：
        </label>
        <input type="password" name="password" placeholder="请输入密码" />
    </form>
</body>
</html>
```

### 四、数字输入框 type="number"

语法：

`<input name="number" type="number" />`

技术点的解释：

1、input 的 type 属性设置为 number,则表示该输入框的类型为数字。

2、数字框只能输入数字，输入其他字符无效。

3、数字框最右侧会有一个加减符号,可以调整输入数字的大小,不同浏览器表现不一致。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>input-number</title>
</head>
<body>
    放假天数：
    <input name="number" type="number" />
</body>
</html>
```

### 五、网址输入框 type="url"

语法：
`<input name="url" type="url" />`

技术点的解释：

1、input 的 type 属性设置为 url, 则表示该输入框的类型为网址。

2、数字框的值需以 http:// 或者 https:// 开头,且后面必须有内容,否则表单提交的时候会报错误提示。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>input-url</title>
</head>
<body>
    百度网址：
    <input name="url" type="url" />
</body>
</html>
```

### 六、邮箱输入框 type="email"

语法：
`<input type="email" name="email" placeholder="请输入邮箱"/>`

技术点的解释：

1、Input 的 type 属性设置为 email,则表示该输入框的类型为邮箱。

2、输入框的值必须包含 @。

3、输入框的值 @ 之后必须有内容,否则会报错误提示。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>input-email</title>
</head>
<body>
    邮箱：
    <input type="email" name="email" placeholder="请输入邮箱"/>
</body>
</html>
```

### 七、使用`<textarea>`标签创建文本域

当用户需要在表单中输入大段文字时，需要用到文本输入域。

语法：

`<textarea  rows="行数" cols="列数">文本</textarea>`

1、`<textarea>`标签是成对出现的，以`<textarea>`开始，以`</textarea>`结束。

2、cols ：多行输入域的列数。

3、rows ：多行输入域的行数。

4、在`<textarea></textarea>`标签之间可以输入默认值。

举例：
```html
<form  method="post" action="save.php">
        <label>联系我们</label>
        <textarea cols="50" rows="10" >在这里输入内容...</textarea>
</form>
```
注意：代码中的`<label>`标签在本章 5-9中 讲解。

这两个属性可用 css 样式的 width 和 height 来代替。
col 用 width、row 用 height 来代替。（这两个 css 样式在以后的章节会讲解）

### 八、使用 label 为 input 标签穿上衣服

label 标签不会向用户呈现任何特殊效果，它的作用是为鼠标用户改进了可用性。如果你在 label 标签内点击文本，就会触发此控件。
就是说，当用户单击选中该 label 标签时，浏览器就会自动将焦点转到和标签相关的表单控件上（就自动选中和该 label 标签相关连的表单控件上）。

语法：
`<label for="控件id名称">`

> 注意：
> - 标签的 for 属性中的值应当与相关控件的 id 属性值一定要相同。
> - 当用户单击选中该 label 标签时，浏览器就会自动将焦点转到和标签相关的表单控件上

例子：
```html
<form
  <label for="email">输入你的邮箱地址</label>
  <input type="email" id="email" placeholder="Enter email">
</form>
```

### 九、单选框、复选框，让用户选择 type="radio/checkbox"

在使用表单设计调查表时，为了减少用户的操作，使用选择框是一个好主意。
html 中有两种选择框，即单选框和复选框，两者的区别是单选框中的选项用户只能选择一项，而复选框中用户可以任意选择多项，甚至全选。

语法：

`<input   type="radio/checkbox"   value="值"    name="名称"   checked="checked"/>`

1、type:

- 当 type="radio" 时，控件为单选框
- 当 type="checkbox" 时，控件为复选框

2、value：提交数据到服务器的值（后台程序PHP使用）

3、name：为控件命名，以备后台程序 ASP、PHP 使用

4、checked：当设置 checked="checked" 时，该选项被默认选中

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>单选框、复选框</title>
</head>

<body>
    <form action="save.php" method="post">
        <label>你是否喜欢旅游？</label><br />
        <input type="radio" name="radiolove" value="喜欢" checked="checked" />
        <label>喜欢</label>
        <input type="radio" name="radiolove" value="不喜欢" />
        <label>不喜欢</label>
        
        <br /><br />
        
        <label>你对那些运动感兴趣？</label><br />
        <input type="checkbox" value="跑步" name="checkbox1" />
        <label>跑步</label>
        <input type="checkbox" value="打球" name="checkbox2" checked="checked" />
        <label>打球</label>
        <input type="checkbox" value="登山" name="checkbox3" checked="checked" />
        <label>登山</label>
        <input type="checkbox" value="健身" name="checkbox4" />
        <label>健身</label>
    </form>
</body>

</html>
```

> 注意：同一组的单选按钮，**name 取值一定要一致**，比如上面例子为同一个名称 “radioLove”，这样同一组的单选按钮才可以起到单选的作用。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>单选框、复选框</title>
</head>
<body>
    <form action="save.php" method="post">
        <label>性别:</label>
        <label>男</label>
        <input type="radio" value="1" name="gender" />
        <label>女</label>
        <input type="radio" value="2" name="gender" />
    </form>
</body>
</html>
```
### 十、使用 select、option 标签创建下拉菜单

下拉列表在网页中也常会用到，它可以有效的节省网页空间。既可以单选、又可以多选。

讲解：

1、select 和 option 标签都是双标签，它总是成对出现的，需要首标签和尾标签。

2、select 标签里面只能放 option 标签，表示下拉列表的选项。

3、option 标签放选项内容，不放置其他标签。

4、value：value 里面的值是向服务器提交的值，option 标签中间的值是页面展示显示的值。

5、selected="selected"：设置 selected="selected" 属性，则该选项就被默认选中。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>select下拉框</title>
</head>

<body>
    <form>
        <label>请选择：</label>
        <select>
            <option value="看书">看书</option>
            <option value="旅游">旅游</option>
            <option value="运动">运动</option>
            <option value="购物" selected="selected">购物</option>
        </select>
    </form>
</body>
</html>
```

### 十一、提交按钮 type="submit"

在表单中有两种按钮可以使用，分别为：提交按钮、重置。

这一小节讲解提交按钮：当用户需要提交表单信息到服务器时，需要用到提交按钮。

语法：

`<input   type="submit"   value="提交">`

- type：只有当 type 值设置为 submit 时，按钮才有提交作用
- value：按钮上显示的文字

### 十二、使用重置按钮，重置表单信息 type="reset"

当用户需要重置表单信息到初始时的状态时，比如用户输入“用户名”后，发现书写有误，可以使用重置按钮使输入框恢复到初始状态。  
只需要把 type 设置为 "reset" 就可以。

语法：

`<input type="reset" value="重置">`

- type：只有当 type 值设置为 reset 时，按钮才有重置作用
- value：按钮上显示的文字

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>重置按钮</title>
</head>

<body>
    <form action="save.php" method="post">
        <label>爱好:</label>
        <select>
            <option value="看书">看书</option>
            <option value="旅游" selected="selected">旅游</option>
            <option value="运动">运动</option>
            <option value="购物">购物</option>
        </select>
        <br /><br />
        <input type="submit" value="确定" />
        <input type="submit" value="提交" />
        <input type="reset" value="重置" />
    </form>
</body>

</html>
```
