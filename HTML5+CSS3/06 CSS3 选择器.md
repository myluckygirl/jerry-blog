
# CSS3 选择器

## 一、什么是选择器？

每一条css样式声明（定义）由两部分组成，形式如下：

```
选择器{
    样式;
}
```
在 {} 之前的部分就是“选择器”，“选择器”指明了 {} 中的“样式”的作用对象，也就是“样式”作用于网页中的哪些元素。
比如右侧代码编辑器中第7行代码中的 “body” 就是选择器。

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>选择器</title>
    <style type="text/css">
    body {
        font-size: 12px;
        color: red;
    }
    </style>
</head>

<body>
    <p>慕课网（IMOOC）是学习编程最简单的免费平台。慕课网提供了丰富的移动端开发、php开发、web前端、html5教程以及css3视频教程等课程资源。它富有交互性及趣味性，并且你可以和朋友一起编程。</p>
</body>

</html>
```
## 二、标签选择器

标签选择器其实就是 html 代码中的标签。如右侧代码编辑器中的 `<html>、<body>、<h1>、<p>、<img>`。例如下面代码：

`p{font-size:12px;line-height:1.6em;}`

上面的 css 样式代码的作用：为 p 标签设置 12px 字号，行间距设置 1.6em 的样式。

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>标签选择器</title>
    <style type="text/css">
    h1{
        font-weight:normal;
        color:red;
    }
    </style>
</head>

<body>
    <h1>勇气</h1>
    <p>三年级时，我还是一个胆小如鼠的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p>到了三年级下学期时，我们班上了一节公开课，老师提出了一个很简单的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
    <img src="http://img.mukewang.com/52b4113500018cf102000200.jpg">
</body>
```

## 三、类选择器 .类名

类选择器在 css 样式编码中是最常用到的，如右侧代码编辑器中的代码:可以实现为“胆小如鼠”、“勇气”字体设置为红色。

语法：

`.类选器名称{css样式代码;}`

注意：

1、英文圆点开头  
2、其中类选器名称可以任意起名（但不要起中文噢）  

使用方法：

第一步：使用合适的标签把要修饰的内容标记起来，如下：

`<span>胆小如鼠</span>`

第二步：使用 `class="类选择器名称"` 为标签设置一个类，如下：

`<span class="stress">胆小如鼠</span>`

第三步：设置类选器 css 样式，如下：

`.stress{color:red;}/*类前面要加入一个英文圆点*/`

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>认识html标签</title>
    <style type="text/css">
    .stress {
        color: red;
    }
    .course {
        color:green;
    }
    </style>
</head>

<body>
    <h1>勇气</h1>
    <p>三年级时，我还是一个<span class="stress">胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个<span class="stress">勇气</span>来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p>到了三年级下学期时，我们班上了一节<sapn class="course">公开课</sapn>，老师提出了一个很简单的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
    <img src="http://img.mukewang.com/52b4113500018cf102000200.jpg">
</body>

</html>
```

## 四、ID 选择器 #ID

这一小节我们来学习 ID 选择器了，那么 ID 选择器如何使用呢?

```
<style>
    #box {
        color:red;
    }
</style>
```

技术点的解释：

1、使用 ID 选择器，必须给标签添加上 id 属性，为标签设置 `id="ID名称"`，而不是`class="类名称"`。

2、ID 选择符的前面是井号（#）号，而不是英文圆点（.）。

3、id 属性的值既为当前标签的 id，尽量见名思意，语义化。

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>认识html标签</title>
    <style type="text/css">
    #stress {
        color: red;
    }
    #course {
        color:green;
    }
    
    </style>
</head>

<body>
    <h1>勇气</h1>
    <p>三年级时，我还是一个<span id="stress">胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p>到了三年级下学期时，我们班上了一节<span id="course">公开课</span>，老师提出了一个很简单的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
</body>

</html>
```

## 五、类选择器和 ID 选择器的区别

学习了类选择器和 ID 选择器，我们会发现他们之间有很多的相似处，是不是两者可以通用呢？

我们不要着急先来总结一下他们的相同点和不同点：
- 相同点：可以应用于任何元素
- 不同点：
    1. ID选择器只能在文档中使用一次;
    2. 可以使用`类选择器词列表方法`为一个元素同时设置多个样式。
    
  
1、ID选择器只能在文档中使用一次。与类选择器不同，在一个HTML文档中，ID选择器只能使用一次，而且仅一次。而类选择器可以使用多次。

下面代码是正确的：

```
 <p>三年级时，我还是一个<span class="stress">胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个<span class="stress">勇气</span>来回答老师提出的问题。</p>
```
而下面代码是错误的：
```
 <p>三年级时，我还是一个<span id="stress">胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个<span id="stress">勇气</span>来回答老师提出的问题。</p>
 ```
2、可以使用类选择器词列表方法为一个元素同时设置多个样式。我们可以为一个元素同时设多个样式，但只可以用类选择器的方法实现，ID选择器是不可以的（不能使用 ID 词列表）。

下面的代码是正确的(完整代码见右侧代码编辑器)

```
.stress{
    color:red;
}
.bigsize{
    font-size:25px;
}
<p>到了<span class="stress bigsize">三年级</span>下学期时，我们班上了一节公开课...</p>
上面代码的作用是为“三年级”三个文字设置文本颜色为红色并且字号为25px。
```
下面的代码是不正确的(完整代码见右侧代码编辑器)

```
#stressid{
    color:red;
}
#bigsizeid{
    font-size:25px;
}
<p>到了<span id="stressid bigsizeid">三年级</span>下学期时，我们班上了一节公开课...</p>
上面代码不可以实现为“三年级”三个文字设置文本颜色为红色并且字号为25px的作用。
```

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>类和ID选择器的区别</title>
    <style type="text/css">
    .stress {
        color: red;
    }

    .bigsize {
        font-size: 25px;
    }

    #stressid {
        color: red;
        font-size: 25px;
    }
    </style>
</head>

<body>
    <h1>勇气</h1>
    <p>三年级时，我还是一个<span class="stress">胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个<span class="stress">勇气</span>来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p>到了<span class="stress bigsize">三年级</span>下学期时，我们班上了一节公开课，老师提出了一个很简单的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
    <p>到了<span id="stressid">三年级</span>下学期时，我们班上了一节公开课，老师提出了一个很简单的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
</body>

</html>
```
## 六、子选择器 `.类名>子标签`

还有一个比较有用的选择器**子选择器**，即大于符号(>),用于选择指定标签元素的**第一代子元素**。

`.food>li{border:1px solid red;}`

这行代码会使class名为food下的子元素li（水果、蔬菜）加入红色实线边框。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>子选择器</title>
    <style type="text/css">
    /*添加边框样式（粗细为1px， 颜色为红色的实线）*/

    .food>li {
        border: 1px solid red;
        color: green;
    }
    .first>span {
        border: 1px solid blue;
    }
    </style>
</head>
<body>
    <p class="first">三年级时，<span>我还是一个<span>胆小如鼠</span>的小女孩</span>，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <h1>食物</h1>
    <ul class="food">
        <li>水果
            <ul>
                <li>香蕉</li>
                <li>苹果</li>
                <li>梨</li>
            </ul>
        </li>
        <li>蔬菜
            <ul>
                <li>白菜</li>
                <li>油菜</li>
                <li>卷心菜</li>
            </ul>
        </li>
    </ul>
</body>
</html>
```

## 七、后代选择器 `.类名 子标签`

包含选择器，即加入**空格**,用于选择指定标签元素下的后辈元素。如右侧代码编辑器中的代码：

`.first  span{color:red;}`

这行代码会使第一段文字内容中的“胆小如鼠”字体颜色变为红色。

请注意这个选择器与子选择器的区别，
- 子选择器（child selector）: 仅是指它的直接后代，或者你可以理解为作用于子元素的第一代后代。子选择器是通过 “>” 进行选择。
- 后代选择器: 作用于所有子后代元素。后代选择器通过空格来进行选择。

总结：
- `>`作用于元素的第一代后代
- `空格`作用于元素的所有后代。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>后代选择器</title>
    <style type="text/css">
    .first span {
        color: red;
    }
    /*区别 .food>li*/
    .food li {
        /*添加边框样式（粗细为1px， 颜色为红色的实线）*/
        border: 1px solid red;
    }
    </style>
</head>
<body>
    <p class="first">三年级时，我还是一个<span>胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <!--下面是本小节任务代码-->
    <ul class="food">
        <li>水果
            <ul>
                <li>香蕉</li>
                <li>苹果</li>
                <li>梨</li>
            </ul>
        </li>
        <li>蔬菜
            <ul>
                <li>白菜</li>
                <li>油菜</li>
                <li>卷心菜</li>
            </ul>
        </li>
    </ul>
</body>
</html>
```
## 八、通用选择器 `*`

通用选择器是功能最强大的选择器，它使用一个（*）号指定，它的作用是匹配 html 中所有标签元素，如下使用下面代码使用 html 中任意标签元素字体颜色全部设置为红色：

`* {color:red;}`

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>* 选择符</title>
    <style type="text/css">
    * {
        color: red;
    }
    </style>
</head>
<body>
    <h1>勇气</h1>
    <p>三年级时，我还是一个胆小如鼠的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p>到了三年级下学期时，我们班上了一节公开课，老师提出了一个很简单的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
    <img src="http://img.mukewang.com/52b4113500018cf102000200.jpg">
</body>
</html>
```

## 九、 伪类选择器

更有趣的是伪类选择符，为什么叫做伪类选择符，它允许给 html 不存在的标签（标签的某种状态）设置样式。
比如说我们给 html 中一个标签元素的鼠标滑过的状态来设置字体颜色：
 
`a:hover{color:red;}`
 
上面一行代码就是为 a 标签鼠标滑过的状态设置字体颜色变红。这样就会使第一段文字内容中的“胆小如鼠”文字加入鼠标滑过字体颜色变为红色特效。
 
关于伪选择符：
 
关于伪类选择符，到目前为止，可以兼容所有浏览器的“伪类选择符”就是 a 标签上使用 :hover 了（其实伪类选择符还有很多，尤其是 css3 中，但是因为不能兼容所有浏览器，本教程只是讲了这一种最常用的）。

其实 :hover 可以放在任意的标签上，比如说 p:hover，但是它们的兼容性也是很不好的，所以现在比较常用的还是 a:hover 的组合。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>伪类选择符</title>
    <style type="text/css">
    a:hover {
        color: red;
        font-size: 20px;

    }
    </style>
</head>
<body>
    <h1>勇气</h1>
    <p class="first">三年级时，我还是一个<a href="http://www.imooc.com">胆小如鼠</a>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p id="second">到了三年级下学期时，我们班上了一节公开课，老师提出了一个很<span>简单</span>的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
    <img src="http://img.mukewang.com/52b4113500018cf102000200.jpg">
</body>
</html>
```
## 十、分组选择器

当你想为 html 中多个标签元素设置同一个样式时，可以使用分组选择符（，），如下代码为右侧代码编辑器中的 h1、span 标签同时设置字体颜色为红色：

`h1,span{color:red;}`

它相当于下面两行代码：

```css
h1{color:red;}
span{color:red;}
```

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>分组选择符</title>
    <style type="text/css">
    .first,#second>span {
        color:green;
    }
    </style>
</head>
<body>
    <h1>勇气</h1>
    <p class="first">三年级时，我还是一个<span>胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p id="second">到了三年级下学期时，我们班上了一节公开课，老师提出了一个很<span>简单</span>的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
    <img src="http://img.mukewang.com/52b4113500018cf102000200.jpg">
</body>
</html>
```
