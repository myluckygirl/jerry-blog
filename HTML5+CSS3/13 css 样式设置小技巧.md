
## css 样式设置小技巧

### 一、行内元素

我们在实际工作中常会遇到需要设置水平居中的场景，比如为了美观，文章的标题一般都是水平居中显示的。

这里我们又得分两种情况：行内元素 还是 块状元素 ，块状元素里面又分为定宽块状元素，以及不定宽块状元素。今天我们先来了解一下行内元素怎么进行水平居中？

如果被设置元素为文本、图片等行内元素时，水平居中是通过给父元素设置 text-align:center 来实现的。(父元素和子元素：如下面的html代码中，div是“我想要在父容器中水平居中显示”这个文本的父元素。反之这个文本是div的子元素 )如下代码：

html代码：
```html
<body>
  <div class="txtCenter">我想要在父容器中水平居中显示。</div>
</body>
css代码：

<style>
  .txtCenter{
    text-align:center;
  }
</style>
```
```html
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>定宽块状元素水平居中</title>
<style>
div{
    border:1px solid red;
    margin:20px;
}
.txtCenter{
	text-align:center;
}
</style>
</head>
<body>
<div class="txtCenter">我想要在父容器中水平居中显示</div>
<!--下面是任务部分-->
<div class="imgCenter"><img src="http://img.mukewang.com/52da54ed0001ecfa04120172.jpg" /></div>
</body>
</html>
```

### 二、定宽块状元素

当被设置元素为 块状元素 时用 text-align：center 就不起作用了，这时也分两种情况：定宽块状元素和不定宽块状元素。

这一小节我们先来讲一讲定宽块状元素。(定宽块状元素：块状元素的宽度 width 为固定值。)

满足定宽和块状两个条件的元素是可以通过设置“左右 margin ”值为 “auto” 来实现居中的。我们来看个例子就是设置 div 这个块状元素水平居中：

html代码：
```
<body>
  <div>我是定宽块状元素，哈哈，我要水平居中显示。</div>
</body>
```
css代码：

```csss
<style>
div{
    border:1px solid red;/*为了显示居中效果明显为 div 设置了边框*/
    
    width:200px;/*定宽*/
    margin:20px auto;/* margin-left 与 margin-right 设置为 auto */
}
</style>
```
也可以写成：

`margin-left:auto;`  
`margin-right:auto;`
注意：元素的“上下 margin” 是可以随意设置的。

```html
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>定宽块状元素水平居中</title>
<style>
div{
    border:1px solid red;
	
	width: 200px;
	margin: 20px auto;
}

</style>
</head>
<body>
<div>我是定宽块状元素，我要水平居中显示。</div>
</body>
</html>
```

### 三、面试常考题之已知宽高实现盒子水平垂直居中

这一章节我们来学习已知宽高实现盒子水平垂直居中。

我们有如下两个 div 元素，要实现子元素相对于父元素垂直水平居中,我们只需要输入以下代码：


技术点的解释：

1、利用父元素设置相对定位,子元素设置绝对定位,那么子元素就是相对于父元素定位的特性。

2、子元素设置上和左偏移的值都为 50%，是元素的左上角在父元素中心点的位置。

3、然后再用 margin 给上和左都给负的自身宽高的一半,就能达到垂直水平居中的效果。

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>已知宽高实现盒子水平垂直居中</title>
    <style type="text/css">
    .box {
        border: 1px solid #00ee00;
        height: 300px;
        position: relative;
    }

    .box1 {
        width: 200px;
        height: 200px;
        border: 1px solid red;
        position: absolute;
        top: 50%;
        left: 50%;
        margin: -100px 0 0 -100px;
        
    }
    </style>
</head>

<body>
    <div class="box">
        <div class="box1"></div>
    </div>
</body>

</html>
```

### 四、面试常考题之宽高不定实现盒子水平垂直居中

技术点的解释：

1、利用父元素设置相对定位,子元素设置绝对定位,那么子元素就是相对于父元素定位的特性。

2、子元素设置上和左偏移的值都为 50%。

3、然后再用 css3 属性 translate 位移,给上和左都位移 -50% 距离，就能达到垂直水平居中的效果。

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>宽高不定实现盒子水平垂直居中</title>
    <style type="text/css">
    .box {
        border: 1px solid #00ee00;
        height: 300px;
        position: relative;
    }

    .box1 {
        border: 1px solid red;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    </style>
</head>

<body>
    <div class="box">
        <div class="box1">
            慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网慕课网
        </div>
    </div>
</body>

</html>
```