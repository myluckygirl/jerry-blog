
## flex 弹性盒模型

### 一、弹性盒模型之 flex 属性 `display: flex;`

这一章节我们来学习 flex 弹性盒子模型，根据下面的例子来理解一下吧：

三个块元素设置大小以及背景色，在父容器中添加 flex。

技术点的解释：

1、设置 `display: flex;` 属性可以把块级元素在一排显示。

2、flex 需要添加在父元素上，改变子元素的排列顺序。

3、默认为从左往右依次排列,且和父元素左边没有间隙。

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>flex布局</title>
    <style type="text/css">
    .box {
        background: blue;
        display: flex;
    }

    .box div {
        width: 200px;
        height: 200px;
    }

    .box1 {
        background: red;
    }

    .box2 {
        background: orange;
    }

    .box3 {
        background: green;
    }
    </style>
</head>

<body>
    <div class="box">
        <div class="box1"></div>
        <div class="box2"></div>
        <div class="box3"></div>
    </div>
</body>

</html>
```

### 二、使用`justify-content`属性设置横轴排列方式

这一章节我们来学习 justify-content 属性，本属性定义了项目在主轴上的对齐方式。结合上一节的布局例子进行理解，属性值分别为：

justify-content: flex-start | flex-end | center | space-between | space-around;

flex-start：交叉轴的起点对齐

```css
 .box {
        background: blue;
        display: flex;
        justify-content: flex-start;
 } 
```

flex-end：右对齐
```css
 .box {
        background: blue;
        display: flex;
        justify-content: flex-end;
    }
```

center： 居中
```css
 .box {
        background: blue;
        display: flex;
        justify-content: center;
    }
```

space-between：两端对齐，项目之间的间隔都相等。

```css
 .box {
        background: blue;
        display: flex;
        justify-content: space-between;
    }
```

space-around：每个项目两侧的间隔相等。所以，项目之间的间隔比项目与边框的间隔大一倍。

```
.box {
        background: blue;
        display: flex;
        justify-content: space-around;
    }
```

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>justify-content</title>
    <style type="text/css">
    .box {
        background: blue;
        display: flex;
        justify-content: flex-start;
        /*
        justify-content: flex-end;
        justify-content: center;
        justify-content: space-between;
        justify-content: space-around;
        */
    }

    .box div {

        width: 200px;
        height: 200px;
    }

    .box1 {
        background: red;
    }

    .box2 {
        background: orange;
    }

    .box3 {
        background: green;
    }
    </style>
</head>
<body>
    <div class="box">
        <div class="box1"></div>
        <div class="box2"></div>
        <div class="box3"></div>
    </div>
</body>
</html>
```

### 三、使用 `align-items`属性设置纵轴排列方式

这一章节我们来学习 align-items 属性，本属性定义了项目在交叉轴上的对齐方式。属性值分别为：

align-items: flex-start | flex-end | center | baseline | stretch;

结合右侧编辑器中的布局以及下面的样式设置进行理解：

flex-start：默认值，左对齐
```css
   .box {
        height: 700px;
        background: blue;
        display: flex;
        align-items: flex-start;
    }

```

flex-end：交叉轴的终点对齐
```css
 .box {
        height: 700px;
        background: blue;
        display: flex;
        align-items: flex-end;
    }
```

center： 交叉轴的中点对齐
```css
.box {
        height: 700px;
        background: blue;
        display: flex;
        align-items: center;
    }
```


baseline：项目的第一行文字的基线对齐。
```
.box {
        height: 700px;
        background: blue;
        display: flex;
        align-items: baseline;
    }
```
三个盒子中设置不同的字体大小，可以参考右侧编辑器中的代码进行测试。


stretch（默认值）：如果项目未设置高度或设为auto，将占满整个容器的高度。
```css
 .box {
        height: 300px;
        background: blue;
        display: flex;
        align-items: stretch;
    }

    .box div {
        /*不设置高度，元素在垂直方向上铺满父容器*/
        width: 200px;
    }
```
```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>align-items</title>
    <style type="text/css">
    .box {
        height: 700px;
        background: blue;
        display: flex;
        align-items: baseline;
    }

    .box div {
        width: 200px;
        height: 200px;
    }

    .box1 {
        background: red;
    }

    .box2 {
        font-size: 30px;
        background: orange;
    }

    .box3 {
        font-size: 50px;
        background: green;
    }
    </style>
</head>
<body>
    <div class="box">
        <div class="box1">baseline</div>
        <div class="box2">baseline</div>
        <div class="box3">baseline</div>
    </div>
</body>
</html>
```

### 四、给子元素设置 flex 占比

这一章节我们来学习flex属性，设置子元素相对于父元素的占比。

可以参考右侧编辑器的代码，测试效果如下：

技术点的解释：

1、给子元素设置flex属性,可以设置子元素相对于父元素的占比。

2、flex属性的值只能是正整数,表示占比多少。

3、给子元素设置了flex之后,其宽度属性会失效。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>flex占比</title>
    <style type="text/css">
    .box {
        height: 300px;
        background: blue;
        display: flex;
    }
    .box div {
        width: 200px;
        height: 200px;
    }
    .box1 {
        flex: 1;
        background: red;
    }
    .box2 {
        flex: 3;
        background: orange;
    }
    .box3 {
        flex: 2;
        background: green;
    }
    </style>
</head>
<body>
    <div class="box">
        <div class="box1">flex:1</div>
        <div class="box2">flex:3</div>
        <div class="box3">flex:2</div>
    </div>
</body>
</html>
```