
## CSS 的继承，优先级和重要性

### 一、样式的继承

CSS 的**某些样式**是具有继承性的，那么什么是继承呢？继承是一种规则，它允许样式不仅应用于某个特定 html 标签元素，而且应用于其后代。

比如下面代码：如某种颜色应用于 p 标签，这个颜色设置不仅应用 p 标签，还应用于 p 标签中的所有子元素文本，这里子元素为 span 标签。

```
`p{color:red;}`

<p>三年级时，我还是一个<span>胆小如鼠</span>的小女孩。</p>
```
可见右侧结果窗口中 p 中的文本与 span 中的文本都设置为了红色。但注意有一些 css 样式是不具有继承性的。如 border:1px solid red;

```
p{border:1px solid red;}

<p>三年级时，我还是一个<span>胆小如鼠</span>的小女孩。</p>
在上面例子中它代码的作用只是给p标签设置了边框为1像素、红色、实心边框线，而对于子元素span是没用起到作用的。
```

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>继承</title>
    <style type="text/css">
    p {
        color: red;
        border:1px solid green;
    }
    </style>
</head>
<body>
    <h1>勇气</h1>
    <p class="first">三年级时，我还是一个<span>胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p id="second">到了三年级下学期时，我们班上了一节公开课，老师提出了一个很<span>简单</span>的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
</body>
</html>
```
### 二、选择器的优先级

之前我们已经学过了很多 CSS3 选择器了,但是每个选择器是有优先级的。这一章我们来学习 CSS3 中选择器的优先级。

技术点的解释：

1、如果一个元素使用了多个选择器,则会按照选择器的优先级来给定样式。

2、选择器的优先级依次是: **内联样式 > id 选择器 > 类选择器 > 标签选择器 > 通配符选择器**

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>css3选择器优先级</title>
    <style type="text/css">
       #id {
           color: green;
       } 
       .className {
           color:red;
       }
       * {
           color: pink;
       }
      div {
          color: black;
      }
    </style>
</head>
<body>
    <div id="id" class="className" style="color: blue">
        我是一个div
    </div>
</body>
</html>
```

### 三、权值计算-特殊性

有的时候我们为同一个元素设置了不同的 CSS 样式代码，那么元素会启用哪一个 CSS 样式呢？下面我们一起来看一下代码：

```
p{color:red;}
.first{color:green;}
<p class="first">三年级时，我还是一个<span>胆小如鼠</span>的小女孩。</p>
p和.first都匹配到了p这个标签上，那么会显示哪种颜色呢？green是正确的颜色，那么为什么呢？是因为浏览器是根据权值来判断使用哪种css样式的，权值高的就使用哪种css样式。
```
下面是权值的规则：
- 标签的权值为 1
- 类选择符的权值为 10
- ID 选择符的权值最高为 100

例如下面的代码：
```
p{color:red;} /*权值为1*/
p span{color:green;} /*权值为1+1=2*/
.warning{color:white;} /*权值为10*/
p span.warning{color:purple;} /*权值为1+1+10=12*/
#footer .note p{color:yellow;} /*权值为100+10+1=111*/
```
注意：还有一个权值比较特殊--继承也有权值但很低，有的文献提出它只有0.1，所以可以理解为继承的权值最低。

```html
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>特殊性</title>
<style type="text/css">
p{color: red;}/*1*/
.first{color: green;}/*因为权值高显示为绿色 10*/
span{color: pink;}/*设置为粉色 1*/
/*设置为紫色 .first>span 1+1=2*/
.first span {
    color: purple;
}
</style>
</head>
<body>
    <h1>勇气</h1>
    <p class="first">三年级时，我还是一个<span>胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p id="second">到了三年级下学期时，我们班上了一节公开课，老师提出了一个很简单的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
</body>
</html>
```
### 四、选择器最高层级`!important`

我们在做网页代码的时，有些特殊的情况需要为某些样式设置具有最高权值，怎么办？这时候我们可以使用 `!important` 来解决。

如下代码：
```
p{color:red!important;}
p{color:green;}
<p class="first">三年级时，我还是一个<span>胆小如鼠</span>的小女孩。</p>
```
这时 p 段落中的文本会显示的 red 红色。

注意：**!important** 要写在分号的前面

这里注意当网页制作者不设置 css 样式时，浏览器会按照自己的一套样式来显示网页。
并且用户也可以在浏览器中设置自己习惯的样式，比如有的用户习惯把字号设置为大一些，使其查看网页的文本更加清楚。

这时注意样式优先级为：**浏览器默认的样式 < 网页制作者样式 < 用户自己设置的样式**，但记住 `!important` 优先级样式是个例外，权值高于用户自己设置的样式。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>!important</title>
    <style type="text/css">
    p {
        color: red!important;
    }
    p.first {
        color: green;
    }
    </style>
</head>
<body>
    <h1>勇气</h1>
    <p class="first">三年级时，我还是一个<span>胆小如鼠</span>的小女孩，上课从来不敢回答老师提出的问题，生怕回答错了老师会批评我。就一直没有这个勇气来回答老师提出的问题。学校举办的活动我也没勇气参加。</p>
    <p id="second">到了三年级下学期时，我们班上了一节公开课，老师提出了一个很<span class="first">简单</span>的问题，班里很多同学都举手了，甚至成绩比我差很多的，也举手了，还说着："我来，我来。"我环顾了四周，就我没有举手。</p>
</body>
</html>
```
