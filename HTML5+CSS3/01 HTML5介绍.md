# 第一章 HTML5 介绍

## 一. html 和 css 的关系

html标签：
```html
<p>我是一个p标签</p>
```

css代码：
```css
p {
        color: red;
        border: 1px solid blue;
        width: 140px;
        height: 40px;
    }
```
- css 是用来修饰 html 样式的;
- html 本身是有一些默认样式,如果我们想改变 html 标签的样式,就需要借助 css;
- html + css 构成了我们网页的基本页面结构和样式。

> 常用的样式：
- 字体大小：font-size:12px;
- 文字颜色：color:#930;
- 文字对齐方式：text-align:center;
- 边框样式：border: 1px solid blue;
- 宽度：width: 140px;
- 高度：height: 40px;

## 二. 标签的语法

1. 标签由英文尖括号<和>括起来，如<html>就是一个标签。

2. html中的标签一般都是成对出现的，分开始标签和结束标签。结束标签比开始标签多了一个/。

>如：  
（1） `<p></p>`   
（2） `<div></div>`  
（3） `<span></span>`    

3. 标签与标签之间是可以嵌套的，但先后顺序必须保持一致，如：`<div>`里嵌套`<p>`，那么`</p>`必须放在`</div>`的前面。

4. HTML 标签不区分大小写，`<h1>`和`<H1>`是一样的，但建议小写，因为大部分程序员都以小写为准。

## 三. html5 文档结构

```html
<!DOCTYPE html> <!--声明文档类型为 html5-->
<html> <!--html 标签为 html 文件的跟标签-->
<head> <!--head 标签为头部标签，一般用来放置 meta 和 title 标签等-->
    <meta charset="UTF-8"> <!--声明当前文件字符集编码为 utf-8-->
    <title>html 文档结构</title> <!--title 标签设置浏览器标题-->
</head>
<body>
    <!--body 标签为我们网页的内容-->
</body>
</html>
```

技术点的解释：

1. `<!DOCTYPE html>`:文档类型声明，表示该文件为 HTML5 文件。`<!DOCTYPE>` 声明必须是 HTML 文档的第一行，位于 `<html>` 标签之前

2. `<html></html>`标签对：`<html>`标签位于 HTML 文档的最前面，用来标识 HTML 文档的开始；`</html>`标签位于 HTML 文档的最后面，用来标识 HTML 文档的结束；这两个标签对成对存在，中间的部分是文档的头部和主题。

3. `<head></head>`标签对：标签包含有关 HTML 文档的信息，可以包含一些辅助性标签。如`<title></title>`，`<link />``<meta />`，`<style></style>`，`<script></script>`等，但是浏览器除了会在标题栏显示`<title>`元素的内容外，不会向用户显示 head 元素内的其他任何内容。

4. `<body></body>`标签对：它是 HTML 文档的主体部分，在此标签中可以包含`<p><h1><br>`等众多标签，`<body>`标签出现在`</head>`标签之后，且必须在闭标签`</html>`之前闭合。

## 四. 认识 head 标签

```html
<!DOCTYPE html>
<html>
<head>
   <!-- head 标签为头部标签，通常嵌套 meta、title、style 等标签-->
    <meta charset="UTF-8"> <!--声明当前文件字符集编码为 utf-8-->
    <title>认识 head</title> <!--title 标签设置浏览器标题-->
    
    <!--书写 css 样式-->
    <style>
        #box1 {
            color:red;
        }
    </style>
</head>
<body>

</body>
</html>
```

文档的头部描述了文档的各种属性和信息，包括文档的标题等，绝大多数文档头部包含的数据都不会真正作为内容显示给读者。

下面这些标签可用在 head 部分：

1、head 标签为双标签，有尾标签，`<head></head>`;

2、head 标签表示头部标签,通常用来嵌套 meta、title、style 等标签;

3、`<title>`标签：在`<title>`和`</title>`标签之间的文字内容是网页的标题信息，它会出现在浏览器的标题栏中。网页的 title 标签用于告诉用户和搜索引擎这个网页的主要内容是什么，搜索引擎可以通过网页标题，迅速的判断出网页的主题。每个网页的内容都是不同的，每个网页都应该有一个独一无二的 title ;

4、`<meta charset="UTF-8">`设置当前文件字符编码;

5、style 标签：双标签中设置当前文件样式。

## 五. 认识 body 标签

在网页上要展示出来的页面内容一定要放在 body 标签中。
```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"> 
    <title>认识 body</title> 
</head>
<body>
    <!--标题标签-->
    <h1>小和尚的故事</h1>
    <!--段落标签-->
    <p>一个和尚打水喝</p>
    <!--段落标签-->
    <p>两个和尚抬水喝</p>
    <!--段落标签-->
    <p>三个和尚没水喝 那怎么办呢？<span>你说呢？</span></p>
    
    <!--超链接-->
    <a href="#">咨询一下</a>
</body>
</html>
```

## 六. 代码注解

**什么是代码注释？**
>代码注释的作用是帮助程序员标注代码的用途，过一段时间后再看你所编写的代码，就能很快想起这段代码的用途。代码注释不仅方便程序员自己回忆起以前代码的用途，还可以帮助其他程序员很快的读懂你的程序的功能，方便多人合作开发网页代码。

语法：
`<!--注释文字 -->`

Mac 代码注解快捷键：`control + shift + /`

